package com.tr.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.Query;
import javax.persistence.Transient;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicHeader;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.junit.runner.Result;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.tr.model.AppParamDetail;
import com.tr.model.Defect;
import com.tr.model.GnlParameter;
import com.tr.model.PropertyGroup;
import com.tr.model.Report;
import com.tr.model.Request;
import com.tr.model.Request_Header;
import com.tr.model.ScheduleStep;
import com.tr.model.Scheduler;
import com.tr.model.TestCase;
import com.tr.model.TestCaseProperty;
import com.tr.model.TestStep;

import io.appium.java_client.android.AndroidElement;

public class Smartest {

	Boolean bool;
	private String logText = "";
	private String testResult = "Success";
	private ChromeDriver driver;
	private Map<String, String> map = new HashMap<String, String>();
	private String filePath = "";
	private Scheduler sch;
	transient StringWriter sw = null;
	transient PrintWriter pw = null;
	private SessionFactory sessionFactory;
	private Session session;
	public WebDriverWait wait;
	private Report rep = null;

	public Smartest(String username) {
		sessionFactory = new Configuration().configure().buildSessionFactory();
		getSession().beginTransaction();
		rep = new Report();
		rep.setCreate_by(username);
		getSession().saveOrUpdate(rep);
		getSession().getTransaction().commit();
		getSession().beginTransaction();

	}

	public void executeJavaStep(WebDriver driver, int teststep_id, String status) throws Exception {
		TestStep ts = (TestStep) getSession().createQuery("from TestStep where id=:id").setParameter("id", teststep_id)
				.getSingleResult();
		if (status.equals("failed")) {
			for (ScheduleStep ss : sch.getScheduleSteps()) {
				if (ss.getTestStep_id() == ts.getId()) {
					DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
					ss.setStatus("FAILED");
					takeScreenShot(driver);
					ss.setExecuion_date(dateFormat.format(new Date()));
					ss.setSs_url(filePath);
					ss.setDescription(logText);
					getSession().saveOrUpdate(ss);
					getSession().saveOrUpdate(sch);
				}
			}
			getSession().getTransaction().commit();
			throw new Exception("\n Hata :" + ts.getAppParameter().getDescription() + " Hata Alındı ");
		} else {
			for (ScheduleStep ss : sch.getScheduleSteps()) {
				if (ss.getTestStep_id() == ts.getId()) {
					DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
					ss.setStatus("PASSED");
					takeScreenShot(driver);
					ss.setExecuion_date(dateFormat.format(new Date()));
					ss.setSs_url(filePath);
					ss.setDescription(logText);
					getSession().saveOrUpdate(ss);
					getSession().saveOrUpdate(sch);
					getSession().getTransaction().commit();
					getSession().beginTransaction();
				}
			}
			logText = "";
		}

	}

	public void executeTestStep(WebDriver driver, int teststep_id, String platform) throws Exception {

		Thread.sleep(1000);
		sch.setLogs(logText);
		bool = false;
		TestStep ts = (TestStep) getSession().createQuery("from TestStep where id=:id").setParameter("id", teststep_id)
				.getSingleResult();
		System.out.println(ts.getAppParameter().getDescription() + " Adımı İşleniyor....<br/>");
		for (AppParamDetail appParamDetail : ts.getAppParameter().getAppParamDetailList()) {
			bool = executeAppParamDet(driver, appParamDetail, ts, platform);
			if (bool == true) {
				 appParamDetail.setLast_run_status("FOUND");
				 appParamDetail.setExecute_count(appParamDetail.getExecute_count() + 1);
				 getSession().saveOrUpdate(appParamDetail);
				break;
			} else {
				 appParamDetail.setLast_run_status("NOT_FOUND");
				 appParamDetail.setExecute_count(appParamDetail.getExecute_count() + 1);
				 appParamDetail.setFail_count(appParamDetail.getFail_count() + 1);
				 getSession().saveOrUpdate(appParamDetail);
				logText += "Element Not Found <br/>";
			}
		}
		if (!bool) {
			for (ScheduleStep ss : sch.getScheduleSteps()) {
				if (ss.getTestStep_id() == ts.getId()) {
					DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
					ss.setStatus("FAILED");
					// takeScreenShot(driver);
					ss.setExecuion_date(dateFormat.format(new Date()));
					ss.setSs_url(filePath);
					ss.setDescription(logText);
					getSession().saveOrUpdate(ss);
					getSession().saveOrUpdate(sch);
				}
			}
			getSession().getTransaction().commit();
			throw new Exception("\n Hata :" + ts.getAppParameter().getDescription() + " Test Objesi Bulunamadı... ");
		} else {
			for (ScheduleStep ss : sch.getScheduleSteps()) {
				if (ss.getTestStep_id() == ts.getId()) {
					DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
					ss.setStatus("PASSED");
					// takeScreenShot(driver);
					ss.setExecuion_date(dateFormat.format(new Date()));
					ss.setSs_url(filePath);
					ss.setDescription(logText);
					getSession().saveOrUpdate(ss);
					getSession().saveOrUpdate(sch);
					getSession().getTransaction().commit();
					getSession().beginTransaction();
				}
			}
			logText = "";
		}
	}

	private Boolean executeAppParamDet(WebDriver driver, AppParamDetail appParamDetail, TestStep ts, String platform)
			throws  IOException {
		String action = appParamDetail.getAction_type();
		platform = appParamDetail.getPlatform();

		String param1;
		if (ts.getParam1() != null && !ts.getParam1().equals(""))
			param1 = getParameter(ts.getParam1());
		else
			param1 = ts.getDefaultValue();

		WebElement element = getWebElement(driver, param1, appParamDetail.getId(), platform);
		if (element == null)
			return false;

			String attr_value = appParamDetail.getAttr_value();
			String attrs[] = attr_value.split("/");
			attr_value = attrs[attrs.length - 1];

			switch (action) {

			case "click":
				if (element == null)
					return false;
				else {
					try {
						element.click();
						logText += " Clicked <br/>";
						if (driver instanceof JavascriptExecutor) {
							((JavascriptExecutor) driver).executeScript("arguments[0].style.border='3px solid red'",
									element);
						}
						takeScreenShot(driver);
						if (driver instanceof JavascriptExecutor) {
							((JavascriptExecutor) driver)
									.executeScript("arguments[0].style.border='3px solid transparent'", element);
						}
					} catch (Exception exp) {
						return false;
					}
					return true;
				}
			case "dblClick":
				Actions act = new Actions(driver);
				if (element == null)
					return false;
				else {
					try {
						act.moveToElement(element).doubleClick().perform();
						logText += " Double Clicked <br/>";
						if (driver instanceof JavascriptExecutor) {
							((JavascriptExecutor) driver).executeScript("arguments[0].style.border='3px solid red'",
									element);
						}
						takeScreenShot(driver);
						if (driver instanceof JavascriptExecutor) {
							((JavascriptExecutor) driver)
									.executeScript("arguments[0].style.border='3px solid transparent'", element);
						}
					} catch (Exception exp) {
						return false;
					}
					return true;
				}

			case "type":
				if (element == null)
					return false;
				else {
					try {
						element.sendKeys(param1);
						logText += param1 + " Typed <br/>";
						if (driver instanceof JavascriptExecutor) {
							((JavascriptExecutor) driver).executeScript("arguments[0].style.border='3px solid red'",
									element);
						}
						takeScreenShot(driver);
						if (driver instanceof JavascriptExecutor) {
							((JavascriptExecutor) driver)
									.executeScript("arguments[0].style.border='3px solid transparent'", element);
						}
					} catch (Exception exp) {
						return true;
					}
					return true;
				}

			case "select":

				if (element == null)
					return false;
				else {
					try {
						Select slct = new Select(element);
						slct.selectByVisibleText(param1);
						if (driver instanceof JavascriptExecutor) {
							((JavascriptExecutor) driver).executeScript("arguments[0].style.border='3px solid red'",
									element);
						}
						takeScreenShot(driver);
						if (driver instanceof JavascriptExecutor) {
							((JavascriptExecutor) driver)
									.executeScript("arguments[0].style.border='3px solid transparent'", element);
						}
					} catch (Exception exp) {
						return false;
					}
					return true;
				}

			case "is_active":
				if (element == null)
					return false;
				else {
					try {
						element.isEnabled();
					} catch (Exception exp) {
						return false;
					}
					return true;
				}
			default:
				return false;
			}
	}

	public String getSelectBoxText(WebElement webElement) {
		Select select = new Select(webElement);
		WebElement option = select.getFirstSelectedOption();
		return option.getText();
	}

	public WebElement getElementByAppId(WebDriver driver, String param1, int appParameter_id, String type)
			throws Exception {
		List<AppParamDetail> appParamDetailList;
		appParamDetailList = (List<AppParamDetail>) getSession().createQuery(
				"from AppParamDetail where appParameter_id=:id and status=:status and last_run_status=:last_run_status")
				.setParameter("id", appParameter_id).setParameter("status", "Aktif")
				.setParameter("last_run_status", "FOUND").getResultList();
		if (!appParamDetailList.isEmpty()) {
			for (AppParamDetail appParamDetail : appParamDetailList) {
				WebElement element = getWebElement(driver, param1, appParamDetail.getId(), type);
				if (element != null) {
					if (driver instanceof JavascriptExecutor) {
						((JavascriptExecutor) driver).executeScript("arguments[0].style.border='3px solid red'",
								element);
					}
					takeScreenShot(driver);
					if (driver instanceof JavascriptExecutor) {
						((JavascriptExecutor) driver).executeScript("arguments[0].style.border='3px solid transparent'",
								element);
					}
					return element;
				}
			}
		} else {
			appParamDetailList = (List<AppParamDetail>) getSession()
					.createQuery("from AppParamDetail where appParameter_id=:id and status=:status")
					.setParameter("id", appParameter_id).setParameter("status", "Aktif").getResultList();
			for (AppParamDetail appParamDetail : appParamDetailList) {
				WebElement element = getWebElement(driver, param1, appParamDetail.getId(), type);
				if (element != null) {
					if (driver instanceof JavascriptExecutor) {
						((JavascriptExecutor) driver).executeScript("arguments[0].style.border='3px solid red'",
								element);
					}
					takeScreenShot(driver);
					if (driver instanceof JavascriptExecutor) {
						((JavascriptExecutor) driver).executeScript("arguments[0].style.border='3px solid transparent'",
								element);
					}
					return element;
				}

			}

		}
		throw new Exception("\n Hata : Test Objesi Bulunamadı... ");
	}

	public WebElement getWebElement(WebDriver driver, String param1, int appParamDetail_id, String type) {
		List<WebElement> list;
		AppParamDetail appParamDetail = (AppParamDetail) getSession()
				.createQuery("from AppParamDetail where id=:id and status=:status")
				.setParameter("id", appParamDetail_id).setParameter("status", "Aktif").getSingleResult();
		String attr_value = appParamDetail.getAttr_value();
		String attrs[] = attr_value.split("/");
		attr_value = attrs[attrs.length - 1];
		try {
		if (type.equals("ANDROID")) {

			AndroidElement element = (AndroidElement) driver.findElement(By.id(attr_value));

			return element;

		} else {
			switch (appParamDetail.getAttr_name()) {
			case "xpath":
				String rep_xpath = appParamDetail.getAttr_value();
				String params[];
				if (param1 != null) {
					if (param1.contains("&")) {
						params = param1.split("&");
						if (rep_xpath.contains("param1"))
							rep_xpath = rep_xpath.replaceAll("param1", params[0]);
						if (rep_xpath.contains("param2"))
							rep_xpath = rep_xpath.replaceAll("param2", params[1]);
						if (rep_xpath.contains("param3"))
							rep_xpath = rep_xpath.replaceAll("param2", params[2]);
						if (rep_xpath.contains("param4"))
							rep_xpath = rep_xpath.replaceAll("param2", params[3]);
					} else {
						if (rep_xpath.contains("param1"))
							rep_xpath = rep_xpath.replaceAll("param1", param1);
					}
				}

				logText += "driver.findElements(By.xpath(" + rep_xpath + "));....<br/>";
				list = (List<WebElement>) driver.findElements(By.xpath(rep_xpath));
				if (list.isEmpty()) {
					return null;
				} else {
					return list.get(0);
				}
			case "prev_text":
				logText += "driver.findElements(By.xpath(\"//span[contains(text(),'" + attr_value
						+ "')]/following-sibling::" + appParamDetail.getTag_name() + "));<br/>";
				list = (List<WebElement>) driver.findElements(By.xpath("//span[contains(text(),'" + attr_value
						+ "')]/following-sibling::" + appParamDetail.getTag_name() + ""));
				if (list.isEmpty()) {
					return null;
				} else {
					return list.get(0);
				}
			case "textContent":
				logText += "driver.findElements(By.xpath(\"//*[contains(text(),'" + attr_value + "')]\"));....<br/>";
				list = (List<WebElement>) driver.findElements(By.xpath("//*[contains(text(),'" + attr_value + "')]"));
				if (list.isEmpty()) {
					logText += "driver.findElements(By.xpath(\"" + attr_value + "\"));....<br/>";
					list = (List<WebElement>) driver.findElements(By.xpath("" + attr_value + ""));
				} else {
					return list.get(0);
				}
			default:
				logText += "driver.findElements(By.cssSelector(" + "\"" + appParamDetail.getTag_name() + "["
						+ appParamDetail.getAttr_name() + "*='" + attr_value + "']\"));....<br/>";
				list = (List<WebElement>) driver.findElements(By.cssSelector("" + appParamDetail.getTag_name() + "["
						+ appParamDetail.getAttr_name() + "*='" + attr_value + "']"));
				if (list.isEmpty()) {
					return null;
				} else {
					return list.get(0);
				}
			}
		}
		}catch(Exception exp) {
			logText += exp.toString();
			return null;
		}
			 
	}

	public void setParameter(String parameter, String value) {
		PropertyGroup pg = sch.getPropertyGroup();
		for (TestCaseProperty tcp : pg.getTestCasePropertyList()) {
			if (tcp.getKey().equals(parameter)) {
				tcp.setValue(value);
				getSession().saveOrUpdate(tcp);
				break;
			}

		}
	}

	public String getParameter(String param) {
		PropertyGroup pg = sch.getPropertyGroup();
		Set<TestCaseProperty> ltcp = pg.getTestCasePropertyList();
		for (TestCaseProperty tcp : ltcp) {
			if (tcp.getKey().equals(param)) {
				String str = tcp.getSourceStr();
				if (str != null) {
					String astr[] = str.split("_");
					String tcStr = astr[0];
					String tcpStr = astr[2];
					Scheduler schSource = (Scheduler) getSession()
							.createQuery("from Scheduler where schedulerid=:sch_id and testcase_id=:testcase_id")
							.setParameter("sch_id", sch.getSchedulerid())
							.setParameter("testcase_id", Integer.parseInt(tcStr)).getSingleResult();
					for (TestCaseProperty tcpSource : schSource.getPropertyGroup().getTestCasePropertyList()) {
						if (tcpSource.getKey().equals(tcpStr)) {
							return tcpSource.getValue();
						}
					}

				} else {
					return tcp.getValue();
				}
				return null;
			}
		}
		return null;

	}

	public void setSchId(int schId) {
		sch = getSchedulerById(schId);
	}

	public Scheduler getSchedulerById(int schId) {
		sch = (Scheduler) getSession().createQuery("from Scheduler where id=:id").setParameter("id", schId)
				.getSingleResult();
		return sch;
	}

	private String getParamValue(String key) {

		String value = null;
		List<GnlParameter> list = getSession().createQuery("from GnlParameter where status=:status")
				.setParameter("status", "Aktif").getResultList();
		for (GnlParameter g : list) {
			if (g.getName().equals(key)) {
				value = g.getValue();
			}
		}

		return value;
	}

	public String executeSql(String sql) throws SQLException {

		Connection dbConnection = null;
		Statement statement = null;

		dbConnection = getDBConnection();
		statement = dbConnection.createStatement();

		ResultSet rs = statement.executeQuery(sql);
		rs.next();
		return rs.getString(1);

	}

	private Connection getDBConnection() throws SQLException {

		Connection dbConnection = null;
		String dbUrl = getParamValue("db_url");
		String dbUsername = getParamValue("db_username");
		String dbPassword = getParamValue("db_password");

		dbConnection = DriverManager.getConnection(dbUrl, dbUsername, dbPassword);
		return dbConnection;

	}

	public String getStackTrace(final Throwable throwable) {
		sw = new StringWriter();
		pw = new PrintWriter(sw, true);
		throwable.printStackTrace(pw);
		pw.close();
		try {
			sw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return sw.getBuffer().toString().substring(0, 500);
	}

	@SuppressWarnings("restriction")
	public void finishTestCase(WebDriver driver, int testcase_id) {
		TestCase tc = null;
		try {
			if (getSession().getTransaction().isActive())
				getSession().getTransaction().commit();
			getSession().beginTransaction();
			tc = (TestCase) getSession().createQuery("from TestCase where id=:id").setParameter("id", testcase_id)
					.getSingleResult();
			takeScreenShot(driver);

		} catch (Exception exp) {
			exp.printStackTrace();
		} finally {
			driver.close();
			driver.quit();
			sch.setStatus("FINISHED");
			reportTestCase(tc);
		}

	}

	private void takeScreenShot(WebDriver driver) {
		filePath = new SimpleDateFormat("yyyyMMddHHmmss'.jpg'").format(new Date());
		String path = this.getClass().getClassLoader().getResource("").getPath();

		String fullPath = null;
		try {
			fullPath = URLDecoder.decode(path, "UTF-8");
		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		String pathArr[] = fullPath.split("WEB-INF/classes/");
		fullPath = pathArr[0];

		filePath = "resources/images/" + rep.getId() + "/" + "jpg_" + filePath;
		File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		try {
			FileUtils.copyFile(scrFile, new File(fullPath + filePath));
		} catch (IOException ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
		}

	}

	public void reportTestCase(TestCase tc) {
		Long key = null;
		try {
			Query query = getSession().createSQLQuery("select distinct next_val from hibernate_sequence ");
			key = ((BigInteger) ((org.hibernate.Query) query).uniqueResult()).longValue();
		} catch (Exception ex) {
			System.out.println(ex.toString());
		}

		Result result = null;
		Long xxx = key;
		int runId = Integer.parseInt(String.valueOf(xxx));
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		Report report = (Report) getSession().createQuery("from Report where id =:id").setParameter("id", rep.getId()).getSingleResult();
		report.setTestCase(tc);
		report.setRunId(runId);
		report.setEndDate(dateFormat.format(date));
		report.setSs_url(filePath);
		report.setErrlog(logText);
		report.setScheduler(sch);
		report.setCompany_id(tc.getCompany_id());
		if (!testResult.equals("Success")) {
			rep.setState("failed");
			Defect defect = new Defect();
			defect.setFailed_test_case(tc.getId());
			// defect.setCreated_by((String) session.getAttribute("username"));
			defect.setCreated_date(new Date());
			defect.setDescription(logText);
			defect.setFailed_test_suite(0);
			defect.setRunId(runId);
			getSession().saveOrUpdate(defect);
		} else
			report.setState("passed");
		getSession().saveOrUpdate(report);
		getSession().saveOrUpdate(sch);
		getSession().getTransaction().commit();

	}

	public void getVideoFromScreenShots() {
		String filePath = "output";
		File fileP = new File(filePath);
		String commands = "D:\\ffmpeg-win32-static\\bin\\ffmpeg -f image2 -i " + fileP + "\\image%5d.png " + fileP
				+ "\\video.mp4";
		System.out.println(commands);
		try {
			Runtime.getRuntime().exec(commands);
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println(fileP.getAbsolutePath());
	}

	public String runRequest(int request_id) {
		Request request = (Request) getSession().createQuery("from Request where id =:request_id")
				.setParameter("request_id", request_id).getSingleResult();
		String input = request.getRequest_text();
		PropertyGroup pg = sch.getPropertyGroup();
		Map<String, String> propertyList = new HashMap<String, String>();
		for (TestCaseProperty tcp : pg.getTestCasePropertyList()) {
			input = input.replace("[" + tcp.getKey() + "]", tcp.getValue());
			propertyList.put(tcp.getKey(), tcp.getValue());
		}

		List<Request_Header> rh_list = getSession().createQuery("from Request_Header where request_id =:request_id")
				.setParameter("request_id", request_id).list();

		String output = "";
		List<Header> headers = new ArrayList<Header>();
		Header header;
		for (Request_Header rh : rh_list) {
			header = new BasicHeader(rh.getKey(), rh.getValue());
			headers.add(header);
		}
		HttpResponse response = null;
		StringEntity entity = null;
		try {
			entity = new StringEntity(input);
		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			output = getStackTrace(e1);
			return output;
		}
		CredentialsProvider provider = new BasicCredentialsProvider();
		UsernamePasswordCredentials credentials = new UsernamePasswordCredentials(propertyList.get("username"),
				propertyList.get("password"));
		provider.setCredentials(AuthScope.ANY, credentials);

		HttpClient httpClient = HttpClientBuilder.create().setDefaultCredentialsProvider(provider)
				.setDefaultHeaders(headers).build();
		HttpPost postRequest = new HttpPost(request.getUrl());
		postRequest.addHeader("content-type", "application/json");
		postRequest.setEntity(entity);

		try {
			response = httpClient.execute(postRequest);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			output = getStackTrace(e1);
			return output;
		}
		System.out.println(request.toString());
		System.out.println(response.toString());
		try {

			String authString = "hbintegration:admin123";
			byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
			String authStringEnc = new String(authEncBytes);
			// URL url = new URL("http://10.70.82.29:8092/auth/getToken");
			URL url = new URL(request.getUrl());
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestProperty("Authorization", "Basic " + authStringEnc);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Accept", "application/json");

			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
			}

			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

			System.out.println("Output from Server .... \n");
			while ((output = br.readLine()) != null)
				;
		} catch (MalformedURLException e) {

			e.printStackTrace();
			output = getStackTrace(e);

		} catch (IOException e) {

			e.printStackTrace();
			output = getStackTrace(e);

		}
		return output;

	}

	public Session getSession() {
		return sessionFactory.getCurrentSession();
	}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public Map<String, String> getMap() {
		return map;
	}

	public void setMap(Map<String, String> map) {
		this.map = map;
	}

	public String getLogText() {
		return logText;
	}

	public void setLogText(String logText) {
		this.logText = logText;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getTestResult() {
		return testResult;
	}

	public void setTestResult(String testResult) {
		this.testResult = testResult;
	}

}
