package com.tr.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

public class HibernateUtil {

	private static SessionFactory sessionFactory = buildSessionFactory();
	private static StandardServiceRegistry registry;
	private static final Logger LOG = LogManager.getLogger(HibernateUtil.class);
	
	public static SessionFactory buildSessionFactory() {
		SessionFactory sessionFactory = null;
		// A SessionFactory is set up once for an application!
		registry = new StandardServiceRegistryBuilder().configure() // configures settings from hibernate.cfg.xml
				.build();
		try {
			sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
		} catch (Exception e) {
			LOG.error(e);
			// The registry would be destroyed by the SessionFactory, but we had trouble building the SessionFactory
			// so destroy it manually.
			StandardServiceRegistryBuilder.destroy(registry);
		}
		return sessionFactory;
	}

	public static SessionFactory getSessionFactory() {
		return sessionFactory;
	}
	
	public static void close(){
		try{
			if(registry!= null) {
		        StandardServiceRegistryBuilder.destroy(registry);
		    }
		}catch(Exception e){
			LOG.error(e);
		}
	}

	public static StandardServiceRegistry getRegistry() {
		return registry;
	}

	public static void setRegistry(StandardServiceRegistry registry) {
		HibernateUtil.registry = registry;
	}

	public static void setSessionFactory(SessionFactory sessionFactory) {
		HibernateUtil.sessionFactory = sessionFactory;
	}
}