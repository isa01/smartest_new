package com.tr.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.junit.Assume.assumeNoException;

import java.io.File;
import java.io.Serializable;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;

import org.apache.commons.io.FileUtils;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.tr.dao.UserDao;
import com.tr.model.AppParamDetail;
import com.tr.model.AppParameter;
import com.tr.model.GnlParameter;
import com.tr.model.TestStep;

import org.junit.Assert;

public class Functions implements Serializable {
	public void getCurrentUrl(WebDriver driver) {
		driver.getCurrentUrl();
	}
	public void getTitle(WebDriver driver) {
		driver.getTitle();
	}
}
