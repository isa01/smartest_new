package com.tr.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class FileUtil {
	private static final Logger LOG = LogManager.getLogger(FileUtil.class);

	public static String loadPropertyFromFile(String fileName, String key) {

		LOG.debug("loadPropertyFromFile");

		String value = null;

		Properties prop = new Properties();
		try {
			
			ClassLoader classLoader = FileUtil.class.getClassLoader();
			final InputStream resourceAsStream = classLoader.getResourceAsStream(fileName);
			prop.load(resourceAsStream);
			value = (String) prop.get(key);
		} catch (IOException e) {
			LOG.error(e);
		}
		return value;
	}

}