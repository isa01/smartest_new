package com.tr.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@SuppressWarnings("unused")
@Entity
@Table(name = "User")
@Data
public class User implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4731034817073695131L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "id")
	private int id;

	@Column(name = "user_name" , unique=true)
	private String user_name;
	
	@Column(name = "password")
	private String password;
	
	@Column(name = "user_auth")
	private BigDecimal userAuth;
	
	@Column(name = "mail_address" , unique=true)
	private String mailAddress;
	
	@Column(name = "tel_no")
	private String telNo;
	 
    @ManyToOne
    @JoinColumn(name = "company_id")
	private Company company;
		
	@Column(name = "status" ,  nullable = false, columnDefinition = "nvarchar(20) default 'Aktif'")
	private String status="Aktif";
	
	@Column(name="record_date")
	private Date  recordDate ;
	
	@Column(name="update_date")
	private Date updateDate ; 
	
	@Column(name="record_macadress")
	private String recordMacadress ; 
	
	@Column(name="update_macadress")
	private String updateMacadress ; 
	
	@Column(name="record_ipadress")
	private String recordIpadress ; 
	
	@Column(name="update_ipadress")
	private String updateIpadress ; 	
	 
	
	//getter setter 
   
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUser_name() {
		return user_name;
	}

	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}


	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Date getRecordDate() {
		return recordDate;
	}

	public void setRecordDate(Date recordDate) {
		this.recordDate = recordDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getRecordMacadress() {
		return recordMacadress;
	}

	public void setRecordMacadress(String recordMacadress) {
		this.recordMacadress = recordMacadress;
	}

	public String getUpdateMacadress() {
		return updateMacadress;
	}

	public void setUpdateMacadress(String updateMacadress) {
		this.updateMacadress = updateMacadress;
	}

	public String getRecordIpadress() {
		return recordIpadress;
	}

	public void setRecordIpadress(String recordIpadress) {
		this.recordIpadress = recordIpadress;
	}

	public String getUpdateIpadress() {
		return updateIpadress;
	}

	public void setUpdateIpadress(String updateIpadress) {
		this.updateIpadress = updateIpadress;
	}

	public String getMailAddress() {
		return mailAddress;
	}

	public void setMailAddress(String mailAddress) {
		this.mailAddress = mailAddress;
	}

	public String getTelNo() {
		return telNo;
	}

	public void setTelNo(String telNo) {
		this.telNo = telNo;
	}

	public BigDecimal getUserAuth() {
		return userAuth;
	}

	public void setUserAuth(BigDecimal userAuth) {
		this.userAuth = userAuth;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	
	
	
	
     
}
