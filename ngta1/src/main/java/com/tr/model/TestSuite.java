package com.tr.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;


import lombok.Data;

@Entity
@Table(name = "TestSuite")
@Data
public class TestSuite implements Serializable {

	private static final long serialVersionUID = -5935666406473115588L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;

	@Column(name = "name")
	private String name;
	
	@Column(name = "folderUrl")
	private String folderUrl;

	@ManyToMany(targetEntity =TestCase.class,fetch = FetchType.EAGER)
	@Column(unique=false)
	private List<TestCase> testCaseList=new ArrayList<TestCase>();

	@Column(name = "status" ,  nullable = false, columnDefinition = "nvarchar(20) default 'Aktif'")
	private String status="Aktif";

	
	@Column(name = "created_by")
	private String created_by;
	
	@Column(name = "updated_by")
	private String updated_by;
	
    @Column(name = "company_id")
	private int company_id;
	//getter setter
	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFolderUrl() {
		return folderUrl;
	}

	public void setFolderUrl(String folderUrl) {
		this.folderUrl = folderUrl;
	}

	

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public List<TestCase> getTestCaseList() {
		return testCaseList;
	}

	public void setTestCaseList(List<TestCase> testCaseList) {
		this.testCaseList = testCaseList;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCreated_by() {
		return created_by;
	}

	public void setCreated_by(String created_by) {
		this.created_by = created_by;
	}

	public String getUpdated_by() {
		return updated_by;
	}

	public void setUpdated_by(String updated_by) {
		this.updated_by = updated_by;
	}

	public int getCompany_id() {
		return company_id;
	}

	public void setCompany_id(int company_id) {
		this.company_id = company_id;
	}

	@Override
	public String toString(){
		return "TestSuite";
	}
	

}
