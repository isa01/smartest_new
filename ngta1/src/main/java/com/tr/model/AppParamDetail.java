package com.tr.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "AppParameterDetail") 
public class AppParamDetail  implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -674020861199122325L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
  	
	@Column(name="action_type")
	private String action_type;
	
//	@Column(name="priority")
//	private String priority;

	@Column(name="tag_name")
	private String tag_name;

	@Column(name="attr_name")
	private String attr_name;
	
	@Column(name="attr_value")
	private String attr_value;
	
	@Column(name="status")
	private String status="Aktif";
	
	@Column(name="platform")
	private String platform="WEB";
	
	@Column(name="script")
	private String script;
	
	@Column(name="fail_count")
	private int fail_count = 0;
	
	@Column(name="execute_count")
	private int execute_count = 0;
	
	@Column(name="last_run_status")
	private String last_run_status = "N/A";
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
 
	public String getAction_type() {
		return action_type;
	}

	public void setAction_type(String action_type) {
		this.action_type = action_type;
	}


	public String getAttr_name() {
		return attr_name;
	}

	public void setAttr_name(String attr_name) {
		this.attr_name = attr_name;
	}

	public String getAttr_value() {
		return attr_value;
	}

	public void setAttr_value(String attr_value) {
		this.attr_value = attr_value;
	}

	public String getTag_name() {
		return tag_name;
	}

	public void setTag_name(String tag_name) {
		this.tag_name = tag_name;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPlatform() {
		return platform;
	}

	public void setPlatform(String platform) {
		this.platform = platform;
	}

	public String getScript() {
		return script;
	}

	public void setScript(String script) {
		this.script = script;
	}

	public int getFail_count() {
		return fail_count;
	}

	public void setFail_count(int fail_count) {
		this.fail_count = fail_count;
	}

	public int getExecute_count() {
		return execute_count;
	}

	public void setExecute_count(int execute_count) {
		this.execute_count = execute_count;
	}

	public String getLast_run_status() {
		return last_run_status;
	}

	public void setLast_run_status(String last_run_status) {
		this.last_run_status = last_run_status;
	}
	
	

	 
}
