package com.tr.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.Data;

@Entity
@Table(name = "Defect") 
public class Defect implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4731034817073695131L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;

    @Column(name = "created_by")
 	private String created_by;
	
    @Column(name="created_date")
	private Date  created_date ;
    
    @Column(name="close_date")
   	private Date  close_date ;
	
	@Column(name = "description" , length = 12000, columnDefinition = "text")
	private String description;
	
	@Column(name = "failed_test_case")
	private int failed_test_case;
	
	@Column(name = "failed_test_suite")
	private int failed_test_suite;

	@Column(name = "status")
	private String status="Aktif";

	@Column(name = "severity")
	private String severity;
	
	
	@Column(name = "run_id")
	private int runId;
	
	@Column(name="state")
	private String state ;
	
	@Column(name = "update_by")
 	private String update_by;
	
    @Column(name="update_date")
	private Date  update_date ;

    
    @Column(name="task_id")
    private int taskId ;
    
    @Column(name="assign_user")
    private String assign_user ;
    
    
    
	//getter setter 
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}


	public String getCreated_by() {
		return created_by;
     }

	public void setCreated_by(String created_by) {
		this.created_by = created_by;
	}

	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSeverity() {
		return severity;
	}

	public void setSeverity(String severity) {
		this.severity = severity;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public int getFailed_test_case() {
		return failed_test_case;
	}

	public void setFailed_test_case(int failed_test_case) {
		this.failed_test_case = failed_test_case;
	}

	public Date getCreated_date() {
		return created_date;
	}

	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}

	public int getFailed_test_suite() {
		return failed_test_suite;
	}

	public void setFailed_test_suite(int failed_test_suite) {
		this.failed_test_suite = failed_test_suite;
	}

	public int getRunId() {
		return runId;
	}

	public void setRunId(int runId) {
		this.runId = runId;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getUpdate_by() {
		return update_by;
	}

	public void setUpdate_by(String update_by) {
		this.update_by = update_by;
	}

	public Date getUpdate_date() {
		return update_date;
	}

	public void setUpdate_date(Date update_date) {
		this.update_date = update_date;
	}

	
	
	public int getTaskId() {
		return taskId;
	}

	public void setTaskId(int taskId) {
		this.taskId = taskId;
	}

	public Date getClose_date() {
		return close_date;
	}

	public void setClose_date(Date close_date) {
		this.close_date = close_date;
	}

	public String getAssign_user() {
		return assign_user;
	}

	public void setAssign_user(String assign_user) {
		this.assign_user = assign_user;
	}

 

	
	
}
