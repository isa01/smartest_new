package com.tr.model;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn; 
import javax.persistence.ManyToOne; 
import javax.persistence.OneToMany; 
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.Data;

@Entity
@Table(name = "TestStep")
@Data
public class TestStep implements Serializable, Comparable<TestStep> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 959439448589351353L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;

	@Column(name = "Param1")
	private String param1;

//	@Column(name = "testcase_id")
//    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
//    @JoinColumn(name = "testcase_id")
//	private TestCase testCase;
	
	@Column(name = "template_id")
	private int templateId;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "appParameter_id")
	private AppParameter appParameter;
    
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "stepValidation_id")
	private Set<StepValidation> stepValidationList;

	@Column(name = "ordernum")
	private Integer orderNum;

	@Column(name = "type")
	private String type;
	
	@Column(name = "description")
	private String description;
	
	@Transient
	private String page;
	
	@Column(name = "action_type")
	private String action_type;
	
	@Column(name = "status", nullable = false, columnDefinition = "nvarchar(20) default 'Aktif'")
	private String status = "Aktif";
	

	@Column(name = "defaultValue")
	private String defaultValue ;
	
//	  @Override
//	  public int compareTo(TestStep u) {
//	    if (getCreatedOn() == null || u.getCreatedOn() == null) {
//	      return 0;
//	    }
//	    return getCreatedOn().compareTo(u.getCreatedOn());
//	  }
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getParam1() {
		return param1;
	}

	public void setParam1(String param1) {
		this.param1 = param1;
	}
 

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}


	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getTemplateId() {
		return templateId;
	}

	public void setTemplateId(int templateId) {
		this.templateId = templateId;
	}

	public String getPage() {
		return page;
	}

	public void setPage(String page) {
		this.page = page;
	}

	public String getAction_type() {
		return action_type;
	}

	public void setAction_type(String action_type) {
		this.action_type = action_type;
	}

//	public TestCase getTestCase() {
//		return testCase;
//	}
//
//	public void setTestCase(TestCase testCase) {
//		this.testCase = testCase;
//	}

	public AppParameter getAppParameter() {
		return appParameter;
	}

	public void setAppParameter(AppParameter appParameter) {
		this.appParameter = appParameter;
	}

	public String getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	public void setOrderNum(Integer orderNum) {
		this.orderNum = orderNum;
	}

	public Integer getOrderNum() {
		return orderNum;
	}

	@Override
	public int compareTo(TestStep ts1 ) {
		return this.getOrderNum().compareTo(ts1.getOrderNum());
	}

	public Set<StepValidation> getStepValidationList() {
		return stepValidationList;
	}

	public void setStepValidationList(Set<StepValidation> stepValidationList) {
		this.stepValidationList = stepValidationList;
	}

 
 
 


}
