package com.tr.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.Data;

@Entity
@Table(name = "Folder") 
public class Folder implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4731034817073695131L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	
	@Column(name = "name")
	private String name;

	@Column(name = "url")
	private String folder_url;
//	
    @Column(name = "company_id")
	private int company_id;
    
//	@ManyToOne(fetch=FetchType.EAGER)
//	@JoinColumn(name="created_by", insertable=false, updatable=false)
//	private User creater_name;
//	
//	@Column(name = "description")
//	private String description;
//	
	@Column(name = "status")
	private String status="Aktif";
	
	
	@Column(name="user_roles")
	private int userRoles=1 ;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFolder_url() {
		return folder_url;
	}

	public void setFolder_url(String folder_url) {
		this.folder_url = folder_url;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getUserRoles() {
		return userRoles;
	}

	public void setUserRoles(int userRoles) {
		this.userRoles = userRoles;
	}

	public int getCompany_id() {
		return company_id;
	}

	public void setCompany_id(int company_id) {
		this.company_id = company_id;
	}
	
	
	

}
