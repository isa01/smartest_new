package com.tr.model;

import java.util.LinkedList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OrderColumn;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.Data;

@NamedQueries({
		@NamedQuery(name = "findAllNodesWithTheirChildren", query = "select n from TestHierarchyNode n left join fetch n.child") })

@Entity
@Table(name = "TestHierarchyTree")
@Data
public class TestHierarchyNode {

	@Id
	@GeneratedValue
	private Integer id;

	@NotNull
	private String folderName;

	@OneToMany
	@JoinColumn(name = "parent_id")
	@OrderColumn
	private List<TestHierarchyNode> child = new LinkedList<TestHierarchyNode>();

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFolderName() {
		return folderName;
	}

	public void setFolderName(String folderName) {
		this.folderName = folderName;
	}

	public List<TestHierarchyNode> getChild() {
		return child;
	}

	public void setChild(List<TestHierarchyNode> child) {
		this.child = child;
	}
}
