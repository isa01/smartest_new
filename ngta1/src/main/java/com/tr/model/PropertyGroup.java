package com.tr.model;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.servlet.http.HttpSession;

import lombok.Data;

@Entity
@Table(name = "PropertyGroup")
@Data
public class PropertyGroup implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3588569863954002186L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;

	// @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER, targetEntity =
	// TestCase.class)
	// @JoinColumn(name = "testcase_id")
	// private TestCase testCase;
	
	@OneToMany(targetEntity = TestCaseProperty.class,fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "propertyGroup_id")
	private Set<TestCaseProperty> testCasePropertyList;
 	
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER, targetEntity = TestCase.class)
	@JoinColumn(name = "testCase_id")
	private TestCase testCase;

	@Column(name = "description", length = 1024, columnDefinition = "text")
	private String description;

	@Column(name = "status", nullable = false, columnDefinition = "nvarchar(20) default 'Aktif'")
	private String status = "Aktif";

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER, targetEntity = User.class)
	@JoinColumn(name = "created_by")
	private User created_by;
	 
	@Column(name = "is_default", nullable = false)
	private int defaultPG = 0;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
  
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public User getCreated_by() {
		return created_by;
	}

	public void setCreated_by(User created_by) {
		this.created_by = created_by;
	}

	public TestCase getTestCase() {
		return testCase;
	}

	public void setTestCase(TestCase testCase) {
		this.testCase = testCase;
	}

	public Set<TestCaseProperty> getTestCasePropertyList() {
		return testCasePropertyList;
	}

	public void setTestCasePropertyList(Set<TestCaseProperty> testCasePropertyList) {
		this.testCasePropertyList = testCasePropertyList;
	}

	public int getDefaultPG() {
		return defaultPG;
	}

	public void setDefaultPG(int defaultPG) {
		this.defaultPG = defaultPG;
	}
 
}
