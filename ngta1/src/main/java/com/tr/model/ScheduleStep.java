package com.tr.model;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "ScheduleStep")
public class ScheduleStep implements Serializable,  Comparable<ScheduleStep>  {
	
	@Transient
	DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	
	@Column(name = "testStep_id")
	private int testStep_id;
	
	@Column(name = "status")
	private String status="WAITING";
	
	@Column(name = "description", length = 12000, columnDefinition = "text")
	private String description;
	
	@Column(name = "name", length = 1200, columnDefinition = "text")
	private String name;
	
	@Column(name="ss_url")
	private String ss_url;
	
	@Column(name = "execuion_date", length = 1024, columnDefinition = "text")
	private String execuion_date;
	
	@Column(name = "ordernum")
	private Integer orderNum;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getTestStep_id() {
		return testStep_id;
	}

	public void setTestStep_id(int testStep_id) {
		this.testStep_id = testStep_id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSs_url() {
		return ss_url;
	}

	public void setSs_url(String ss_url) {
		this.ss_url = ss_url;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public int compareTo(ScheduleStep ss) {
		return this.getOrderNum().compareTo(ss.getOrderNum());
	}

	public Integer getOrderNum() {
		return orderNum;
	}

	public void setOrderNum(Integer orderNum) {
		this.orderNum = orderNum;
	}

	public String getExecuion_date() {
		return execuion_date;
	}

	public void setExecuion_date(String execuion_date) {
		this.execuion_date = execuion_date;
	}

}
