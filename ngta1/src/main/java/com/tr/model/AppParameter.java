package com.tr.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Entity
@Table(name = "AppParameter")
public class AppParameter implements Serializable{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;

	@Column(name = "page", length = 1024, columnDefinition = "text")
	private String page = "default";

	@Column(name = "platform")
	private String platform = "WEB";

	@Column(name = "description", length = 1024, columnDefinition = "text")
	private String description;

	@Column(name = "type")
	private String type = "TestStep";

	@Column(name = "status", nullable = false, columnDefinition = "nvarchar(20) default 'Aktif'")
	private String status = "Aktif";

	@OneToMany(targetEntity = AppParamDetail.class,fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "appParameter_id")
	private Set<AppParamDetail> appParamDetailList = new HashSet<AppParamDetail>();

	@Column(name = "company_id")
	private int company_id;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPage() {
		return page;
	}

	public void setPage(String page) {
		this.page = page;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPlatform() {
		return platform;
	}

	public void setPlatform(String platform) {
		this.platform = platform;
	}

	public Set<AppParamDetail> getAppParamDetailList() {
		return appParamDetailList;
	}

	public void setAppParamDetailList(Set<AppParamDetail> appParamDetailList) {
		this.appParamDetailList = appParamDetailList;
	}

	public int getCompany_id() {
		return company_id;
	}

	public void setCompany_id(int company_id) {
		this.company_id = company_id;
	}
}
