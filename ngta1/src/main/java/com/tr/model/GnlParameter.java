package com.tr.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

@Entity
@Table(name = "Gnl_Parameter")
@Data
public class GnlParameter implements Serializable {
	/**
	 * genel parametrelin bulunduğu sınıf
	 */
	private static final long serialVersionUID = 4731034817073695131L;
	
	@Id
	@GeneratedValue
	@Column(name = "id")
	private BigDecimal id ;
	
	
	@Column(name = "name" )
	private String name ;
	
	@Column(name = "value", columnDefinition = "nvarchar(1255)")
	private String value ;
	
	@Column(name = "description")
	private String description ;
	
	@Column(name = "created_date")
	private Date createdDate ;
	
	
	
	@Column(name = "create_by")
	private int createBy ;
	
	
	@Column(name = "status" ,  nullable = false, columnDefinition = "nvarchar(20) default 'Aktif'")
	private String status="Aktif";
	
	//getter setter 
	
	public BigDecimal getId() {
		return id;
	}
	public void setId(BigDecimal id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	
	public int getCreateBy() {
		return createBy;
	}
	public void setCreateBy(int createBy) {
		this.createBy = createBy;
	}
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
	
	
	

}
