package com.tr.model;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.servlet.http.HttpSession;

import lombok.Data;

@Entity
@Table(name = "TestCase")

@Data public class TestCase implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3588569863954002186L;
	
	@Transient
	private String def_open_page = "http://10.70.82.29:8092/#/login";
	@Transient
	transient HttpSession session ;
	 
    @Column(name = "company_id")
	private int company_id;
////	
//	public TestCase() {
//		FacesContext context = FacesContext.getCurrentInstance();
//	if(context == null)
//		company = null;
//	else
//	{
//		session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
//		company = (Company) session.getAttribute("company");
//	}
//	}  
//	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	
	@Column(name = "name")
	private String name;
	
	@OneToMany(targetEntity = TestStep.class,fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "testcase_id")
	private List<TestStep> testSteps;
	
 	@OneToOne( fetch = FetchType.EAGER, cascade = CascadeType.ALL) 
	@JoinColumn(name = "propertyGroupCatalog_id")
	private PropertyGroupCatalog propertyGroupCatalog;
 
	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL) 
	@JoinColumn(name = "defaultPropertyGroup_id")
	private PropertyGroup defaultPropertyGroup; 	
	
	@Column(name = "type_name")
	private String typeName = "Test";
	
	@Column(name = "tccontent")
	private String tcContent;
	
	@Column(name = "edited")
	private int edited;
	
	@Column(name = "folderUrl")
	private String folderUrl;
	
	@Column(name = "created_by")
	private String created_by;

	@Column(name = "updated_by")
	private String updated_by;
	
	@Column(name = "open_page")
	private String open_page = def_open_page;
	
	@Column(name = "description")
	private String description;
	
	@Column(name = "platform")
	private String platform ="WEB";
	
	@Column(name = "isTemplate")
	private boolean template = false;
	
	@Column(name = "status" ,  nullable = false, columnDefinition = "nvarchar(20) default 'Aktif'")
	private String status="Aktif";
	
//	@ManyToOne(targetEntity = TestStep.class,fetch = FetchType.EAGER, cascade = CascadeType.ALL)
//	@JoinColumn(name = "request_id")
//	public Request request;
	
	@Transient
	private List<TestCaseVariable> testCaseVariables;
	
	
	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<TestStep> getTestSteps() {
		return testSteps;
	}

	public void setTestSteps(List<TestStep> testSteps) {
		this.testSteps = testSteps;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public String getTcContent() {
		return tcContent;
	}

	public void setTcContent(String tcContent) {
		this.tcContent = tcContent;
	}

	public int getEdited() {
		return edited;
	}

	public void setEdited(int edited) {
		this.edited = edited;
	}

	public String getFolderUrl() {
		return folderUrl;
	}

	public void setFolderUrl(String folderUrl) {
		this.folderUrl = folderUrl;
	}

	public String getCreated_by() {
		return created_by;
	}

	public void setCreated_by(String created_by) {
		this.created_by = created_by;
	}

	public String getUpdated_by() {
		return updated_by;
	}

	public void setUpdated_by(String updated_by) {
		this.updated_by = updated_by;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getOpen_page() {
		return open_page;
	}

	public void setOpen_page(String open_page) {
		this.open_page = open_page;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isTemplate() {
		return template;
	}

	public void setTemplate(boolean template) {
		this.template = template;
	}

	public String getPlatform() {
		return platform;
	}

	public void setPlatform(String platform) {
		this.platform = platform;
	}
 
	public HttpSession getSession() {
		return session;
	}

	public void setSession(HttpSession session) {
		this.session = session;
	}

	public List<TestCaseVariable> getTestCaseVariables() {
		return testCaseVariables;
	}

	public void setTestCaseVariables(List<TestCaseVariable> testCaseVariables) {
		this.testCaseVariables = testCaseVariables;
	}

	public PropertyGroupCatalog getPropertyGroupCatalog() {
		return propertyGroupCatalog;
	}

	public void setPropertyGroupCatalog(PropertyGroupCatalog propertyGroupCatalog) {
		this.propertyGroupCatalog = propertyGroupCatalog;
	}

	public PropertyGroup getDefaultPropertyGroup() {
		return defaultPropertyGroup;
	}

	public void setDefaultPropertyGroup(PropertyGroup defaultPropertyGroup) {
		this.defaultPropertyGroup = defaultPropertyGroup;
	}

	public int getCompany_id() {
		return company_id;
	}

	public void setCompany_id(int company_id) {
		this.company_id = company_id;
	}

	@Override
	public String toString(){
		return "TestCase";
	}
 	
}
