package com.tr.model;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.servlet.http.HttpSession;

import lombok.Data;

@Entity
@Table(name = "TestCaseProperty")
@Data
public class TestCaseProperty implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3588569863954002186L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;


	@Column(name = "description", length = 1024, columnDefinition = "text")
	private String description;

	@Column(name = "property_key")
	private String key;

	@Column(name = "property_value")
	private String value;

	@Column(name = "status", nullable = false, columnDefinition = "nvarchar(20) default 'Aktif'")
	private String status = "Aktif";
	
	@Column(name = "type")
	private String type="FreeText";
	
	@Column(name = "inputouput")
	private String inputoutput="Input";
	
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER, targetEntity = User.class)
	@JoinColumn(name = "created_by")
	private User created_by;
 
	@Column(name = "sourceStr")
	private String sourceStr;
	
	@ManyToOne(cascade = CascadeType.ALL,  fetch = FetchType.EAGER,  targetEntity = TestStep.class)
	@JoinColumn(name = "testStep_id")
	private TestStep testStep;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public User getCreated_by() {
		return created_by;
	}

	public void setCreated_by(User created_by) {
		this.created_by = created_by;
	}
 
	public String getSourceStr() {
		return sourceStr;
	}

	public void setSourceStr(String sourceStr) {
		this.sourceStr = sourceStr;
	}

	public String getInputoutput() {
		return inputoutput;
	}

	public void setInputoutput(String inputoutput) {
		this.inputoutput = inputoutput;
	}

	public TestStep getTestStep() {
		return testStep;
	}

	public void setTestStep(TestStep testStep) {
		this.testStep = testStep;
	}

}
