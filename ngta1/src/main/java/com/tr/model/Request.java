package com.tr.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "Request") 
public class Request implements Serializable{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;

	@Column(name = "service_name", length = 1024, columnDefinition = "text")
	private String service_name;
	
	@Column(name = "url", length = 1024, columnDefinition = "text")
	private String url="http://10.70.82.29:8092/rest/delivery/sendDeliveryOrder";
	
	@Column(name = "request_text", length = 10240, columnDefinition = "text")
	private String request_text;
	 
	@Column(name = "description", length = 1024, columnDefinition = "text")
	private String description;
	
	@Column(name = "type")
	private String type="REST";
	
	@Column(name = "username")
	private String username="hbintegration";
	
	@Column(name = "password")
	private String password="admin123";

	@Column(name = "status", nullable = false, columnDefinition = "nvarchar(20) default 'Aktif'")
	private String status = "Aktif";
	
	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL) 
	@JoinColumn(name = "propertyGroupCatalog_id")
	private PropertyGroupCatalog propertyGroupCatalog;
	
	@Transient
	private List<RequestProperty> requestPropertyList = new ArrayList<RequestProperty>();

	@OneToMany(cascade=CascadeType.ALL,fetch=FetchType.EAGER,mappedBy="request")
	private List<Request_Header> headers = new ArrayList<Request_Header>();
	 
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
 
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getService_name() {
		return service_name;
	}

	public void setService_name(String service_name) {
		this.service_name = service_name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
 
	public String getRequest_text() {
		return request_text;
	}

	public void setRequest_text(String request_text) {
		this.request_text = request_text;
	}

	public List<RequestProperty> getRequestPropertyList() {
		return requestPropertyList;
	}

	public void setRequestPropertyList(List<RequestProperty> requestPropertyList) {
		this.requestPropertyList = requestPropertyList;
	}

	public List<Request_Header> getHeaders() {
		return headers;
	}

	public void setHeaders(List<Request_Header> headers) {
		this.headers = headers;
	}

	public PropertyGroupCatalog getPropertyGroupCatalog() {
		return propertyGroupCatalog;
	}

	public void setPropertyGroupCatalog(PropertyGroupCatalog propertyGroupCatalog) {
		this.propertyGroupCatalog = propertyGroupCatalog;
	}


 
}
