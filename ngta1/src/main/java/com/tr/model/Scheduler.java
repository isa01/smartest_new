package com.tr.model;

import javax.persistence.*;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.Set;

@Entity
@Table(name = "Scheduler")
public class Scheduler  implements Serializable  {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Transient
	DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	@Transient
	Date date = new Date(); 
//	HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	
	@Column(name = "testcase_id", length = 1024, columnDefinition = "text")
	private int testcase_id;

	@Column(name = "created_by", length = 1024, columnDefinition = "text")
	private String created_by = "Test";// (String) session.getAttribute("username");
	
	@Column(name = "created_date", length = 1024, columnDefinition = "text")
	private String created_date = dateFormat.format(date);
	
	@Column(name = "run_date", length = 1024, columnDefinition="text")
	private String run_date = dateFormat.format(date);
	
	@Column(name = "intervals", length = 1024, columnDefinition = "text")
	private String interval;
	
	@Column(name = "environment", length = 1024, columnDefinition = "text")
	private String environment;
	
	@Column(name = "browser", length = 1024, columnDefinition = "text")
	private String browser;
	
	@Column(name = "status" ,  nullable = false, columnDefinition = "nvarchar(20) default 'Aktif'")
	private String status="Aktif";
	
	@Column(name ="testcase_name", length = 1024, columnDefinition = "text")
	private String testcase_name;
	
	@Column(name ="startURL", length = 1024, columnDefinition = "text")
	private String startURL;
	
	@Column(name ="logs", length = 1024, columnDefinition = "text")
	private String logs;

	@Column(name ="type", length = 1024, columnDefinition = "text")
	private String type = "Regular";

    @Column(name = "company_id")
	private int company_id;
	
    @ManyToOne(targetEntity = PropertyGroup.class)
    @JoinColumn(name = "propertyGroup_id")
	private PropertyGroup propertyGroup;
    
	@OneToMany(targetEntity = ScheduleStep.class,fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "scheduler_id")
	private Set<ScheduleStep> scheduleSteps= new  LinkedHashSet<ScheduleStep>();
    
    private int schedulerid;


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
 
	public String getCreated_by() {
		return created_by;
	}

 	public void setCreated_by(String created_by) {
		this.created_by = created_by;
	}
 
	public String getCreated_date() {
		return created_date;
	}
 
	public void setCreated_date(String created_date) {
		this.created_date = created_date;
	}
 
	public String getRun_date() {
		return run_date;
	}

 	public void setRun_date(String run_date) {
		this.run_date = run_date;
	}
 
	public String getInterval() {
		return interval;
	}

 	public void setInterval(String interval) {
		this.interval = interval;
	}

 	public String getEnvironment() {
		return environment;
	}
 
	public void setEnvironment(String environment) {
		this.environment = environment;
	}
 
	public String getBrowser() {
		return browser;
	}
 
	public void setBrowser(String browser) {
		this.browser = browser;
	}
 
	public String getStatus() {
		return status;
	}

 	public void setStatus(String status) {
		this.status = status;
	}

 	public int getTestcase_id() {
		return testcase_id;
	}
 
 	public void setTestcase_id(int testcase_id) {
		this.testcase_id = testcase_id;
	}
 
	public String getTestcase_name() {
		return testcase_name;
	}
 
	public void setTestcase_name(String testcase_name) {
		this.testcase_name = testcase_name;
	}
 
	public PropertyGroup getPropertyGroup() {
		return propertyGroup;
	}

 	public void setPropertyGroup(PropertyGroup propertyGroup) {
		this.propertyGroup = propertyGroup;
	}

 	public int getSchedulerid() {
		return schedulerid;
	}
 
	public void setSchedulerid(int schedulerid) {
		this.schedulerid = schedulerid;
	}
 
	public String getStartURL() {
		return startURL;
	}
 
	public void setStartURL(String startURL) {
		this.startURL = startURL;
	}

 	public String getLogs() {
		return logs;
	}

 	public void setLogs(String logs) {
		this.logs = logs;
	}

 	public Set<ScheduleStep> getScheduleSteps() {
		return scheduleSteps;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setScheduleSteps(LinkedHashSet<ScheduleStep> scheduleSteps) {
		this.scheduleSteps = scheduleSteps;
	}

	public int getCompany_id() {
		return company_id;
	}

	public void setCompany_id(int company_id) {
		this.company_id = company_id;
	}


 
}