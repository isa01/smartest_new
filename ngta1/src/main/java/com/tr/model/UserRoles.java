package com.tr.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;

@SuppressWarnings("unused")
@Entity
@Table(name = "UserRoles")
@Data
public class UserRoles implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4731034817073695131L;
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "id")
	private BigDecimal id ;
	
	@Column(name="user_roles")
	private String user_roles ;

	@ManyToMany(fetch = FetchType.LAZY)
	private List<UserAuthorization> user_auth=new ArrayList<UserAuthorization>() ;
	
	
	//getter setter
	public BigDecimal getId() {
		return id;
	}

	public void setId(BigDecimal id) {
		this.id = id;
	}

	public String getUser_roles() {
		return user_roles;
	}

	public void setUser_roles(String user_roles) {
		this.user_roles = user_roles;
	}

	public List<UserAuthorization> getUser_auth() {
		return user_auth;
	}

	public void setUser_auth(List<UserAuthorization> user_auth) {
		this.user_auth = user_auth;
	}

	
	
	
	 
	
	
}
