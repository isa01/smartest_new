package com.tr.service;

import java.io.Serializable;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tr.dao.DefectDao;
import com.tr.dao.PreferencesDao;
import com.tr.model.Comments;
import com.tr.model.Defect;

@Service
@Transactional
public class DefectServices implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Resource
	private DefectDao defectDao ;
 	
	//Defect 
	
		public List<Defect> getListDefect(){
			
			
			return defectDao.getListDefect() ;
		}
		
		public void createDefect(Defect t)
		{
			defectDao.createDefect(t);
		}
		
		public void removeCommentOnDefect(Defect d , Comments c){
			
			defectDao.removeCommentOnDefect(d, c);
		}
		
		public void saveComment(List<Comments> list)
		{
			defectDao.saveComment(list);
		}
		
		public List<Comments> getCommentList(Defect d){
			return defectDao.getCommentList(d);
		}
		
		public List<Comments> getCommentList(){
			return defectDao.getCommentList();
		}
	
	//getter setter 
	
}
