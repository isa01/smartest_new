package com.tr.service;

import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.List;

import javax.annotation.Resource;
import javax.faces.bean.SessionScoped;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tr.dao.PreferencesDao;
import com.tr.model.Action;
import com.tr.model.AppParamDetail;
import com.tr.model.AppParameter;
import com.tr.model.FindElementMethod;
import com.tr.model.Folder;
import com.tr.model.PreparedUrl;
import com.tr.model.Request;
import com.tr.model.Request_Header;
import com.tr.model.TestStep;
import com.tr.model.UserRoles;

@Service
@Transactional
@SessionScoped
public class PreferencesService implements Serializable {

	private static final long serialVersionUID = 1L;

	@Resource
	@Autowired
	private PreferencesDao preferencesDao;

	transient StringWriter sw = null;
	transient PrintWriter pw = null;

//	public String runRequest(Request request, Map<String, String> propertyList) {
//
//		RestClient rc = new RestClient();
//
//		// girilen değerin json veya xml olup olmadığının denetlenmesi ve değerin formatının doğruluğunun kontrolünün yapılması gerekli 
//		String jsonContext = request.getRequest_text();
//
//		String output = "";
//
//		boolean isExistAuthorization = (request.getUsername() != null && request.getPassword() != null);
//
//		if (isExistAuthorization) {
//
//			if (request.getType().equals("GET")) {
//				
//				try {
//					ResponseEntity<String> response = rc.getRequestByBasicAuthentication(request.getUrl(), request.getUsername(), request.getPassword());
//					output = response.toString();	
//				} catch (RestClientException e) {
//					output = e.toString();
//					e.printStackTrace();
//				}
//
//				
//			}
//
//			if (request.getType().equals("POST")) {
//
//				Set<TestCaseProperty> requestHeaders = request.getPropertyGroupCatalog().getTestCasePropertyList();
//
//				if (requestHeaders != null) {
//
//					MultiValueMap<String, String> mvm = new LinkedMultiValueMap<>();
//
//					for (TestCaseProperty request_Header : requestHeaders) {
//
//						String headerKey = request_Header.getKey();
//						String headerValue = request_Header.getValue();
//						mvm.add(headerKey, headerValue);
//					}
//
//					try {
//						ResponseEntity<String> response = rc.postRequestBasicAuthenticationByJson(request.getUsername(), request.getPassword(),
//								request.getUrl(), jsonContext, mvm);
//						output = response.getStatusCode() + " " + response.getBody();
//					} catch (RestClientException e) {
//						output = e.toString();
//						e.printStackTrace();
//					}
//				}
//			}
//		}
//
//		return output;

//		String input = request.getRequest_text();
//		for(TestCaseProperty tcp : request.getPropertyGroupCatalog().getTestCasePropertyList())
//			input = input.replace("["+tcp.getKey()+"]", tcp.getValue());
//		String output="";
//		List<Header> headers = new ArrayList<Header>();
//		Header header;
//		List<Request_Header> rh_list = preferencesDao.getHeadersByRequest(request.getId());
//		for(Request_Header rh : rh_list) {
//			header = new BasicHeader(rh.getKey(),rh.getValue());
//			headers.add(header);
//		}
//		HttpResponse response = null;
//		StringEntity entity = null;
//		try {
//			entity = new StringEntity(input);
//		} catch (UnsupportedEncodingException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//			output=getStackTrace(e1);
//			return output;
//		}
//		
////		   URL url = new URL(request.getUrl());
////
////		    String authStr = "MyAPIKey"+":"+"Password";
////		    System.out.println("Original String is " + authStr);
////
////		 // encode data on your side using BASE64
////		    byte[] bytesEncoded = Base64.encodeBase64(authStr .getBytes());
////		    String authEncoded = new String(bytesEncoded);
////
////		    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
////		    connection.setRequestProperty("Authorization", "Basic "+authEncoded);
//		    
//		  //  String url = request.getUrl();
//		    String charset = "UTF-8";  // Or in Java 7 and later, use the constant: java.nio.charset.StandardCharsets.UTF_8.name()
//		    String param1 = "value1";
//		    String param2 = "value2";
//		    // ...	    
//		    
//		    
//
//		    String query;
//			try {
//				query = String.format("param1=%s&param2=%s", 
//				     URLEncoder.encode("Username", propertyList.get("username")), 
//				     URLEncoder.encode("Password",  propertyList.get("password")));
//
//		    URLConnection connection = new URL(url + "?" + query).openConnection();
//		    connection.setRequestProperty("Accept-Charset", charset);
//		    InputStream resp = connection.getInputStream();
//		    try (Scanner scanner = new Scanner(resp)) {
//		        String responseBody = scanner.useDelimiter("\\A").next();
//		        System.out.println(responseBody);
//		    }
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		    
//		
////		CredentialsProvider provider = new BasicCredentialsProvider();
////		UsernamePasswordCredentials credentials = new UsernamePasswordCredentials(propertyList.get("username"), propertyList.get("password"));
////		provider.setCredentials(AuthScope.ANY, credentials); 
////
////		HttpClient httpClient = HttpClientBuilder.create().setDefaultCredentialsProvider(provider)
////				.setDefaultHeaders(headers).build();
////		HttpPost postRequest = new HttpPost(request.getUrl());
////		postRequest.addHeader("content-type", "application/json");
////		postRequest.setEntity(entity);
////
////		try {
////			response = httpClient.execute(postRequest);
////		} catch (IOException e1) {
////			// TODO Auto-generated catch block
////			e1.printStackTrace();
////			output=getStackTrace(e1);
////			return output;
////		}
////		System.out.println(request.toString());
////		System.out.println(response.toString());
////		try {
////
////			String authString = "hbintegration:admin123";
////			byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
////			String authStringEnc = new String(authEncBytes);
////			URL url = new URL("http://10.70.82.29:8092/auth/getToken");
////			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
////			conn.setRequestProperty("Authorization", "Basic " + authStringEnc);
////			conn.setRequestMethod("POST");
////			conn.setRequestProperty("Accept", "application/json");
////
////			if (conn.getResponseCode() != 200) {
////				throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
////			}
////
////			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
////
////			
////			System.out.println("Output from Server .... \n");
////			while ((output = br.readLine()) != null);
////		} catch (MalformedURLException e) {
////
////			e.printStackTrace();
////			output=getStackTrace(e);
////
////		} catch (IOException e) {
////
////			e.printStackTrace();
////			output=getStackTrace(e);
////
////		}
//		return output;

//	}

	// url list
//
//	// parametre listesi ve url listesi alma & kaydetme
//	public void getElementList(PreparedUrl pu) {
//
//		try {
//			Document doc;
//			if (pu.getUrl().contains("http")) {
//				doc = Jsoup.connect(pu.getUrl()).validateTLSCertificates(false).get();
//			} else {
//				File input = new File(pu.getUrl());
//				doc = Jsoup.parse(input, "UTF-8", "");
//			}
//			List<AppParameter> appParameters = new ArrayList<AppParameter>();
//			Elements el = doc.getElementsByTag("a");
//			Iterator<Element> e = el.iterator();
//			while (e.hasNext()) {
//				Element x = e.next();
//				if (!x.text().contains("<")) {
//					if (x.text().length() > 0) {
//						AppParameter ap = new AppParameter();
//						ap.setPage(pu.getPage());
//						ap.setDescription(x.text() + " linkine tıkla");
//						// ap.setValue("driver.findElement(By.name("
//						// + "\"" + x.text() + "\")).click();");
//						appParameters.add(ap);
//					}
//				}
//			}
//
//			el = doc.getElementsByTag("input");
//			e = el.iterator();
//			while (e.hasNext()) {
//				Element x = e.next();
//				AppParameter ap = new AppParameter();
//				ap.setPage(pu.getPage());
//				if (x.attr("type").contains("submit") || x.attr("type").contains("checkbox")) {
//					ap.setDescription(x.attr("name") + " butonuna tıkla...");
//					// ap.setValue("driver.findElement(By.xpath("
//					// + "\"//*[@name='" + x.attr("name") + "']\""
//					// + ")).click();");
//				} else {
//					for (Attribute attribute : x.attributes()) {
//						AppParameter ap1 = new AppParameter();
//						ap1.setPage(pu.getPage());
//						ap1.setDescription(attribute.getValue() + " alanına içerik gir...");
//						// ap1.setValue("driver.findElement(By.xpath("+
//						// "\"//*[@"+attribute.getKey()+"='" + attribute.getValue() + "']\""
//						// + ")).sendKeys(" + "\"$param1\");");
//						appParameters.add(ap1);
//					}
//
//				}
//
//			}
//
//			el = doc.getElementsByTag("button");
//			e = el.iterator();
//			while (e.hasNext()) {
//				Element x = e.next();
//				if (!x.html().contains("<")) {
//					AppParameter ap = new AppParameter();
//					ap.setPage(pu.getPage());
//					ap.setDescription(x.html() + " butonuna tıkla...");
//					// ap.setValue("driver.findElement(By.xpath("
//					// + "\"//*[@class='" + x.className() + "']\""
//					// + ")).click();");
//					appParameters.add(ap);
//				}
//			}
//			AppParameter ap = new AppParameter();
//			ap.setPage(pu.getPage());
//			ap.setDescription(pu.getPage() + " Sayfasını aç");
//			// ap.setValue("driver.get(" + "\"" + pu.getUrl() + "\");");
//			appParameters.add(ap);
//
//			// save
//			saveParameters(appParameters);
//			saveUrl(pu);
//
//		} catch (Exception ex) {
//
//		}
//
//	}

	public void saveParameters(List<AppParameter> appParameters) {
		preferencesDao.saveParameters(appParameters);
	}

	public void saveAppParamDetail(AppParamDetail appParamDetail) {
		preferencesDao.saveAppParamDetail(appParamDetail);

	}

	public int saveParameter(AppParameter appParameter) {
		return preferencesDao.saveParameter(appParameter);
	}

//	public void saveAppParamDetailList(List<AppParamDetail> appParamDetailList) {
//		preferencesDao.saveAppParamDetailList(appParamDetailList);
//
//	}

//	public List<AppParamDetail> getAppParamDetByAppParamId(int app_param_id) {
//		return preferencesDao.getAppParamDetByAppParamId(app_param_id);
//	}

	public AppParameter getAppParameterById(int old_java_code_app_det2) {
		// TODO Auto-generated method stub
		return preferencesDao.getAppParameterById(old_java_code_app_det2);
	}

	public void saveUrl(PreparedUrl url) {
		preferencesDao.saveUrl(url);
	}

	public List<PreparedUrl> getUrl() {

		return preferencesDao.getUrl();
	}

	public void deleteUrl(PreparedUrl url) {
		preferencesDao.deleteUrl(url);
	}

	public TestStep getTestStepById(int id) {
		return preferencesDao.getTestStepById(id);
	}

	// test step

	public List<AppParameter> getAppParamaters() {
		return preferencesDao.getAppParamaters();
	}

	public void removeParam(AppParameter app) {
		preferencesDao.removeParam(app);
	}

	public void removeParamDetail(AppParamDetail app) {
		preferencesDao.removeParamDetail(app);
	}

	public List<FindElementMethod> getFindElementMethodList() {

		return preferencesDao.getFindElementMethodList();
	}

	public List<String> getPageFilter() {
		return preferencesDao.pagesFilter();
	}

	public List<AppParameter> getPageFilter(String page) {
		return preferencesDao.pagesFilter(page);
	}

	public String getStackTrace(final Throwable throwable) {
		sw = new StringWriter();
		pw = new PrintWriter(sw, true);
		throwable.printStackTrace(pw);
		return sw.getBuffer().toString().substring(0, 500);
	}

	// public void selectedPageChanged(){
	// appParameters = preferencesService.getAppParamsByPage(selectedPage);
	// }

	// test case

	public Folder getFolderByUrl(String url) {

		return preferencesDao.getFolderByUrl(url);
	}

	public void createFolder(Folder f) {
		preferencesDao.createFolder(f);
	}

	public List<Folder> getFolderList(String userAuth) {
		return preferencesDao.getFolderList(userAuth);
	}

	public void updateFolderName(String oldValue, String newValue) {

		preferencesDao.updateFolderName(oldValue, newValue);
	}

	public void deleteFolder(Folder f) {
		preferencesDao.deleteFolder(f);
	}

	public List<UserRoles> getUserRoles() {
		return preferencesDao.getUserRoles();
	}

	public void saveRole(List<UserRoles> list) {
		preferencesDao.saveRole(list);
	}

	public void deleteRole(UserRoles u) {
		preferencesDao.deleteRole(u);
	}

	public List<Action> getActionList() {
		return preferencesDao.getActionList();
	}

	public List<AppParameter> getAppParamsByPage(String selectedPage) {
		return preferencesDao.getAppParamsByPage(selectedPage);
	}

	public void saveRequest(Request request) {
		preferencesDao.saveRequest(request);

	}

	public List<Request> getAllRequests() {
		return preferencesDao.getAllRequests();
	}

	public List<Request_Header> getHeadersByRequest(int request_id) {
		return preferencesDao.getHeadersByRequest(request_id);
	}

	public void saveHeaders(List<Request_Header> headers) {
		preferencesDao.saveHeaders(headers);

	}

	public Request getRequestById(int request_id) {

		return preferencesDao.getRequestById(request_id);
	}

	public void deleteRequest(Request_Header req) {
		preferencesDao.deleteRequest(req);

	}

}
