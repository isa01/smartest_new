package com.tr.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tr.dao.TestCaseDao;
import com.tr.dao.TestSuiteDao;
import com.tr.model.Defect;
import com.tr.model.Report;
import com.tr.model.TestSuite;

@Service
@Transactional
public class TestSuiteService implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Resource
	private TestCaseDao testCaseDao ;
	
	@Resource
	private TestSuiteDao testSuiteDao ;

	
	public void saveUpdate(TestSuite ts){
		testSuiteDao.saveUpdate(ts);
	}
	
	public TestSuite getTestSuiteByUrlName(String url ,String name){
		return testSuiteDao.getTestSuiteByUrlName(url, name);
	}
	
	
	public List<TestSuite> getListTestSuite(){
		
		return testSuiteDao.getListTestSuite();
	}
	
	
	public void deleteSuite(TestSuite ts){
		 testSuiteDao.deleteSuite(ts);
		
	}
	
	
	//Test Execution 
	
	public void createDefect(Defect df){
		testSuiteDao.createDefect(df);
	}
	
	public void saveReport(Report rep){
		testSuiteDao.saveReport(rep);
	}
	
	
	
	
	//Report işlemi
	
	public List<Report> getListReport(){
		
		return testSuiteDao.getListReport();
	}
	
	 public Long getNextKey()
	 {
		return  testSuiteDao.getNextKey();
		 
	 }
	
	//getter setter 
	
	public TestCaseDao getTestCaseDao() {
		return testCaseDao;
	}

	public void setTestCaseDao(TestCaseDao testCaseDao) {
		this.testCaseDao = testCaseDao;
	}

	public TestSuiteDao getTestSuiteDao() {
		return testSuiteDao;
	}

	public void setTestSuiteDao(TestSuiteDao testSuiteDao) {
		this.testSuiteDao = testSuiteDao;
	}
	
	
	
	

}
