package com.tr.service;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.tr.dao.UserDao;
import com.tr.model.Company;
import com.tr.model.GnlParameter;
import com.tr.model.User;
import com.tr.model.UserAuthorization;

@Service
@Transactional
public class UserService implements Serializable{
	@Resource
	UserDao userDao;
	
	private static final long serialVersionUID = -7951292209282032239L;
	
	
	//parola getirme
	public String getPasswordByName(String name)
	{
		User user = userDao.getUserByName(name);
		if(user!=null){
			return user.getPassword();
		}
		
		return "";
	}
	
	
	public User getUserByName(String name){
		User user = userDao.getUserByName(name);
		if(user!=null){
			return user;
		}
		
		return null ;
	}
	
   
	//kullanıcı listesi
	public List<User> getUserList() 
	{
		return userDao.getUserList();
	}

	//user id getirme
	public int getIdByUsername(String username) 
	{
		User user = userDao.getUserByName(username);
		return user.getId();
		
				
	}
	
	//kullanıcı getirme
	
	public User getUser(String id){
		
		User user=userDao.getUserById(Integer.parseInt(id));
		return user;
		
	}
	
	//parola güncelleme
	public int updatePassword(int id,String password)
	{
		return userDao.updatePassword(id, password);
	}
	
	public void createUser(User user){
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
		user.setCompany(getCompanyByUsername((String) session.getAttribute("username")));
		userDao.createUser(user);
		
	}
	
	//delete işlemi 
	
	public void deleteUser(User user){
		userDao.deleteUser(user);
	}
	
	public void loadProperties(){
		   userDao.loadProperties();
	 }
	
	public List<UserAuthorization> getUserAuth(){
		return  userDao.getUserAuth();
	}
	
	
	//gnl param işlemleri
	
	     	public void saveParam(BigDecimal selectparam,GnlParameter saveparam){
				
				userDao.saveParam(selectparam, saveparam);
				
			}
			
			public void deleteParam(BigDecimal id){
				userDao.deleteParam(id);
			}
			
			public List<GnlParameter> getParamList(){
				
			    return	userDao.getParamList();
			}
	
	
			public void saveAuth(List<UserAuthorization> list){
				
				userDao.saveAuth(list);
			}
			
			public void delete(UserAuthorization u){
				userDao.delete(u);
			}
			
			
			
			public List<String> getParamValueList(String key){
				
				List<String> values=new ArrayList<String>() ;
				
				for(GnlParameter g :  userDao.getParamList())
				{
					if(g.getName().equals(key)){
						 values.add(g.getValue()) ;
					}
				}
				
				return values;
				
			}
			
         public List<GnlParameter> getParamValueListByKey(String key){
				
				List<GnlParameter> values=new ArrayList<GnlParameter>() ;
				
				for(GnlParameter g :  userDao.getParamList())
				{
					if(g.getName().equals(key)){
						 values.add(g) ;
					}
				}
				
				return values;
				
			}
         
         public String getParamValue(String key){
				for(GnlParameter g :  userDao.getParamList())
				{
					if(g.getName().equals(key)){
						 return g.getValue();
					}
				}				
				return null;
			}
	
	
	//getter setter 
	public UserDao getUserDao() {
		return userDao;
	}

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}


	public Company getCompanyByUsername(String username) {
		// TODO Auto-generated method stub
		return userDao.getCompanyByUsername(username);
	}
	
	

}
