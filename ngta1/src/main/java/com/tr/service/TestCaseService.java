package com.tr.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.annotation.Resource;
import javax.faces.application.FacesMessage;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.tools.JavaCompiler;
import javax.tools.JavaCompiler.CompilationTask;
import javax.tools.JavaFileObject;
import javax.tools.StandardJavaFileManager;
import javax.tools.StandardLocation;
import javax.tools.ToolProvider;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tr.controller.LoginController;
import com.tr.dao.GenericDao;
import com.tr.dao.TestCaseDao;
import com.tr.dao.UserDao;
import com.tr.model.AppParamDetail;
import com.tr.model.AppParameter;
import com.tr.model.GnlParameter;
import com.tr.model.PropertyGroup;
import com.tr.model.PropertyGroupCatalog;
import com.tr.model.RefTcType;
import com.tr.model.Scheduler;
import com.tr.model.StepValidation;
import com.tr.model.TestCase;
import com.tr.model.TestCaseProperty;
import com.tr.model.TestStep;
import com.tr.util.Smartest;

@ManagedBean(name = "testCaseService")
@Service
@Transactional
@SessionScoped
public class TestCaseService extends GenericDao implements Serializable {

	private static final long serialVersionUID = 1L;

	@Resource
	private TestCaseDao testCaseDao;

	private TestCase tCase;
	


	@Resource
	private UserDao userDao;

	transient StringWriter output;

	public String getParamValue(String key) {
		String value = null;
		for (GnlParameter g : userDao.getParamList()) {
			if (g.getName().equals(key)) {
				value = g.getValue();
			}
		}

		return value;
	}

	public List<String> getTcTypeList() {

		List<String> tcType = new ArrayList<String>();
		for (RefTcType rf : testCaseDao.getTcTypeList())
			tcType.add(rf.getName());

		return tcType;

	}

	public List<TestCase> getAllTestCase() {
		return testCaseDao.getAllTestCase();
	}

	public TestCase getTestCase(String selectedTestCase) {

		tCase = testCaseDao.getTestCaseByName(selectedTestCase);
		if (tCase != null)
			return tCase;

		return new TestCase();
	}

	public TestCase getTestCaseById(int id) {

		TestCase tCase = testCaseDao.getTestCaseById(id);
		if (tCase != null)
			return tCase;
		return tCase;
	}

	public TestCase getTestCaseByIdExcludePGC(int id) {

		TestCase tCase = testCaseDao.getTestCaseByIdExcludePGC(id);
		if (tCase != null)
			return tCase;
		return tCase;
	}

	// tc content
	public String getTcContent(String selectedTestCase) {

		StringBuilder stringBuilder = new StringBuilder();
		FileInputStream fstream = null;
		try {
			fstream = new FileInputStream(getParamValue("testCaseFolder") + "?????" + selectedTestCase + ".java");
		} catch (FileNotFoundException e) {

		}

		BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
		String strLine;
		try {
			while ((strLine = br.readLine()) != null)
				stringBuilder.append(strLine + "\n");

		} catch (IOException e) {

		}

		return stringBuilder.toString();
	}

	public void saveOrUpdateTestCase(TestCase tc,String username) {
		tc.setCreated_by(username);
		testCaseDao.saveOrUpdateTestCase(tc);
	}

	public List<TestCase> getTestCaseByUrlName(String url, String name) {

		List<TestCase> tCase = testCaseDao.getTestCaseByUrlName(url, name);

		return tCase;
	}

	public void saveOrUpdateTestStep(List<TestStep> ts) {
		testCaseDao.saveOrUpdateTestStep(ts);
	}

	public int createTestCaseFile(TestCase tc, Scheduler sch,String username) throws IOException {
		String environment = "Server";
		String browser = "Chrome";
		String schedulerString = "";
		if (sch != null) {
			environment = sch.getEnvironment();
			browser = sch.getBrowser();
			schedulerString = "\n smartest.setSchId(" + sch.getId() + ");";
		} else {
			sch = new Scheduler();
			sch.setId(0);
		}
		int i = 0;
		OutputStreamWriter out = null;
		String folder = tc.getFolderUrl().replace("/", "\\");
		File f = new File(getParamValue("testCaseFolder") + tc.getFolderUrl());
		if (!f.exists()) {
			f.mkdirs();
		}
		f = new File(getParamValue("testCaseFolder") + tc.getFolderUrl() + tc.getName() + "_" + sch.getId() + ".java");
		String packageName = "com.tr.testCaseFiles" + tc.getFolderUrl();
		packageName = packageName.replace('/', '.');
		packageName = "package " + packageName.substring(0, packageName.length() - 1);
		f.createNewFile();
		FileOutputStream fop = null;
		try {
			fop = new FileOutputStream(f);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			out = new OutputStreamWriter(fop, "utf8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (tc.getPlatform().equals("ANDROID")) {

			String android_import = getParamValue("android_imports");
			String str_class_android = "public class " + tc.getName() + "_" + sch.getId()
					+ " { \n private Smartest smartest = null; \n private String env=\"" + environment + "\";\n"
					+ getParamValue("android_configure") + "\n";

			out.write(android_import + "\n");
			out.write(str_class_android + "\n");
			out.write("@Test\n public void " + tc.getName() + "Test(){\n try{" + "\n");
			out.write(" if(env==\"Local\"){\r\n"
					+ "			final DesiredCapabilities caps = DesiredCapabilities.chrome();\r\n"
					+ "			final ChromeOptions chromeOptions = new ChromeOptions();\r\n"
					+ "            chromeOptions.addArguments(\"--disable-gpu\");\r\n"
					+ "            chromeOptions.addArguments(\"--no-sandbox\");\r\n"
					+ "			caps.setCapability(ChromeOptions.CAPABILITY, chromeOptions); \r\n"
					+ "			System.setProperty(\"webdriver.chrome.driver\", \"/usr/bin/chromedriver\");\r\n"
					+ "			driver = new ChromeDriver(caps);\r\n" + " }\r\n"
					+ " else{	 System.setProperty(\"webdriver.chrome.driver\", " + getParamValue("chrome_yol")
					+ ");\r\n" + "driver = new ChromeDriver();\r\n" + " }");
			out.write("smartest = Smartest.getSmartest();\n");
			out.write(schedulerString + "\n");

			convertTestStepToScript(out, tc, tc.getPlatform());

			out.write(
					"\n}catch (Exception e) {\r\n smartest.closePage(driver); fail(smartest.getLogText());\r\n }  catch (AssertionError exp) {\r\n smartest.closePage(driver);\r\n fail(smartest.getLogText());\r\n	} smartest.closePage(driver); ");
			out.write("}\n}\n\n");

			out.close();
			int isSuccess = addPath(
					getParamValue("testCaseFolder") + tc.getFolderUrl() + tc.getName() + "_" + sch.getId() + ".java",
					tc);
			if (isSuccess == -1) {
				i = -1;
				// f.delete();
				FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "HATA", "Test Case Yaratılamadı");
				FacesContext.getCurrentInstance().addMessage(null, msg);
			} else {
				i = 1;
				// saveTestCase(tc);
				FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "TestCase Yaratıldı", tc.getName());
				FacesContext.getCurrentInstance().addMessage(null, msg);
			}
			return i;

		} else if (tc.getPlatform().equals("WEB")) {
			// out.write(" @Before\r\n" + " public void settings() {\r\n" + " try {\r\n"
			// + " smartest = new Smartest();\r\n" + "String env=\"" + environment + "\";\n"
			// + " System.setProperty(\"webdriver.chrome.driver\", \"" +
			// getParamValue("chrome_yol")
			// + "\");\r\n" + " final ChromeOptions chromeOptions = new
			// ChromeOptions();\r\n"
			// + " final DesiredCapabilities caps = DesiredCapabilities.chrome();\r\n"
			// + " if (env.equals(\"Server\")) {\r\n"
			// + " chromeOptions.addArguments(\"--disable-gpu\");\r\n"
			// + " chromeOptions.addArguments(\"--no-sandbox\");\r\n"
			// + " caps.setCapability(ChromeOptions.CAPABILITY, chromeOptions);\r\n"
			// +"
			// caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,\""+getParamValue("phantomjs_url")+"\");\r\n"
			// + " driver = new PhantomJSDriver(caps);\r\n" + " } else {\r\n"
			// + " driver = new ChromeDriver(caps);"
			// + " \r\n" + " }\r\n"
			// + " driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);\r\n"
			// + " driver.manage().window().setSize(new Dimension(1600, 800));\r\n"
			// + " } catch (Exception exp) {\r\n"
			// + " smartest.setLogText(smartest.getLogText() +
			// smartest.getStackTrace(exp));\r\n"
			// + " smartest.setTestResult(\"failed\");\r\n" + " exp.printStackTrace();\r\n"
			// + " }\r\n" + " }\r\n" + "\r\n");
			// out.write(" @Test\r\n" + " public void " + tc.getName() + "Test() {");
			String testCaseType = tc.getTypeName();
			out.write(packageName + ";\n");
			out.write(getParamValue("str_imports") + "\n");
			out.write("public class " + tc.getName() + "_" + sch.getId() + "{\n");
			out.write("private WebDriver driver = null;  \n   int testcase_id = " + tc.getId()
					+ ";\n WebElement webElement;\n ");
			out.write("private Smartest smartest;\n DesiredCapabilities caps; \r\n");
			out.write("	@Before\r\n" + "	public void settings() {\r\n" + "		try {\r\n"
					+ "			smartest = new Smartest(\""+username+"\");\r\n" + "String env=\"" + environment + "\";\n"
					+ "			System.setProperty(\"webdriver.chrome.driver\",  \"" + getParamValue("chrome_yol")
					+ "\");");
			out.write("				caps = DesiredCapabilities.chrome();\r\n"
					+ " final ChromeOptions chromeOptions = new ChromeOptions();\r\n"
					+ " if (env.equals(\"Server\")) {\r\n" + " chromeOptions.addArguments(\"--disable-gpu\");\r\n"
					+ " chromeOptions.addArguments(\"--no-sandbox\");\r\n"
					+ "chromeOptions.addArguments(\"--headless\");\r\n"
					+ "chromeOptions.addArguments(\"--disable-gpu\");\r\n"
					+ "chromeOptions.addArguments(\"--window-size=1800,800\");\r\n"
					+ "chromeOptions.addArguments(\"--enable-javascript\");			\r\n"
					+ "caps.setCapability(ChromeOptions.CAPABILITY, chromeOptions); \r\n"
					+ "			driver = new ChromeDriver(caps);	}		\r\n" + "else {\r\n"
					+ "chromeOptions.addArguments(\"--window-size=1800,800\");\r\n"
					+ "chromeOptions.addArguments(\"--enable-javascript\");			\r\n"
					+ "caps.setCapability(ChromeOptions.CAPABILITY, chromeOptions); \r\n"
					+ "				driver = new ChromeDriver(caps);\r\n"
					+ "		}\r\n	driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);\r\n"
					+ "		} catch (Exception exp) {\r\n"
					+ "			smartest.setLogText(smartest.getLogText() + smartest.getStackTrace(exp));\r\n"
					+ "			smartest.setTestResult(\"failed\");\r\n" + "			exp.printStackTrace();\r\n"
					+ "		}\r\n" + "	}");
			out.write("	@Test\r\n" + "	public void " + tc.getName() + "Test() {");
			out.write(" try {\n driver.get(\"" + tc.getOpen_page() + "\");\n"
					+ " smartest.setLogText(smartest.getLogText()+\"" + tc.getOpen_page()
					+ " Sayfası Açıldı.... <br/>\");");
			out.write(schedulerString + "\n");

			convertTestStepToScript(out, tc, tc.getPlatform());

			out.write("\r\n" + "		} catch (Exception e) {\r\n"
					+ "			smartest.setTestResult(\"failed\");\r\n" + "			e.printStackTrace();\r\n"
					+ "			smartest.setLogText(smartest.getLogText() + smartest.getStackTrace(e));\r\n"
					+ "		} catch (AssertionError exp) {\r\n"
					+ "			smartest.setLogText(smartest.getLogText() + \"\\n Assertion Hatası :\" + exp.getMessage());\r\n"
					+ "			smartest.setTestResult(\"failed\");\r\n"
					+ "			smartest.setLogText(smartest.getLogText() + smartest.getStackTrace(exp));\r\n"
					+ "		}\r\n" + "	}\r\n" + "\r\n" + "	@After\r\n" + "	public void tearDown() {\r\n"
					+ "		smartest.finishTestCase(driver," + tc.getId() + ");\r\n" + "	}\r\n" + "\r\n"
					+ "	public Smartest getSmartest() {\r\n" + "		return smartest;\r\n" + "	}\r\n" + "\r\n"
					+ "	public void setSmartest(Smartest smartest) {\r\n" + "		this.smartest = smartest;\r\n"
					+ "	}\r\n" + "}");

			out.close();
			fop.close();
			int isSuccess;
			if (getParamValue("testCaseFolder").contains("/")) {
				System.out.println(
						"linux: " + getParamValue("testCaseFolder") + tc.getFolderUrl() + tc.getName() + ".java");
				isSuccess = addPath(getParamValue("testCaseFolder") + tc.getFolderUrl() + tc.getName() + "_"
						+ sch.getId() + ".java", tc);
			} else {
				System.out.println("windows: " + getParamValue("testCaseFolder") + folder + tc.getName() + ".java");
				isSuccess = addPath(
						getParamValue("testCaseFolder") + folder + tc.getName() + "_" + sch.getId() + ".java", tc);
			}
			if (isSuccess == -1) {
				// f.delete();
				FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR,
						"HATA : Failed to compile: " + tc.getName() + ":" + output.toString(), "");
				FacesContext.getCurrentInstance().addMessage(null, msg);
				return -1;
			} else {
				// saveTestCase(tc);
				FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "TestCase Yaratıldı", tc.getName());
				FacesContext.getCurrentInstance().addMessage(null, msg);
				return 1;
			}

		}
		return -1;
	}

	public void convertTestStepToScript(OutputStreamWriter out, TestCase tc, String type) throws IOException {
		TestCase testcase;
		tc.getTestSteps().sort(Comparator.comparing(TestStep::getOrderNum));
		if (tc.getTestSteps() != null) {
			if (tc.getTestSteps().size() > 0) {
				for (TestStep ts : tc.getTestSteps()) {
					if (ts.getType().equals("TestCase")) {
						testcase = getTestCaseById(ts.getTemplateId());
						testcase.setTestSteps(getTestStepById(testcase.getId()));
						convertTestStepToScript(out, testcase, type);
					} else if (ts.getType().equals("TestStep")) {
						String value = "smartest.executeTestStep(driver," + ts.getId() + ",\"" + type + "\");";
						out.write(value + "//"
								+ testCaseDao.getAppParamaterById(ts.getAppParameter().getId()).getDescription()
								+ "\n");
					} else if (ts.getType().equals("Java")) {
						out.write("try{\n");
						out.write(getTestCaseDao().getAppParamDetByAppParamId(ts.getAppParameter().getId()).get(0)
								.getAttr_value() + "\n");
						out.write("		smartest.executeJavaStep(driver, " + ts.getId() + ", \"passed\");\r\n"
								+ "			}catch(Exception exp) {\r\n smartest.setTestResult(\"failed\");"
								+ "				smartest.executeJavaStep(driver, " + ts.getId()
								+ ", \"failed\");\r\n throw new Exception(\"\");\n" + "			}");
					} else if (ts.getType().equals("Rest")) {
						out.write("smartest.runRequest(" + ts.getTemplateId() + "<br/>\");\n");
					}
				}
			}
		}

		/*
		 * if (tc.getTestSteps() != null) { if (tc.getTestSteps().size() > 0) { for
		 * (TestStep ts : tc.getTestSteps()) { if (ts.getType().equals("TestCase")) {
		 * out.write("smartest.executeTestCase(driver," +
		 * testCaseDao.getTestCaseById(ts.getTemplateId()).getId() + ",\""+type +
		 * "\");\n"); List<TestStep> tsList = getTestStepById(ts.getTemplateId());
		 * for(TestStep tstep : tsList) if(tstep.getType() == "Java")
		 * out.write(getTestCaseDao().getAppParamDetByAppParamId(ts.getAppParameter_id()
		 * ).get(0).getAttr_value()); } else if (ts.getType().equals("TestStep")) {
		 * String value = "smartest.executeTestStep(driver," + ts.getId() + ",\"" +
		 * ts.getParam1() + "\",\"" + type +"\");\n"; out.write(value + "\n"); } else if
		 * (ts.getType().equals("Java")) { AppParamDetail appdet = (AppParamDetail)
		 * testCaseDao .getAppParamDetByAppParamId(ts.getAppParameter_id()).get(0);
		 * out.write(appdet.getAttr_value() + "\n"); } } } }
		 */
	}

	public int addPath(String s, TestCase tc) {
		System.out.println(".class oluşturuluyor: " + s);
		int isSuccess = 0;
		try {
			String javaHome = getParamValue("java.home");
			System.setProperty("java.home", javaHome);
			output = new StringWriter();
			boolean result;

			JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
			StandardJavaFileManager fileManager = compiler.getStandardFileManager(null, null, null);
			List<File> sourceFileList = new ArrayList<File>();
			File fff = new File(s);
			sourceFileList.add(fff);
			Iterable<? extends JavaFileObject> compilationUnits = fileManager
					.getJavaFileObjectsFromFiles(sourceFileList);
			List<String> optionList = new ArrayList<String>();
			String path = this.getClass().getClassLoader().getResource("").getPath();
			// String path = "\\smartest_ngta1\\target\\ngta\\WEB-INF\\classes\\";
			System.out.println("classesFolder:" + path);
			File f = new File(path + "com\\tr\\testCaseFiles" + tc.getFolderUrl() + tc.getName() + ".class");
			f.delete();
			optionList.addAll(Arrays.asList("-classpath", path +";/C:\\apache-tomcat-8.5.93\\webapps\\libs\\"));
			optionList.addAll(Arrays.asList("-d", path));
			CompilationTask task = compiler.getTask(output, fileManager, null, optionList, null, compilationUnits);
			result = task.call();
			System.out.println("Compilation result : " + output.toString());
			if (result) {
				isSuccess = 1;
				System.out.println("Success");
			} else {
				System.out.println("Fail");
				isSuccess = -1;
			}
			try {
				fileManager.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {
				output.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return isSuccess;

	}



	// private String buildClassPath(String... paths) {
	// StringBuilder sb = new StringBuilder();
	// for (String path : paths) {
	// if (path.endsWith("*")) {
	// path = path.substring(0, path.length() - 1);
	// File pathFile = new File(path);
	// for (File file : pathFile.listFiles()) {
	// if (file.isFile()) {
	// sb.append(path);
	// sb.append(file.getName());
	// sb.append(System.getProperty("path.separator"));
	// }
	// }
	// } else {
	// sb.append(path);
	// sb.append(System.getProperty("path.separator"));
	// }
	// }
	// return sb.toString();
	// }

	public void deleteClassFile(String name) {
		String path = this.getClass().getClassLoader().getResource("").getPath() + "com\\tr\\testCaseFiles";
		try {

			File file = new File(path + name + ".class");

			if (file.delete()) {
				System.out.println(file.getName() + " is deleted!");
			} else {
				System.out.println("Delete operation is failed.");
			}

		} catch (Exception e) {

			e.printStackTrace();

		}
	}

	public TestStep getTestStepByTsId(int ts_id) {
		return testCaseDao.getTestStepByTsId(ts_id);
	}

	public List<AppParamDetail> getAppParamDetByAppParamId(int app_param_id) {
		return testCaseDao.getAppParamDetByAppParamId(app_param_id);
	}

	public AppParameter getAppParamaterById(int appParameter_id) {
		return testCaseDao.getAppParamaterById(appParameter_id);
	}

	public List<TestCase> lastRunFailedTestCaseList() {
		return testCaseDao.lastRunFailedTestCaseList();
	}

	public List<TestStep> getTestStepById(int tCaseId) {

		return testCaseDao.getTestStepById(tCaseId);
	}

	public void deleteTestCase(TestCase tCase) {
		testCaseDao.deleteTestCase(tCase);
	}

	// getter setter

	public TestCaseDao getTestCaseDao() {
		return testCaseDao;
	}

	public void setTestCaseDao(TestCaseDao testCaseDao) {
		this.testCaseDao = testCaseDao;
	}

	public TestCase gettCase() {
		return tCase;
	}

	public void settCase(TestCase tCase) {
		this.tCase = tCase;
	}

	public UserDao getUserDao() {
		return userDao;
	}

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	public List<AppParameter> appParamListNotUsed() {
		// TODO Auto-generated method stub
		return testCaseDao.appParamListNotUsed();
	}

	public List<TestCase> getAllTemplateCase() {
		return testCaseDao.getAllTemplateCase();
	}

	public List<TestCase> getTesCaseListByTsId(int id) {
		return testCaseDao.getTesCaseListByTsId(id);
		// TODO Auto-generated method stub

	}

	public void saveScheduler(Scheduler sch) {
		testCaseDao.saveScheduler(sch);

	}

	public List<Scheduler> getSchedulers() {
		return testCaseDao.getSchedulers();
	}

	public PropertyGroup getPropertyGroupById(int pg_id) {
		return testCaseDao.getPropertyGroupById(pg_id);
	}

	public int getNextSchedulerId() {
		return testCaseDao.getNextSchedulerId();
	}

	public List<PropertyGroupCatalog> getAllPropertyGroupCatalogList() {
		return testCaseDao.getAllPropertyGroupCatalogList();
	}

	public PropertyGroup getCatalogPropertyGroup(int id) {
		return testCaseDao.getCatalogPropertyGroup(id);
	}

	public PropertyGroup getDefaultPropertGroupForTestCase(TestCase selectedTestCase) {
		return testCaseDao.getDefaultPropertGroupForTestCase(selectedTestCase);
	}

	public void savePropertyGroup(PropertyGroup pg) {
		testCaseDao.savePropertyGroup(pg);
	}

	public void savePropertyGroupCatalog(PropertyGroupCatalog pgc) {
		testCaseDao.savePropertyGroupCatalog(pgc);
	}

	public TestCaseProperty getPropertyOfTestStep(int testStep_id) {
		return testCaseDao.getPropertyOfTestStep(testStep_id);
	}

	public List<Scheduler> getSchedulersToBeExecute(String sch_type) {
		return testCaseDao.getSchedulersToBeExecute(sch_type);
	}

	public List<Scheduler> getSchedulersRunning() {
		return testCaseDao.getSchedulersRunning();
	}

	public Scheduler getSchedulerById(int i) {
		return (Scheduler) testCaseDao.getSchedulerById(i);
	}

	public List<TestCase> getTestCaseListByAppParam(AppParameter selectedAppParam) {
		return testCaseDao.getTestCaseListByAppParam(selectedAppParam);
	}

	public void saveOrUpdateValidation(Set<StepValidation> selectedValidationList) {
		testCaseDao.saveOrUpdateValidation(selectedValidationList);
		
	}

	

	// public List<TestCaseProperty> bringPropertiesOfGroup(PropertyGroup
	// selectedPropertyGroup) {
	// return testCaseDao.bringPropertiesOfGroup(selectedPropertyGroup);
	// }

}
