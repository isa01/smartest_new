package com.tr.service;

import java.io.Serializable;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Properties;
import java.util.ResourceBundle;

import javax.annotation.Resource;
import javax.faces.context.FacesContext;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tr.dao.TestSuiteDao;
import com.tr.dao.UserDao;

@Service
@Transactional
public class GeneralServices implements Serializable{

	private ResourceBundle bundle;
	private static final long serialVersionUID = 1L;
	private InetAddress ip;
	
	
	
	 public ResourceBundle getBundle() {
	        if (bundle == null) {
	            FacesContext context = FacesContext.getCurrentInstance();
	            bundle = context.getApplication().getResourceBundle(context, "lang");
	        }
	        return bundle;
	 }
	 
	 
	 public String getIpAddress() throws UnknownHostException{
		 
			ip = InetAddress.getLocalHost();
			return ip.getHostAddress();

	 }
	 
	 public String getMacAddress() throws SocketException, UnknownHostException{
		 
		 ip = InetAddress.getLocalHost();
		 NetworkInterface network = NetworkInterface.getByInetAddress(ip);
         byte[] mac = network.getHardwareAddress();
        
 
         StringBuilder sb = new StringBuilder();
         for (int i = 0; i < mac.length; i++) {
            sb.append(String.format("%02X%s", mac[i], (i < mac.length - 1) ? "-" : ""));		
         }
        
		 
		 return sb.toString() ; 
	 }
	 
	 
	 public void sendMail(String subject , String text , String receiverMailAddress){
		 
		 final String username = "isa.aytmr@gmail.com";
         final String password = "aaaa";
         Properties properties = new Properties();
         properties.put("mail.smtp.auth", "true");
         properties.put("mail.smtp.starttls.enable", "true");
         properties.put("mail.smtp.host", "smtp.gmail.com");
         properties.put("mail.smtp.port", "587");

         Session session = Session.getInstance(properties,
                       new javax.mail.Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                       return new PasswordAuthentication(username, password);
                }
         }); 
         try {

                Message message = new MimeMessage(session);
                message.setFrom(new InternetAddress("xxx@xxx.com"));
                message.setRecipients(Message.RecipientType.TO,InternetAddress.parse(receiverMailAddress));
                message.setSubject(subject);
                message.setText(text); 
                Transport.send(message);

         } catch (MessagingException ex) {
                throw new RuntimeException(ex);
         }
		 
		 
	 }
	 
	
	
}
