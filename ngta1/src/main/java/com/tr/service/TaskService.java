package com.tr.service;

import java.io.Serializable;
import java.util.List;

import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.tr.dao.TaskDao;
import com.tr.model.Comments;
import com.tr.model.Task;

@Service
@Transactional
public class TaskService implements Serializable{

	private static final long serialVersionUID = 1L;
   
	@Resource
	private TaskDao taskDao ;
	
	public List<Task> getListTask(){
		return taskDao.getListTask();
	}
	
	public List<Comments> getCommentList(Task d){
		return taskDao.getCommentList(d);
	}
	
	public void removeCommentOnTask(Task t,Comments c){
		taskDao.removeCommentOnTask(t, c);
	}
	
	 public void saveComment(List<Comments> list)
		{
			
			taskDao.saveComment(list);
		}
	 
	 public void saveTask(Task t){
		   taskDao.saveTask(t);
	 }
 	
}
