package com.tr.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.tr.model.Comments;
import com.tr.model.Defect;

@Repository
public class DefectDao extends GenericDao  implements Serializable
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6767900867311590838L;

	
	
	@SuppressWarnings({ "unchecked", "deprecation" })
	public List<Defect> getDefectListByUserId(int id)
	{ 
		  List<Defect> list =  (List<Defect>) getSession().createQuery("from Defect where user_id=:id").setParameter("id", id).list();
		  return list;
		
	}
	
	public List<Defect> getDefectByDefectId(String id)
	{
		return  getSession().createQuery("from Defect where id=:id").setParameter("id", id).list();
	}

	public List<Defect> getDefectListByCreatedby(int id)
	{
		  List<Defect> list =  (List<Defect>) getSession().createQuery("from Defect where created_by=:id").setParameter("id", id).list();
		  return list;
	}

	
	
	//Defect işlemleri 
	
			public List<Defect> getListDefect(){
				
				String a="";
				List<Defect> d ;
				try{
					d=getSession().createQuery("from Defect where status=:status").setParameter("status", "Aktif")  .list() ;
				    
			    }catch(Exception ex){
			    	d=new ArrayList<Defect>();
			    }
				
				return d ;
			}
			
			public void createDefect(Defect t)
			{
				getSession().save(t);
			}
			
			public void removeCommentOnDefect(Defect d , Comments c){
				
				
			}
			
			public void saveComment(List<Comments> list)
			{
				
				for(Comments l : list)
			    	getSession().save(l);
			    	
			}
			
			public List<Comments> getCommentList(Defect d){
				
				List<Comments> lc ;
				try{
					lc=getSession().createQuery("from Comments where status=:status and defect_id=:id").setParameter("status", "Aktif").setParameter("id", d.getId()).list();
				}catch(Exception ex){
					lc=new ArrayList<Comments>();
				}
				
			    return lc ;
			}

			public List<Comments> getCommentList() {
				// TODO Auto-generated method stub
				return getSession().createQuery("from Comments  where status=:status").setParameter("status", "Aktif").getResultList();
			}

}
