package com.tr.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.tr.model.Comments;
import com.tr.model.Defect;
import com.tr.model.Task;

@Repository
public class TaskDao extends GenericDao implements Serializable
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6767900867311590838L;

	public List<Task> getListTask(){
		
		String a="";
		List<Task> d ;
		try{
			d=getSession().createQuery("from Task where status=:status").setParameter("status", "Aktif")  .list() ;
		    
	    }catch(Exception ex){
	    	d=new ArrayList<Task>();
	    }
		
		return d ;
	}
	
	public List<Comments> getCommentList(Task d){
		
		List<Comments> lc ;
		try{
			lc=getSession().createQuery("from Comments where task_id=:id").setParameter("id", d.getId()).list();
			
		}catch(Exception ex){
			lc=new ArrayList<Comments>();
		}
		
	    return lc ;
	}
	
	 public void removeCommentOnTask(Task t,Comments c){
			System.out.println("hata");
	 }
	 
	 public void saveComment(List<Comments> list)
		{
			
			for(Comments l : list)
		    	getSession().save(l);
		    	
		}
	 
	 public void saveTask(Task t){
		 getSession().save(t);
	 }
	
}
