package com.tr.dao;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.tr.model.Action;
import com.tr.model.AppParamDetail;
import com.tr.model.AppParameter;
import com.tr.model.FindElementMethod;
import com.tr.model.Folder;
import com.tr.model.Request_Header;
import com.tr.model.PreparedUrl;
import com.tr.model.Request;
import com.tr.model.TestStep;
import com.tr.model.UserRoles;

@Repository
public class PreferencesDao extends GenericDao implements Serializable {

	private static final long serialVersionUID = 1L;

	// url list func.

	public void saveParameters(List<AppParameter> appParameters) {
		int company_id = getCompanyId(); 
		for (AppParameter p : appParameters) {
			p.setCompany_id(company_id);
			getSession().saveOrUpdate(p);
		
		}
	}

	public int saveParameter(AppParameter appParameter) {
		int company_id = getCompanyId();
		appParameter.setCompany_id(company_id);
		getSession().saveOrUpdate(appParameter);
		return appParameter.getId();
	}
	
	public void saveAppParamDetail(AppParamDetail appParamDetail) {
		getSession().saveOrUpdate(appParamDetail);
		
	}
	
//	public void saveAppParamDetailList(List<AppParamDetail> appParamDetailList) {
//		for (AppParamDetail p : appParamDetailList)
//		getSession().saveOrUpdate(p);
//	}
	
	@SuppressWarnings("unchecked")
	public List<AppParamDetail> getAppParamDetByAppParamId(int id)
	{
		return getSession().createQuery("from AppParamDetail where appParameter_id=:id and  status=:status")
				.setParameter("id", id)
				.setParameter("status", "Aktif")
				.getResultList();
	}


	public void saveUrl(PreparedUrl url) {
		getSession().saveOrUpdate(url);
	}

	public List<PreparedUrl> getUrl() {

		List<PreparedUrl> list = getSession().createQuery("from PreparedUrl where status=:status")
				.setParameter("status", "Aktif").list();
		return list;
	}

	public void deleteUrl(PreparedUrl url) {

		try {
			getSession().createQuery("update AppParameter set status = :status where page = :page")
					.setParameter("status", "Pasif").setParameter("page", url.getPage()).executeUpdate();

			getSession().createQuery("update PreparedUrl set status = :status where page = :page")
					.setParameter("status", "Pasif").setParameter("page", url.getPage()).executeUpdate();
		} catch (Exception ex) {
			System.out.println("Hatamız" + ex);
		}

	}

	// test step funct.

	public List<AppParameter> getAppParamaters() {
		int company_id = getCompanyId();
		List<AppParameter> apps = getSession().createQuery("from AppParameter where status=:status and company_id=:company_id").setParameter("company_id", company_id)
				.setParameter("status", "Aktif").list();
		return apps;
	}

	public void removeParam(AppParameter app) {

		getSession().createQuery("update AppParameter set status = :status where id = :id")
				.setParameter("status", "Pasif").setParameter("id", app.getId()).executeUpdate();

	}
	
	public void removeParamDetail(AppParamDetail app) {

		getSession().createQuery("update AppParamDetail set status = :status where id = :id")
				.setParameter("status", "Pasif").setParameter("id", app.getId()).executeUpdate();

	}

	public List<FindElementMethod> getFindElementMethodList() {
		return getSession().createQuery("from FindElementMethod").list();
	}

	public List<String> pagesFilter() {
		return getSession().createQuery("SELECT page FROM AppParameter  where status=:status group by page")
				.setParameter("status", "Aktif").getResultList();
	}
	
	public List<AppParameter> pagesFilter(String page) {
		if(page.equals("")) {
			return getSession().createQuery("FROM AppParameter  where status=:status")
					.setParameter("status", "Aktif").getResultList();
		}
		else
		return getSession().createQuery("FROM AppParameter  where status=:status and page=:page")
				.setParameter("status", "Aktif")
				.setParameter("page", page).getResultList();
	}

	// test case

	public Folder getFolderByUrl(String url) {
		return (Folder) getSession().createQuery("from Folder where url=:url and status=:status")
				.setParameter("url", url).setParameter("status", "Aktif").uniqueResult();
	}

	public void createFolder(Folder f) {
		int company_id = getCompanyId();
		f.setCompany_id(company_id);
		getSession().saveOrUpdate(f);
	}

	// where r.user_auth_id=:userAuth and f.status=:status
	public List<Folder> getFolderList(String userAuth) {
		int company_id = getCompanyId();
			//BigDecimal u = new BigDecimal(userAuth);
			List<Folder> f = getSession().createQuery("from Folder f " + " where  f.status=:status  and f.company_id=:company_id").setParameter("company_id", company_id)
					.setParameter("status", "Aktif").getResultList();

//			getSession().flush();
//			List<Folder> f1 = new ArrayList<Folder>();
//			for (Folder ff : f) {
//				boolean is = true;
//				UserRoles rr = (UserRoles) getSession().createQuery("from UserRoles " + " where  id=:id ")
//						.setParameter("id", new BigDecimal(ff.getUserRoles())).uniqueResult();
//
//				for (int i = 0; i < rr.getUser_auth().size(); i++) {
//					if (rr.getUser_auth().get(i).getId().equals(u))
//						is = false;
//				}
//				if (!is)
//					f1.add(ff);

//			}
	/* yetki kontrolü devre dışı return f1*/ 
			return f;

 
	}

	public void updateFolderName(String oldValue, String newValue) {

		String name = newValue.split("/")[newValue.split("/").length - 1];

		getSession().createQuery("update Folder set name=:name  where url=:oldValue ").setParameter("name", name)
				.setParameter("oldValue", oldValue).executeUpdate();

		getSession().createQuery("update Folder set  url=REPLACE(url,:oldValue,:newValue)  ")
				.setParameter("newValue", newValue).setParameter("oldValue", oldValue).executeUpdate();

		getSession().createQuery("update TestCase set folderUrl=REPLACE(folderUrl,:oldValue,:newValue)")
				.setParameter("newValue", newValue).setParameter("oldValue", oldValue).executeUpdate();

	}

	public void deleteFolder(Folder f) {

		getSession().createQuery("update Folder set status=:status  where id=:id ").setParameter("status", "Pasif")
				.setParameter("id", f.getId()).executeUpdate();

		getSession().createQuery("update TestCase set status=:status where folderUrl=:folderUrl")
				.setParameter("status", "Pasif").setParameter("folderUrl", f.getFolder_url()).executeUpdate();

	}

	public List<UserRoles> getUserRoles() {
		return getSession().createQuery("from UserRoles").list();
	}

	public void saveRole(List<UserRoles> list) {
		for (UserRoles ur : list)
			if (!ur.getUser_roles().trim().equals(""))
				getSession().saveOrUpdate(ur);
	}

	public void deleteRole(UserRoles u) {
		getSession().delete(u);
	}

	public List<Action> getActionList() {
		return getSession().createQuery("from Action").list();
	}


	public AppParameter getAppParameterById(int id) {
		// TODO Auto-generated method stub
		return  (AppParameter) getSession().createQuery("from AppParameter where status=:status and id=:id")
				.setParameter("status", "Aktif")
				.setParameter("id", id).getSingleResult();
	}

	public TestStep getTestStepById(int id) {
		return (TestStep) getSession().createQuery("from TestStep where id=:id")
				.setParameter("id", id).getResultList();
	}

	public List<AppParameter> getAppParamsByPage(String selectedPage) {
		return    getSession().createQuery("from AppParameter where status=:status and page=:page")
				.setParameter("status", "Aktif")
				.setParameter("page", selectedPage).getResultList();
	}

	public void saveRequest(Request request) {
		getSession().saveOrUpdate(request);
		
	}

	public List<Request> getAllRequests() {
		return getSession().createQuery("from Request").list();
	}
	
	public List<Request_Header> getHeadersByRequest(int request_id) {
		return getSession().createQuery("from Request_Header where request_id =:request_id").setParameter("request_id", request_id).list();
	}

	public void saveHeaders(List<Request_Header> headers) {
		for(Request_Header h : headers)
			getSession().saveOrUpdate(h);		
	}

	public void deleteRequest(Request_Header req) {
		getSession().delete(req);
		
	}

	public Request getRequestById(int request_id) {
		return (Request) getSession().createQuery("from Request where id =:request_id").setParameter("request_id", request_id).getSingleResult();
	}
 
 



	



}
