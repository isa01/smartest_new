package com.tr.dao;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.faces.bean.ManagedProperty;
import javax.persistence.Query;
import javax.persistence.Transient;

import org.hibernate.FetchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.tr.controller.LoginController;
import com.tr.model.AppParamDetail;
import com.tr.model.AppParameter;
import com.tr.model.PropertyGroup;
import com.tr.model.PropertyGroupCatalog;
import com.tr.model.RefTcType;
import com.tr.model.Report;
import com.tr.model.Scheduler;
import com.tr.model.StepValidation;
import com.tr.model.TestCase;
import com.tr.model.TestCaseProperty;
import com.tr.model.TestStep;

@Repository
public class TestCaseDao extends GenericDao implements Serializable {

	private static final long serialVersionUID = 6767900867311590838L;

	public List<RefTcType> getTcTypeList() {
		return getSession().createQuery("from RefTcType where status=:status").setParameter("status", "Aktif").list();
	}

	public List<TestCase> getAllTestCase() {
		int company_id = getCompanyId();
		List<TestCase> list = getSession().createQuery("from TestCase where status=:status  and company_id=:company_id")
				.setParameter("status", "Aktif").setParameter("company_id", company_id).getResultList();// .setParameter("company_id",
																										// getCompany().getId())
		if (list.isEmpty())
			list = new ArrayList<>();
		return list;
	}

	public List<TestCase> getAllTemplateCase() {
		int company_id = getCompanyId();
		List<TestCase> list = getSession()
				.createQuery("from TestCase where status=:status and template=:template and company_id=:company_id")
				.setParameter("status", "Aktif").setParameter("template", true).setParameter("company_id", company_id)
				.list();

		if (list.isEmpty())
			list = new ArrayList<>();

		return list;
	}

	public TestCase getTestCaseByName(String t) {
		return (TestCase) getSession().createQuery("from TestCase where name=:t and  status=:status")
				.setParameter("t", t).setParameter("status", "Aktif").uniqueResult();// .setParameter("company_id",
																						// getCompany().getId())
	}

	public TestCase getTestCaseById(int id) {
		return (TestCase) getSession()
				.createQuery("select distinct t from TestCase t where t.id=:t and  t.status=:status")
				.setParameter("t", id).setParameter("status", "Aktif").uniqueResult();
	}

	public TestCase getTestCaseByIdExcludePGC(int id) {
		return (TestCase) getSession().createCriteria(TestCase.class).add(Restrictions.like("id", id))
				.add(Restrictions.like("status", "Aktif")).setFetchMode("propertyGroupCatalog", FetchMode.LAZY)
				.uniqueResult();
	}

	public void saveOrUpdateTestCase(TestCase tc) {
		int company_id = getCompanyId();
		tc.setCompany_id(company_id);
		getSession().saveOrUpdate(tc);
		int aa = tc.getId();
		// Descriptionı değişen testcaselerin kullanıldığı testepleri update et
		getSession().createQuery(
				"update TestStep set description=(select description from TestCase  where id =:template_id) where templateId =:template_id")
				.setParameter("template_id", tc.getId()).executeUpdate();
	}

	public List<TestCase> getTestCaseByUrlName(String url, String name) {
		int company_id = getCompanyId();
		return getSession().createQuery(
				"from TestCase where folderUrl=:url and name=:name and  status=:status  and company_id=:company_id")
				.setParameter("url", url).setParameter("name", name).setParameter("status", "Aktif")
				.setParameter("company_id", company_id).getResultList();// .setParameter("company_id",
		// getCompany().getId())

	}

	public void saveOrUpdateTestStep(List<TestStep> ts) {
		for (TestStep t : ts)
			getSession().saveOrUpdate(t);
	}

	public List<TestStep> getTestStepById(int tCaseId) {

		List<TestStep> ts = getSession().createQuery("from TestStep where status=:status and testcase_id=:tCaseId")
				.setParameter("status", "Aktif").setParameter("tCaseId", tCaseId).getResultList();

		if (ts != null)
			return ts;
		return new ArrayList<TestStep>();
	}

	public void deleteTestCase(TestCase tCase) {

		getSession().createQuery("update TestCase set status=:status where id=:id").setParameter("status", "Pasif")
				.setParameter("id", tCase.getId()).executeUpdate();

		getSession().createQuery("update TestStep set status=:status where testcase_id=:testcase_id")
				.setParameter("status", "Pasif").setParameter("testcase_id", tCase.getId()).executeUpdate();

	}

	public List<AppParamDetail> getAppParamDetByAppParamId(int app_param_id) {
		return getSession().createQuery("from AppParamDetail where appParameter_id=:id and  status=:status")
				.setParameter("id", app_param_id).setParameter("status", "Aktif").getResultList();
	}

	public List<TestCase> lastRunFailedTestCaseList() {
		return getSession().createQuery(
				"from TestCase t where t.id in(select r2.testCase.id from Report r2 where r2.id in(Select max(r.id)  FROM Report r group by r.testCase))")
				.getResultList();

	}

	public List<AppParameter> appParamListNotUsed() {
		// TODO Auto-generated method stub
		return getSession().createQuery(
				"from AppParameter where  status=:status and id not in(select ts.appParameter from TestStep ts)")
				.setParameter("status", "Aktif").getResultList();
	}

	public AppParameter getAppParamaterById(int appParameter_id) {
		return (AppParameter) getSession().createQuery("from AppParameter where id=:id and status=:status")
				.setParameter("id", appParameter_id).setParameter("status", "Aktif").getResultList().get(0);
	}

	public List<TestCase> getTesCaseListByTsId(int ts_id) {
		// TODO Auto-generated method stub
		return null;
	}

	public TestStep getTestStepByTsId(int ts_id) {
		return (TestStep) getSession().createQuery("from TestStep where id=:id").setParameter("id", ts_id)
				.getSingleResult();
	}

	public void saveScheduler(Scheduler sch) {
		int company_id = getCompanyId();
		sch.setCompany_id(company_id);
		getSession().saveOrUpdate(sch);

	}

	public List<Scheduler> getSchedulers() {
		int company_id = getCompanyId();
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Date date = new Date();
		return (List<Scheduler>) getSession()
				.createQuery("from Scheduler where type=:type and run_date <=:runDate  and company_id=:company_id")
				.setParameter("type", "Regular").setParameter("runDate", dateFormat.format(date))
				.setParameter("company_id", company_id).getResultList();
	}

	public int getNextOrderNum(List<TestStep> testSteps) {
		// TODO Auto-generated method stub
		return 0;
	}

	public PropertyGroup getPropertyGroupById(int pg_id) {
		return (PropertyGroup) getSession().createQuery("from PropertyGroup where id=:id").setParameter("id", pg_id)
				.getSingleResult();
	}

	public int getNextSchedulerId() {
		try {
			List<Scheduler> ls = getSession().createQuery("from Scheduler").getResultList();
		} catch (Exception nre) {
			return 1;
		}

		return (int) getSession().createNativeQuery("select max(id) from Scheduler").getSingleResult();
	}

	public List<PropertyGroupCatalog> getAllPropertyGroupCatalogList() {
		return getSession().createQuery("from PropertyGroupCatalog").getResultList();
	}

	public PropertyGroup getCatalogPropertyGroup(int id) {
		List<PropertyGroup> lpg = getSession()
				.createQuery("from PropertyGroup where type='catalog' and testcase_id=:id").setParameter("id", id)
				.getResultList();
		if (!lpg.isEmpty())
			return lpg.get(0);
		else
			return null;
	}

	public PropertyGroup getDefaultPropertGroupForTestCase(TestCase id) {
		return (PropertyGroup) getSession().createQuery("from TestCase where id=:id").setParameter("id", id)
				.getResultList();
	}

	public void savePropertyGroup(PropertyGroup pg) {
		getSession().saveOrUpdate(pg);
	}

	public void savePropertyGroupCatalog(PropertyGroupCatalog pgc) {
		getSession().saveOrUpdate(pgc);

	}

	public TestCaseProperty getPropertyOfTestStep(int testStep_id) {
		List<TestCaseProperty> ltcp = (List<TestCaseProperty>) getSession()
				.createQuery("from TestCaseProperty t where t.testStep.id=:id").setParameter("id", testStep_id)
				.getResultList();
		if (ltcp.isEmpty())
			return null;
		else
			return ltcp.get(0);
	}

	public List<Scheduler> getSchedulersToBeExecute(String sch_type) {
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Date date = new Date();
		return (List<Scheduler>) getSession()
				.createQuery("from Scheduler where status=:status and run_date <=:runDate and type=:type")
				.setParameter("status", "WAITING").setParameter("type", sch_type)
				.setParameter("runDate", dateFormat.format(date)).getResultList();
	}

	public List<Scheduler> getSchedulersRunning() {
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Date date = new Date();
		return (List<Scheduler>) getSession().createQuery("from Scheduler where status=:status and run_date <=:runDate")
				.setParameter("status", "RUNNING").setParameter("runDate", dateFormat.format(date)).getResultList();
	}
	// public List<TestCaseProperty> bringPropertiesOfGroup(PropertyGroup
	// selectedPropertyGroup) {
	// return (List<TestCaseProperty>) getSession().createQuery("from
	// TestCaseProperty where run_date <=:runDate")
	// .setParameter("runDate", dateFormat.format(date))
	// .getResultList();
	// }

	public Scheduler getSchedulerById(int i) {
		// TODO Auto-generated method stub
		return (Scheduler) getSession().createQuery("from Scheduler where id=:id").setParameter("id", i)
				.getSingleResult();
	}

	public List<TestCase> getTestCaseListByAppParam(AppParameter app) {
		// TODO Auto-generated method stub
		return (List<TestCase>) getSession().createNativeQuery(
				"select * from TestCase t where t.status='Aktif' and t.id in(select ts.testcase_id from teststep ts where ts.appParameter_id ="
						+ app.getId() + ")",
				TestCase.class).getResultList();

	}

	public void saveOrUpdateValidation(Set<StepValidation> selectedValidationList) {
		getSession().saveOrUpdate(selectedValidationList);

	}

}
