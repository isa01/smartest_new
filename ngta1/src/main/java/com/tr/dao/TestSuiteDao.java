package com.tr.dao;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.tr.model.Defect;
import com.tr.model.Report;
import com.tr.model.TestCase;
import com.tr.model.TestSuite;

import junit.framework.Test;

@Repository
public class TestSuiteDao extends GenericDao implements Serializable {

	private static final long serialVersionUID = 6767900867311590838L;

	public void saveUpdate(TestSuite ts) {
		int company_id = getCompanyId();
		ts.setCompany_id(company_id);
		getSession().saveOrUpdate(ts);
	}

	public TestSuite getTestSuiteByUrlName(String url, String name) {
		TestSuite ts = null;
		try {
			ts = (TestSuite) getSession().createQuery(
					"from TestSuite u left join fetch u.testCaseList where u.folderUrl=:url and u.name=:name and  u.status=:status")
					.setParameter("url", url).setParameter("name", name).setParameter("status", "Aktif").uniqueResult();

			List<TestCase> tclist = new ArrayList<TestCase>();
			for (TestCase tc : ts.getTestCaseList()) {
				tclist.add(tc);
			}

			ts.setTestCaseList(tclist);

		} catch (Exception ex) {
			System.out.println(ex.toString());
		}
		return ts;

	}

	public List<TestSuite> getListTestSuite() {
		int company_id = getCompanyId();
		List<TestSuite> list;
		try {
			list = getSession().createQuery("from TestSuite where status=:status   and company_id=:company_id")
					.setParameter("company_id", company_id).setParameter("status","Aktif").getResultList();

		} catch (Exception e) {
			list = new ArrayList<>();
		}

		return list;
	}

	public void deleteSuite(TestSuite ts) {

		getSession().createQuery("update TestSuite set status=:status where id=:id").setParameter("status", "Pasif")
				.setParameter("id", ts.getId()).executeUpdate();

	}

	// Test Execution

	public void createDefect(Defect df) {
		getSession().save(df);
	}

	public void saveReport(Report rep) {
		int company_id = getCompanyId();
		rep.setCompany_id(company_id);
		getSession().save(rep);
	}

	// Report işlemleri

	public List<Report> getListReport() {
		int company_id = getCompanyId();
		String a = "";
		List<Report> d;
		try {
			d = getSession().createQuery("from Report where status=:status  and company_id=:company_id")
					.setParameter("status", "Aktif").setParameter("company_id", company_id).getResultList();

		} catch (Exception ex) {
			d = new ArrayList<Report>();
		}

		return d;
	}

	public Long getNextKey() {
		Long key = null;
		try {
			Query query = getSession().createSQLQuery("select distinct next_val from hibernate_sequence ");
			key = ((BigInteger) ((org.hibernate.Query) query).uniqueResult()).longValue();
		} catch (Exception ex) {
			System.out.println(ex.toString());
		}

		return key;
	}

}
