package com.tr.dao;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Service;

import com.tr.model.Company;
import com.tr.model.GnlParameter;
import com.tr.model.User;
import com.tr.model.UserAuthorization;

@Service
public class UserDao extends GenericDao  implements Serializable{

	public User getUserById(int id){
		return  (User) getSession().createQuery("from User where id=:id").setParameter("id", id).uniqueResult();
	}
	@SuppressWarnings("deprecation")
	public User getUserByName(String name){
		return  (User) getSession().createQuery("from User where user_name=:name and status=:status")
				.setParameter("name", name)
				.setParameter("status", "Aktif")
				.uniqueResult();
	}
	
	public List<User> getUserList() 
	{
		return  getSession().createQuery("from User where status=:status")
				.setParameter("status", "Aktif")
				.list();
	}
	
	public int updatePassword(int id  , String password){
		
		Query query = getSession().createQuery("update User set password = :password" +
				" where id = :id");
		query.setParameter("id", id);
		query.setParameter("password", password);
		
		
	    return query.executeUpdate();
	    
	}
	
	
	//new user and update 
	public void createUser(User user){
		
		try{ 
				getSession().saveOrUpdate(user);  
				
		}catch(Exception ex){
			   System.out.println("Hata:"+ex);
		}
		
	}
	
	public void deleteUser(User user){
		
	//	getSession().getTransaction().begin();
		getSession().createQuery("update User set status = :status" +
				" where id = :id")
		          .setParameter("id", user.getId())
		          .setParameter("status", "Pasif")
		          .executeUpdate();
	
		
	}
	
	public List<UserAuthorization> getUserAuth(){
		 return getSession().createQuery("from UserAuthorization ")				
				.list();
	}
	
	//properties conf.
	
	public void loadProperties(){
		
		try{
			
		}catch(Exception ex){
			
		}
		
		
	}
	
	//gnl param
	
	public void saveParam(BigDecimal selectparam,GnlParameter saveparam){
		
		
		if(selectparam!=null)
		   getSession().createQuery("delete from GnlParameter " +
					" where id = :id")
		   .setParameter("id", selectparam)
		   .executeUpdate();
		
		getSession().save(saveparam);
		
	}
	
	public void deleteParam(BigDecimal id){
		getSession().createQuery("update GnlParameter set status = :status" +
				" where id = :id")
		          .setParameter("id", id)
		          .setParameter("status", "Pasif")
		          .executeUpdate();
	}
	
	public List<GnlParameter> getParamList(){
		
		return getSession().createQuery("from GnlParameter where status=:status")
				.setParameter("status", "Aktif")
				.list();
	}
	
	//user auth
	
	public void saveAuth(List<UserAuthorization> list){
		
		
		for(UserAuthorization u : list)
			if(!u.getUser_type().trim().equals(""))
			  getSession().saveOrUpdate(u);
	}
	
	public void delete(UserAuthorization u){
		getSession().delete(u);
	}
	public Company getCompanyByUsername(String username) {
		User user =  (User) getSession().createQuery("from User u join fetch u.company  where u.status=:status and u.user_name=:username")
				.setParameter("status", "Aktif")
				.setParameter("username", username).getSingleResult();
		return user.getCompany();
	}
	
	
}
