package com.tr.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;
import javax.naming.NamingException;
import javax.naming.Reference;
import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceUnitUtil;
import javax.persistence.Query;
import javax.persistence.SynchronizationType;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.metamodel.Metamodel;
import javax.servlet.http.HttpSession;

import org.hibernate.Cache;
import org.hibernate.HibernateException;
import org.hibernate.LockOptions;
import org.hibernate.Session;
import org.hibernate.SessionBuilder;
import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.StatelessSessionBuilder;
import org.hibernate.TypeHelper;
import org.hibernate.boot.spi.SessionFactoryOptions;
import org.hibernate.criterion.Example;
import org.hibernate.engine.spi.FilterDefinition;
import org.hibernate.metadata.ClassMetadata;
import org.hibernate.metadata.CollectionMetadata;
import org.hibernate.stat.Statistics;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.common.base.Preconditions;
import com.tr.controller.LoginController;
import com.tr.model.Company;
import com.tr.model.User;

import lombok.Getter;
import lombok.Setter;

public class GenericDao implements Serializable {

	@Autowired
	@Getter
	@Setter
	private SessionFactory sessionFactory;
	
    @ManagedProperty("#{loginController}")
    private LoginController loginController ;
    
    protected int company_id;
    transient HttpSession session;
  
//
//	public <T> List<T> findAllByExample(Class clazz, T example) {
//		Example _example = Example.create(example);
//		return getSession().createCriteria(clazz).add(_example).list();
//	}
//
//	public List findAll(Class clazz) {
//		return getSession().createCriteria(clazz).list();
//	}
//    
//	@SuppressWarnings("unchecked")
//	public <T> T merge(T transientObject) {
//		transientObject = (T) getSession().merge(transientObject);
//		return transientObject;
//	}
//
//	public void flushSessionChanges() {
//		getSession().flush();
//	}
//
//	public <T> void reattachUsingLock(T transientObject) {
//		getSession().buildLockRequest(LockOptions.NONE).lock(transientObject);
//	}
//
//	public <T> T load(Class<T> transientObjectClazz, String objectId) {
//		return (T) getSession().load(transientObjectClazz, objectId);
//	}
//
//	public Long getNextSequenceNumber(String sequenceName) {
//		Preconditions.checkNotNull(sequenceName, "Sequence ismi boş olamaz");
//		StringBuilder query = new StringBuilder();
//		query.append("select ");
//		query.append(sequenceName);
//		query.append(".nextval from dual");
//		return ((Number) getSession().createSQLQuery(query.toString()).uniqueResult()).longValue();
//	}
    public int getCompanyId() {
		session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
		String username = (String) session.getAttribute("username");
		User u = (User) getSession().createQuery("from User where user_name=:userName").setParameter("userName", username).getResultList().get(0);
		return u.getCompany().getId();
    }
	public Session getSession() {
		return sessionFactory.getCurrentSession();
	}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public LoginController getLoginController() {
		return loginController;
	}

	public void setLoginController(LoginController loginController) {
		this.loginController = loginController;
	}

	public Company getCompany() {
		 session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);  
    	return (Company) session.getAttribute("company"); 
	}

	public void setCompany_id(int company_id) {
		this.company_id = company_id;
	}


}
