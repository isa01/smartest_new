package com.tr.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.tr.model.UserAuthorization;
import com.tr.model.UserRoles;
import com.tr.service.GeneralServices;
import com.tr.service.PreferencesService;
import com.tr.service.UserService;

import lombok.Data;

@ManagedBean(name = "authorizationRolesController")
@Data
@SessionScoped
public class AuthorizationRolesController implements  Serializable{

	/**
	 *  
	 * 
	 */
	 private static final long serialVersionUID = 1L;
	 private static Logger logger =Logger.getLogger(AuthorizationRolesController.class);
	 
	 @ManagedProperty("#{userService}")
 	 private UserService userService; 
	 @ManagedProperty("#{generalServices}")
     private GeneralServices generalServices ;
	 @ManagedProperty("#{preferencesService}")
	 private PreferencesService preferencesService ;
	 
	 transient HttpSession session ;
     
     private UserAuthorization userAut, selectUserAuth ;
     private List<UserAuthorization> listUserAuth , selectAuthList ;
	
     private List<UserRoles> listUserRoles ;
     private UserRoles uRoles ;
     private String roleName ;
     
     
     public AuthorizationRolesController() throws IOException {
		
		 super();
		 session= (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true); 

		    if (session.getAttribute("userId") == null) {
		            ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
		            context.redirect(context.getRequestContextPath() + "/login.xhtml?faces-redirect=true");
		    }
		    
		    
	}

     @PostConstruct
     public void init(){
    	 uRoles=new UserRoles();
    	 userAut=new UserAuthorization();
    	 listUserAuth=userService.getUserAuth();
    	 listUserRoles=preferencesService.getUserRoles();
    	 selectAuthList=new ArrayList<UserAuthorization>();
     }

     public void saveAuth(){
    	 if(userAut.getUser_type()!=null && !userAut.getUser_type().trim().equals(""))
    	 {
	         listUserAuth.add(userAut);
	    	 userService.saveAuth(listUserAuth);
	    	 listUserAuth=userService.getUserAuth();
	    	 FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO,  generalServices.getBundle().getString("msgkayitbsrl"), null);
			 FacesContext.getCurrentInstance().addMessage(null, msg);
			 userAut=new UserAuthorization() ;
    	 }
		
     }
     
     public void addAuth(){
    	
    	
     }
     
     public void deleteAuth(UserAuthorization a){
      try{
    	 
    	 userService.delete(a);
    	 listUserAuth.remove(a);
         }catch(Exception ex){
        	 FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR,  "Yetki Silinemez .Role Bağlı !!!", null);
			  FacesContext.getCurrentInstance().addMessage(null, msg);
         }
    	 
     }
     
    
     
     public void saveRole(){
    	 
    	 uRoles=new UserRoles();
    	 uRoles.setUser_roles(roleName);
    	 uRoles.setUser_auth(selectAuthList);
    	 if(uRoles.getUser_auth()!=null && uRoles.getUser_auth().size()>0)
    	 {
    		 listUserRoles.add(uRoles);
    		 preferencesService.saveRole(listUserRoles);
        	 FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO,  generalServices.getBundle().getString("msgkayitbsrl"), null);
    		 FacesContext.getCurrentInstance().addMessage(null, msg);
    	 }else{
    		 FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR,  "Önce Yetki Seçiniz", null);
			  FacesContext.getCurrentInstance().addMessage(null, msg);
			
    	 }
    	 
    	 roleName=null;
    	 selectAuthList=null;
    	 uRoles=new UserRoles();
    	 
    	
		
     }
     
     public void listRole(){
    	 listUserRoles=preferencesService.getUserRoles();
    	 
    	 
     }
     
     public void deleteRole(UserRoles urole){
    	 listUserRoles.remove(urole);
    	 preferencesService.deleteRole(urole);
    	
		
     }
     
     public void  onRowSelectRole(){
    	 try{
    		 
    		 String z=uRoles.getUser_auth().get(0).getUser_type();
    		 
    		 for(UserAuthorization u : uRoles.getUser_auth())
    		      selectAuthList.add(u)   ;
        	 int x=selectAuthList.size();
    	 }catch(Exception ex){
    		 logger.error(ex.getMessage());
    	 }
    	
     }
     
     
     //---------getter setter------------
     

	public UserService getUserService() {
		return userService;
	}



	public void setUserService(UserService userService) {
		this.userService = userService;
	}



	public GeneralServices getGeneralServices() {
		return generalServices;
	}



	public void setGeneralServices(GeneralServices generalServices) {
		this.generalServices = generalServices;
	}



	public UserAuthorization getUserAut() {
		return userAut;
	}



	public void setUserAut(UserAuthorization userAut) {
		this.userAut = userAut;
	}



	public UserAuthorization getSelectUserAuth() {
		return selectUserAuth;
	}



	public void setSelectUserAuth(UserAuthorization selectUserAuth) {
		this.selectUserAuth = selectUserAuth;
	}



	public List<UserAuthorization> getListUserAuth() {
		return listUserAuth;
	}



	public void setListUserAuth(List<UserAuthorization> listUserAuth) {
		this.listUserAuth = listUserAuth;
	}

	public List<UserAuthorization> getSelectAuthList() {
		return selectAuthList;
	}

	public void setSelectAuthList(List<UserAuthorization> selectAuthList) {
		this.selectAuthList = selectAuthList;
	}

	public List<UserRoles> getListUserRoles() {
		return listUserRoles;
	}

	public void setListUserRoles(List<UserRoles> listUserRoles) {
		this.listUserRoles = listUserRoles;
	}

	public UserRoles getuRoles() {
		return uRoles;
	}

	public void setuRoles(UserRoles uRoles) {
		this.uRoles = uRoles;
	}

	public PreferencesService getPreferencesService() {
		return preferencesService;
	}

	public void setPreferencesService(PreferencesService preferencesService) {
		this.preferencesService = preferencesService;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
     
     
     
     
}
