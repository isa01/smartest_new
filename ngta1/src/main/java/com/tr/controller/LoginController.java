package com.tr.controller;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import com.tr.model.Company;
import com.tr.model.User;
import com.tr.service.GeneralServices;
import com.tr.service.UserService;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@ManagedBean(name = "loginController")
@Data
@SessionScoped
public class LoginController implements Serializable {

	/**
	 * 
	 */
	 private static final long serialVersionUID = 1L;
	 private static Logger logger =Logger.getLogger(LoginController.class);
	 
	 private String localeCode="tr";
	// private Locale locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
	 
	 
	 private String username="test";
	 private String password="1";
	 private int id;
	 HttpSession session;
	
	@ManagedProperty("#{userService}")
	private UserService userService;
	
	@ManagedProperty("#{generalServices}")
	private GeneralServices generalServices ;

	
	public LoginController() {
		super(); 	
	}
	
	
	@PostConstruct
	public void init() {
		
		
	}

	public String validateUser() {
		if (userService.getPasswordByName(username).equalsIgnoreCase(password)) {
			
			 id = userService.getIdByUsername(username);		
			 BigDecimal userauth=userService.getUserByName(username).getUserAuth();
			 
			  session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);            
              session.setAttribute("userId", id);
              session.setAttribute("username", username);
              session.setAttribute("userauth", userauth);
              Company company = userService.getCompanyByUsername(username);
              session.setAttribute("company",company);
			return "/index.xhtml?faces-redirect=true";
		}
		else {
						
			FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR,  generalServices.getBundle().getString("loginhata"), "Yanlis kullanici adi veya parola");
			FacesContext.getCurrentInstance().addMessage(null, msg);		
			
			return null;
		}
	}
	
	
	//logout
	public String logout(){
		try{
			
			  HttpSession session = (HttpSession) FacesContext.getCurrentInstance()
		                .getExternalContext().getSession(false);  
		       session.invalidate();	       
		}catch(Exception ex){
			logger.error(ex.toString());
		}
		 return "/login.xhtml?faces-redirect=true";

	}
	
	//getter setter 
	public List<User> getUserList()
	{
		return userService.getUserList();		
	}
	

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public GeneralServices getGeneralServices() {
		return generalServices;
	}

	public void setGeneralServices(GeneralServices generalServices) {
		this.generalServices = generalServices;
	}
	
     
	
	   //dil ayarı
	    public void changeLanguage(ValueChangeEvent e) {

	    	localeCode =  e.getNewValue().toString();

	        FacesContext.getCurrentInstance().getViewRoot().setLocale(new Locale(localeCode));

	    }

		public String getLocaleCode() {
			return localeCode;
		}

		public void setLocaleCode(String localeCode) {
			this.localeCode = localeCode;
		}

		public HttpSession getSession() {
			return session;
		}

		public void setSession(HttpSession session) {
			this.session = session;
		}

      

}