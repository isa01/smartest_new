package com.tr.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.Locale;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import org.primefaces.event.TabChangeEvent;

import com.tr.model.User;
import com.tr.service.GeneralServices;
import com.tr.service.UserService;

import lombok.Data;

@ManagedBean(name = "indexController")
@Data
@SessionScoped
public class IndexController implements Serializable {

	/**
	 * anasayfa işlemleri 
	 * 
	 */
	 private static final long serialVersionUID = 1L;
	 private static Logger logger =Logger.getLogger(IndexController.class);
     private String  username ;	
     transient HttpSession session;
     @ManagedProperty("#{loginController}")
     private LoginController loginController ;
       
 
	 
	 public IndexController() throws IOException {
	    super();
	     session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true); 

	    if (session.getAttribute("userId") == null) {
	            ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
	            context.redirect(context.getRequestContextPath() + "/login.xhtml?faces-redirect=true");
	    } else {
	    	
	    	username=(String) session.getAttribute("username") ; 
	    	 FacesContext.getCurrentInstance().getViewRoot().setLocale(new Locale("tr"));
	    }

     }

	 public void onload(){
		 FacesContext.getCurrentInstance().getViewRoot().setLocale(new Locale(loginController.getLocaleCode()));
	 }

	 
	 //getter setter 
		
	public String getUsername() {
		return username;
	}


	public void setUsername(String username) {
		this.username = username;
	}

	public LoginController getLoginController() {
		return loginController;
	}

	public void setLoginController(LoginController loginController) {
		this.loginController = loginController;
	}
	 
	

	
}
