package com.tr.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import com.tr.model.PreparedUrl;
import com.tr.service.GeneralServices;
import com.tr.service.PreferencesService;

import lombok.Data;

@ManagedBean(name = "urllistController")
@Data
@SessionScoped
public class UrlListController {

	 private static final long serialVersionUID = 1L;
	 private static Logger logger =Logger.getLogger(UrlListController.class);
	 
	 private PreparedUrl pu=new PreparedUrl();
	 private PreparedUrl selectedUrl ;
	 private List<PreparedUrl> urls=new ArrayList<PreparedUrl>();
	 private List<PreparedUrl> filterUrls;
	 transient HttpSession session ;
	 @ManagedProperty("#{preferencesService}")
	 private PreferencesService preferencesService ;
	 
	 @ManagedProperty("#{generalServices}")
     private GeneralServices generalServices ;

    public UrlListController() throws IOException {
		super();
		
		    session= (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true); 

		    if (session.getAttribute("userId") == null) {
		            ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
		            context.redirect(context.getRequestContextPath() + "/login.xhtml?faces-redirect=true");
		            FacesContext.getCurrentInstance().getViewRoot().setLocale(new Locale("tr"));
		    }
	}



	public synchronized  void saveElementList(){
//    	 
//    		preferencesService.getElementList(pu);
//    		pu=null ;
//    		
//    		FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO,  generalServices.getBundle().getString("msgkayitbsrl"), null);
//    		FacesContext.getCurrentInstance().addMessage(null, msg);
    }
	 
   public void deleteUrl(PreparedUrl url){
	   preferencesService.deleteUrl(url);
	  
	   FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO,  generalServices.getBundle().getString("msgsilmebsrl"), null);
	   FacesContext.getCurrentInstance().addMessage(null, msg);
   }
   
   
   public void reset(){
	   
	   pu=new PreparedUrl();
	   pu=null ;
   }
	 
	 //getter setter 
    
    
	public List<PreparedUrl> getUrls() {
		return preferencesService.getUrl();
	}


	public void setUrls(List<PreparedUrl> urls) {
		this.urls = urls;
	}

	public PreparedUrl getPu() {
		return pu;
	}

	public void setPu(PreparedUrl pu) {
		this.pu = pu;
	}

	public PreferencesService getPreferencesService() {
		return preferencesService;
	}

	public void setPreferencesService(PreferencesService preferencesService) {
		this.preferencesService = preferencesService;
	}

	public List<PreparedUrl> getFilterUrls() {
		return filterUrls;
	}

	public void setFilterUrls(List<PreparedUrl> filterUrls) {
		this.filterUrls = filterUrls;
	}

	public PreparedUrl getSelectedUrl() {
		return selectedUrl;
	}

	public void setSelectedUrl(PreparedUrl selectedUrl) {
		this.selectedUrl = selectedUrl;
		if(selectedUrl!=null){
			pu=selectedUrl; 
		}
	}

	public GeneralServices getGeneralServices() {
		return generalServices;
	}

	public void setGeneralServices(GeneralServices generalServices) {
		this.generalServices = generalServices;
	}
	
	
	
}
