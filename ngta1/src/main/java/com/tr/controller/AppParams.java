package com.tr.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.primefaces.model.DefaultTreeNode;

import com.tr.dao.GenericDao;
import com.tr.model.AppParamDetail;
import com.tr.model.AppParameter;
import com.tr.model.TestCase;
import com.tr.model.TestStep;
import com.tr.service.PreferencesService;
import com.tr.service.TestCaseService;

import lombok.Data;

@ManagedBean(name = "getElements")
@Data
@RequestScoped
public class AppParams extends GenericDao implements Serializable {

	@ManagedProperty(value = "#{param.text}")
	private String mainstring;

	@ManagedProperty("#{preferencesService}")
	PreferencesService preferencesService;

	@ManagedProperty("#{testCaseService}")
	private TestCaseService testCaseService;
	
	@ManagedProperty("#{testCaseController}")
	private TestCaseManageController testCaseController;
	
	private DefaultTreeNode selectedNode = new DefaultTreeNode();
	
	public void saveElement() {
//    	HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);  
//    	Company company = (Company) session.getAttribute("company"); 
    	
		List<AppParamDetail> appParamDetailList = new ArrayList<AppParamDetail>();
		AppParamDetail appdet = null;
		System.out.println("");
		selectedNode.setType("folder");
		selectedNode.setData("hb");
		TestCase tc=new TestCase();

		List<TestStep> testSteps = new ArrayList<TestStep>();
		
		String parts[] = mainstring.split("_Smartest_");
		AppParameter ap = new AppParameter();
		ap.setDescription(parts[1]); 		
		String action = parts[2];
		String parts_url[] = parts[3].split("/");
		ap.setPage(parts_url[parts_url.length-1]);

		
		TestStep ts = new TestStep();
		ts.setAppParameter(ap);
		ts.setStatus("Aktif");
		ts.setDescription(ap.getDescription());
		ts.setType("TestStep");
		ts.setPage(ap.getPage());
		ts.setAction_type(action);
		String testCaseName = parts[4];
		tc = testCaseService.getTestCase(testCaseName);
		if(tc.getId() == 0)
		{
			tc.setTestSteps(testSteps);
			tc.setName(testCaseName);
			tc.setStatus("Aktif");
			tc.setFolderUrl("/hb/");
			tc.setOpen_page(parts[3]);
			tc.setDescription(ts.getDescription());  
		}
		else
			tc = testCaseService.getTestCase(testCaseName);
		testSteps.add(ts);

		
			
		
		
		if (parts[0].contains("_ngta_")) {
			String attrs[] = parts[0].split("_ngta_");
			String textContent[] = attrs[0].split("=");
			if(textContent.length == 3 && !action.equals("select"))
			{
				ap.getAppParamDetailList().add(appdet);
				appdet = new AppParamDetail();
//				appdet.setApp_param_id(ap_id);
				appdet.setAction_type(action);
				appdet.setAttr_name(textContent[1]);
				appdet.setAttr_value(textContent[2].trim());
				appdet.setTag_name(textContent[0]); 
				appdet.setStatus("Aktif");
				appdet.setScript(getSeleniumScript(appdet)+getAppDetAction(appdet));
				appParamDetailList.add(appdet);
			}else {
			for (int i = 0; i < attrs.length; i++) {
				String[] key_value = attrs[i].split("=");
				if (key_value.length == 3 && !key_value[1].equalsIgnoreCase("type")  && !key_value[1]. equalsIgnoreCase("maxlength")  && !key_value[1].equalsIgnoreCase("autocomplete")  && !key_value[1].equalsIgnoreCase("for")) {
					ap.getAppParamDetailList().add(appdet);
					appdet = new AppParamDetail();
//					appdet.setApp_param_id(ap_id);
					appdet.setAction_type(action);
					appdet.setAttr_name(key_value[1]);
					appdet.setAttr_value(key_value[2].trim());
					appdet.setTag_name(key_value[0]); 
					appdet.setStatus("Aktif");
					appdet.setScript(getSeleniumScript(appdet)+getAppDetAction(appdet));
					appParamDetailList.add(appdet);
				}
			}
			}
//			preferencesService.saveAppParamDetailList(appParamDetailList);
			preferencesService.saveParameter(ap);
			appParamDetailList.clear();
			testCaseController.setAutoTestCaseCreateMode(true);
			testCaseController.setTestCase(tc);
			testCaseController.setSelectedNode(selectedNode);
			testCaseController.saveOrUpdateTestCaseForPlugin();	 
			testCaseController.setAutoTestCaseCreateMode(false);
		}

	}
	
	public String getSeleniumScript( AppParamDetail appParamDetail) {
		String attr_value = appParamDetail.getAttr_value();
		String attrs[] = attr_value.split("/");
		attr_value = attrs[attrs.length - 1];

		if (appParamDetail.getAttr_name().equalsIgnoreCase("textContent")
				|| appParamDetail.getTag_name().equalsIgnoreCase("label")
				|| appParamDetail.getTag_name().equalsIgnoreCase("span")
				|| appParamDetail.getTag_name().equalsIgnoreCase("a")) {
			if (appParamDetail.getAttr_name().equalsIgnoreCase("class")) {
				return "driver.findElements(By.cssSelector(" + "\"" + appParamDetail.getTag_name() + "["
						+ appParamDetail.getAttr_name() + "*='" + attr_value + "']\"));";
			} else {
				return "driver.findElements(By.xpath(\"//*[contains(text(),'" + attr_value + "')]\"));";
			}

		} 
//		else if (appParamDetail.getAttr_name().equalsIgnoreCase("xpath")) {
//			String rep_xpath = appParamDetail.getAttr_value();
//			String params[];
//			if (param1.contains("&")) {
//				params = param1.split("&");
//				if (rep_xpath.contains("param1"))
//					rep_xpath = rep_xpath.replaceAll("param1", params[0]);
//				if (rep_xpath.contains("param2"))
//					rep_xpath = rep_xpath.replaceAll("param2", params[1]);
//				if (rep_xpath.contains("param3"))
//					rep_xpath = rep_xpath.replaceAll("param2", params[2]);
//				if (rep_xpath.contains("param4"))
//					rep_xpath = rep_xpath.replaceAll("param2", params[3]);
//			} else {
//				if (rep_xpath.contains("param1"))
//					rep_xpath = rep_xpath.replaceAll("param1", param1);
//			}
//
//			return "driver.findElements(By.xpath(" + rep_xpath + "));";
//		}
		else if (appParamDetail.getTag_name().equalsIgnoreCase("select")
				&& appParamDetail.getAttr_name().equalsIgnoreCase("class")) {
			attr_value = appParamDetail.getAttr_value().trim().replaceAll(" ", ".");
			return "driver.findElements(By.cssSelector(\"select.\"" + attr_value + "\"))";
		}

		else {
			return "driver.findElements(By.cssSelector(" + "\"" + appParamDetail.getTag_name() + "["
					+ appParamDetail.getAttr_name() + "*='" + attr_value + "']\"));";
		}
	}
	
	private String getAppDetAction( AppParamDetail appParamDetail) {
//		if (param1.startsWith("$"))
//			param1 = map.get(param1.replace("$", ""));
		String action = appParamDetail.getAction_type();
//		WebElement element = getWebElement(driver, param1, appParamDetail.getId(), platform);
		String attr_value = appParamDetail.getAttr_value();
		String attrs[] = attr_value.split("/");
		attr_value = attrs[attrs.length - 1];

		switch (action) {
		// case "assertEquals":
		// if (appParamDetail.getAttr_name().equals("getCurrentUrl"))
		// Assert.assertEquals(driver.getCurrentUrl(), param1);
		// return true;
		// case "isExist":
		// if (appParamDetail.getAttr_name().equals("getCurrentUrl"))
		// Assert.assertFalse(false);
		// return true;
		// case "Assert.assertTrue":
		// if (appParamDetail.getAttr_name().equals("getCurrentUrl"))
		// Assert.assertEquals(driver.getCurrentUrl(), param1);
		// return true;

		case "click":
			return "element.click();";
//			if (element == null)
//				return false;
//			else {
//				try {
//					element.click();
//					logText += " Clicked <br/>";
//				} catch (Exception exp) {
//					return false;
//				}
//				return true;
//			}
		case "dblClick":
			return "Actions act = new Actions(driver); act.moveToElement(element).doubleClick().perform();";
			
//			Actions act = new Actions(driver);
//			if (element == null)
//				return false;
//			else {
//				try {
//					act.moveToElement(element).doubleClick().perform();
//					logText += " Double Clicked <br/>";
//				} catch (Exception exp) {
//					return false;
//				}
//				return true;
//			}

		case "type":
			return "element.sendKeys(\"$param1\");";
//			if (element == null)
//				return false;
//			else {
//				try {
//					element.sendKeys(param1);
//					logText += param1 + " Typed <br/>";
//				} catch (Exception exp) {
//					return true;
//				}
//				return true;
//			}

		case "select":
			return "Select slct = new Select(element);\r\n" + 
					"					slct.selectByVisibleText(\"$param1\");";

//			if (element == null)
//				return false;
//			else {
//				try {
//					Select slct = new Select(element);
//					slct.selectByVisibleText(param1);
//				} catch (Exception exp) {
//					return false;
//				}
//				return true;
//			}

		case "is_active":
			return "element.isEnabled();";
//			if (element == null)
//				return false;
//			else {
//				try {
//					element.isEnabled();
//				} catch (Exception exp) {
//					return false;
//				}
//				return true;
//			}
		default:
			return "";
		}
	}

	public PreferencesService getPreferencesService() {
		return preferencesService;
	}

	public void setPreferencesService(PreferencesService preferencesService) {
		this.preferencesService = preferencesService;
	}

	public String getMainstring() {
		return mainstring;
	}

	public void setMainstring(String mainstring) {
		this.mainstring = mainstring;
	}

	public TestCaseService getTestCaseService() {
		return testCaseService;
	}

	public void setTestCaseService(TestCaseService testCaseService) {
		this.testCaseService = testCaseService;
	}

	public TestCaseManageController getTestCaseController() {
		return testCaseController;
	}

	public void setTestCaseController(TestCaseManageController testCaseController) {
		this.testCaseController = testCaseController;
	}

}