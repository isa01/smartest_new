package com.tr.controller;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.junit.runner.JUnitCore; 
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.TreeDragDropEvent;
import org.primefaces.event.UnselectEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

import com.tr.model.AppParameter;
import com.tr.model.Folder;
import com.tr.model.GnlParameter;
import com.tr.model.PropertyGroup;
import com.tr.model.PropertyGroupCatalog;
import com.tr.model.ScheduleStep;
import com.tr.model.Scheduler;
import com.tr.model.TestCase;
import com.tr.model.TestCaseProperty;
import com.tr.model.TestStep;
import com.tr.model.TestSuite;
import com.tr.service.GeneralServices;
import com.tr.service.PreferencesService;
import com.tr.service.TestCaseService;
import com.tr.service.TestSuiteService;
import com.tr.service.UserService;
import com.tr.util.Smartest;

import lombok.Data;

@ManagedBean(name = "testExecutionController")
@Data
@SessionScoped
public class TestExecutionController implements Serializable {

	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(TestExecutionController.class);
	transient HttpSession session;

	// variables
	@ManagedProperty("#{preferencesService}")
	private PreferencesService preferencesService;

	@ManagedProperty("#{generalServices}")
	private GeneralServices generalServices;

	@ManagedProperty("#{testSuiteService}")
	private TestSuiteService testSuiteService;

	@ManagedProperty("#{testCaseService}")
	private TestCaseService testCaseService;

	@ManagedProperty("#{userService}")
	private UserService userService;

	// @ManagedProperty("#{ExecuteJob}")
	// private ExecuteJob executeJob;

	@ManagedProperty("#{testCaseController}")
	private TestCaseManageController testCaseController;

	@ManagedProperty("#{smartest}")
	private Smartest smartest;

	// tree variables

	// 1. tree
	private TreeNode root;
	private DefaultTreeNode selectedNode, tempSelectNode;
	private String tempUrl;
	Map<String, DefaultTreeNode> rootNodes = null;
	private List<TestCase> testCaseList;
	private List<TestSuite> testSuiteList;
	private String environment = "Server";
	private List<Scheduler> schedulerList;

	// 2. tree
	private TreeNode rootExec;
	private DefaultTreeNode selectedNodeExec;
	private List<Object> selectExec;
	private int rowKey; // taşımak istediğimiz liste elemanının ilk indexini saklar

	TestCase selectedTestCase;
	TestSuite selectedTestSuite;
	private List<GnlParameter> listBrowser;
	private String browser = "Chrome";
	private String interval = "Immediately";
	private Date executeDateTime = new Date();
	private Date currentDate = new Date();
	private Map<Integer, Integer> tc_pg_map = new HashMap<Integer, Integer>();
	private int schedulerid;
	private TestCaseProperty selectedProperty;
	private Set<TestCaseProperty> testCasePropertyList;
	private Set<PropertyGroup> propertyGroupList;
	private PropertyGroup propertyGroup;
	private PropertyGroupCatalog propertyGroupCatalog;
	private PropertyGroup selectedPropertyGroup;
	private String linkedPropertyString = null;
	private String linkedTestCaseString = null;
	private Set<TestCaseProperty> propertyListForOutput;
	private List<PropertyGroupCatalog> allPropertiesForOutput;
	private PropertyGroupCatalog toPropertyGroupPG;
	private String startURL = "https://dmzlastmile.hepsiexpress.com:8091/#/login";
	private Scheduler runningScheduler;
	private TestSuite selectedTs;
	private boolean isExecuting = false;
	private ArrayList<ScheduleStep> scheduleStepList;
	private Class<?> clazz;
	private ExternalContext context;
	private String testSuiteName;
	private boolean saveAstestSuite = false;

	public TestExecutionController() throws IOException {

		super();

		session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);

		if (session.getAttribute("userId") == null) {
			ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
			context.redirect(context.getRequestContextPath() + "/login.xhtml?faces-redirect=true");
			FacesContext.getCurrentInstance().getViewRoot().setLocale(new Locale("tr"));
		}

	}

	@PostConstruct
	public void init() {
		listBrowser = userService.getParamValueListByKey("tarayici");
		rootNodes = new HashMap<String, DefaultTreeNode>();
		selectExec = new ArrayList<Object>();
		isExecuting = false;
		rootExec = new DefaultTreeNode("RootSelect", null);
		rootExec.setExpanded(true);
		rootNodes.put("Seçiniz", new DefaultTreeNode("folder", "Seçiniz", rootExec));
		allPropertiesForOutput = new ArrayList<PropertyGroupCatalog>();
		root = new DefaultTreeNode("Root", null);
		loadTree();
		// allPropertiesForOutput = testCaseService.getAllPropertyGroupCatalogList();
		propertyListForOutput = new HashSet<TestCaseProperty>();
		root.setExpanded(true);
		listSchedulers();

		scheduleStepList = new ArrayList<ScheduleStep>();
	}

	public void runTests() {

	}

	public void createPropertyGroup() {
		propertyGroup = new PropertyGroup();
		propertyGroup.setTestCasePropertyList(new HashSet<TestCaseProperty>());
		for (TestCaseProperty tcp : propertyGroupCatalog.getTestCasePropertyList()) {
			TestCaseProperty ntcp = new TestCaseProperty();
			ntcp.setDescription(tcp.getDescription());
			ntcp.setInputoutput(tcp.getInputoutput());
			ntcp.setSourceStr(tcp.getSourceStr());
			ntcp.setTestStep(tcp.getTestStep());
			ntcp.setKey(tcp.getKey());
			ntcp.setValue(tcp.getValue());
			propertyGroup.getTestCasePropertyList().add(ntcp);
		}
		testCaseService.savePropertyGroup(propertyGroup);
	}

	public void savePropertyGroupCatalog() {
		// testCaseService.savePropertyGroupCatalog(propertyGroupCatalog);
	}

	public void saveAsPropertyGroup() {
		testCaseService.savePropertyGroup(propertyGroup);
		tc_pg_map.put(selectedTestCase.getId(), propertyGroup.getId());
		FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "İşlem Basarili", null);
		FacesContext.getCurrentInstance().addMessage(null, msg);
	}

	public void initializePropertyGroup() {
		if (tc_pg_map.get(selectedTestCase.getId()) == null) {
			propertyGroupCatalog = selectedTestCase.getPropertyGroupCatalog();
			propertyGroup = new PropertyGroup();
			propertyGroup.setTestCasePropertyList(new HashSet<TestCaseProperty>());
			for (TestCaseProperty tcp : propertyGroupCatalog.getTestCasePropertyList()) {
				TestCaseProperty ntcp = new TestCaseProperty();
				ntcp.setDescription(tcp.getDescription());
				ntcp.setInputoutput(tcp.getInputoutput());
				ntcp.setSourceStr(tcp.getSourceStr());
				ntcp.setTestStep(tcp.getTestStep());
				ntcp.setKey(tcp.getKey());
				ntcp.setValue(tcp.getValue());
				propertyGroup.getTestCasePropertyList().add(ntcp);
			}
		} else {
			propertyGroup = testCaseService.getPropertyGroupById(tc_pg_map.get(selectedTestCase.getId()));
		}

	}

	// case'leri execute etme
	public void schedule(String sch_type) throws IOException {
		propertyGroup.setTestCasePropertyList(new HashSet<TestCaseProperty>());
		if (!sch_type.equals("Test"))
			sch_type = "Regular";
		try {
			TestCase tc;
			List<TestCase> tcList = new ArrayList<TestCase>();

			schedulerid =  testCaseService.getNextSchedulerId();
			TestSuite ts;
			for (Object s : selectExec) {
				if (s.toString().contains("TestCase")) {
					tc = (TestCase) s;
					addToScheduler(tc, sch_type);

				} else {
					ts = (TestSuite) s;
					tcList = ts.getTestCaseList();
					for (TestCase tCase : tcList)
						addToScheduler(tCase, sch_type);
				}
			}
			tc_pg_map.clear();
			FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO,
					generalServices.getBundle().getString("islemtm"), null);
			FacesContext.getCurrentInstance().addMessage(null, msg);
			propertyGroup = new PropertyGroup();
			allPropertiesForOutput = new ArrayList<PropertyGroupCatalog>();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
		context.redirect(context.getRequestContextPath() + "/scheduler.xhtml");
	}

	public void saveListAsTestSuitee() {
		TestSuite ts = new TestSuite();
		ts.setFolderUrl("//TestSuite//");
		ts.setName(testSuiteName);
		TestCase tc;
		for (Object s : selectExec) {
			if (s.toString().contains("TestCase")) {
				tc = (TestCase) s;
				ts.getTestCaseList().add(tc);
			}
		}
		if (ts != null && validateTestSuiteName(ts.getName())) {
			testSuiteService.saveUpdate(ts);
			FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO,
					generalServices.getBundle().getString("islemtm"), null);
			FacesContext.getCurrentInstance().addMessage(null, msg);
		} else {
			FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Bu Test Suite var", null);
			FacesContext.getCurrentInstance().addMessage(null, msg);
		}
	}

	private boolean validateTestSuiteName(String testSuiteName) {
		boolean is = true;
		String url = getUrl(selectedNode) + "/";
		TestSuite tempTS = testSuiteService.getTestSuiteByUrlName(url, testSuiteName);

		if (tempTS != null)
			is = false;
		else
			is = true;
		return is;
	}

	public void test() throws IOException {
		schedule("Test");
		System.out.println("Job Start Running");
		ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
		context.redirect(context.getRequestContextPath() + "/scheduler.xhtml");

		List<Scheduler> listScheduler = testCaseService.getSchedulersToBeExecute("Test");
		for (Scheduler sch : listScheduler) {
			sch.setStatus("RUNNING");
			testCaseService.saveScheduler(sch);
		}
		new Thread(() -> {
			for (Scheduler sch : listScheduler) {
				runningScheduler = sch;
				TestCase tc = testCaseService.getTestCaseByIdExcludePGC(runningScheduler.getTestcase_id());
				try {
					scheduleStepList = new ArrayList<ScheduleStep>(runningScheduler.getScheduleSteps());
					runTestCase(tc, runningScheduler);

				} catch (Exception exp) {
				}
			}
		}).start();
		propertyGroup = new PropertyGroup();
		allPropertiesForOutput = new ArrayList<PropertyGroupCatalog>();

	}

	public void pollRunningstatus() {
		if (runningScheduler.getStatus().equals("RUNNING")) {
			System.out.println("refreshhinnggggg");
			runningScheduler = testCaseService.getSchedulerById(runningScheduler.getId());
			scheduleStepList = new ArrayList<ScheduleStep>(runningScheduler.getScheduleSteps());
		}
	}

	public void runAllAvailableTestCase() throws IOException {
		context = FacesContext.getCurrentInstance().getExternalContext();
		context.redirect(context.getRequestContextPath() + "/scheduler.xhtml");
		System.out.println("Job Start Running");
		List<Scheduler> listScheduler = testCaseService.getSchedulersToBeExecute("Regular");
		for (Scheduler sch : listScheduler) {
			sch.setStatus("RUNNING");
			testCaseService.saveScheduler(sch);
		}
		new Thread(() -> {
			for (Scheduler sch : listScheduler) {
				runningScheduler = sch;
				TestCase tc = testCaseService.getTestCaseByIdExcludePGC(runningScheduler.getTestcase_id());
				try {
					scheduleStepList = new ArrayList<ScheduleStep>(runningScheduler.getScheduleSteps());
					runTestCase(tc, runningScheduler);
					// File f = new File(testCaseService.getParamValue("testCaseFolder") +
					// tc.getFolderUrl() + tc.getName() + ".java");
					// f.delete();
				} catch (Exception exp) {
				}
			}
		}).start();
	}

	private void runTestCase(TestCase tc, Scheduler sch) {

		System.out.println(tc.getName() + " TestCase Preparing");

		JUnitCore junit = new JUnitCore();

		try {
			String packageName = "com.tr.testCaseFiles" + tc.getFolderUrl();
			packageName = packageName.replace('/', '.');
			clazz = Class.forName(packageName + tc.getName() + "_" + sch.getId());
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			sch.setStatus("TestNotFound");
			testCaseService.saveScheduler(sch);
			return;
		}
		try {
			System.out.println("\n" + tc.getName() + " TestCase Starting");

			junit.run(clazz);

			// testCaseService.deleteClassFile(tc.getFolderUrl() + tc.getName());
		} catch (Exception exp) {
		}
	}

	private void addToScheduler(TestCase tc, String sch_type) {
		session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
		String username = (String) session.getAttribute("username");
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Scheduler sch = new Scheduler();
		sch.setCompany_id(tc.getCompany_id());
		sch.setCreated_by(username);
		List<TestStep> lts = testCaseService.getTestStepById(tc.getId());
		Collections.sort(lts);
		int orderNum = 0;
		for (TestStep ts : lts) {
			if (ts.getType().equals("TestCase")) {
				List<TestStep> lts1 = testCaseService.getTestStepById(ts.getTemplateId());
				Collections.sort(lts1);
				for (TestStep ts1 : lts1) {
					ScheduleStep ss = new ScheduleStep();
					ss.setName(ts1.getDescription());
					ss.setTestStep_id(ts1.getId());
					ss.setOrderNum(orderNum);
					orderNum++;
					sch.getScheduleSteps().add(ss);
				}
			} else {
				ScheduleStep ss = new ScheduleStep();
				ss.setName(ts.getDescription());
				ss.setTestStep_id(ts.getId());
				ss.setOrderNum(orderNum);
				orderNum++;
				sch.getScheduleSteps().add(ss);
			}
		}

		Integer pg_id = tc_pg_map.get(tc.getId());
		if (pg_id == null) {
			createPropertyGroup();
			sch.setPropertyGroup(propertyGroup);
		} else
			sch.setPropertyGroup(testCaseService.getPropertyGroupById(tc_pg_map.get(tc.getId())));
		Date date = new Date();
		dateFormat.format(date);
		sch.setBrowser(browser);
		sch.setType(sch_type);
		sch.setEnvironment(environment);
		sch.setRun_date(dateFormat.format(executeDateTime));
		if (interval.equals("ALLWAYS"))
			sch.setStatus("ALLWAYS");
		else
			sch.setStatus("WAITING");
		sch.setInterval(interval);
		sch.setTestcase_id(tc.getId());
		sch.setStartURL(startURL);
		sch.setTestcase_name(tc.getFolderUrl() + ":" + tc.getName());
		sch.setSchedulerid(schedulerid);
		testCaseService.saveScheduler(sch);
		try {
			tc.setTestSteps(testCaseService.getTestStepById(tc.getId()));
			tc.setOpen_page(sch.getStartURL());
			testCaseService.createTestCaseFile(tc, sch, testCaseController.getUsername());
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	public void listSchedulers() {
		schedulerList = testCaseService.getSchedulers();

	}

	public void onRowSelectPropertyGroup(SelectEvent event) {
		propertyListForOutput.clear();
		propertyListForOutput.addAll(toPropertyGroupPG.getTestCasePropertyList());
		linkedTestCaseString = "" + toPropertyGroupPG.getTestCase().getId() + "_"
				+ toPropertyGroupPG.getTestCase().getName();
	}

	public void onRowSelectProperty(SelectEvent event) {
		TestCaseProperty tcp = (TestCaseProperty) event.getObject();
		linkedPropertyString = linkedTestCaseString + "_" + tcp.getKey();
		selectedProperty.setSourceStr(linkedPropertyString);
	}

	public void loadTree() {

		try {

			root.getChildren().clear();

			testCaseList = testCaseService.getAllTestCase();
			rootNodes = new HashMap<String, DefaultTreeNode>();

			// add folders
			BigDecimal uu = (BigDecimal) session.getAttribute("userauth");
			List<Folder> lFolder = preferencesService.getFolderList(uu.toString());
			String parent = null;

			for (Folder f : lFolder) {

				String[] parts = f.getFolder_url().split("/");
				if (parts.length > 2) {
					parent = parts[parts.length - 2];
					rootNodes.put(f.getName(), new DefaultTreeNode("folder", f.getName(), rootNodes.get(parent)));
				} else
					rootNodes.put(f.getName(), new DefaultTreeNode("folder", f.getName(), root));
			}
			// add testcase

			for (TestCase testCase : testCaseList) {
				TreeNode tcase = root;
				String url = testCase.getFolderUrl();
				if (isCaseSuiteAuth(url))
					continue;
				String[] parts = url.split("/");

				for (int i = 1; i < parts.length; i++) {

					for (int x = 0; x < tcase.getChildCount(); x++) {

						if (parts[i].equals(tcase.getChildren().get(x).getData().toString())) {
							tcase = tcase.getChildren().get(x);
						}
					}
				}

				rootNodes.put(testCase.getName(), new DefaultTreeNode("testcase", testCase.getName(), tcase));

			}

			// add Test Suite
			testSuiteList = testSuiteService.getListTestSuite();

			for (TestSuite testSuite : testSuiteList) {
				TreeNode tcase = root;
				String url = testSuite.getFolderUrl();
				if (isCaseSuiteAuth(url))
					continue;
				String[] parts = url.split("/");

				for (int i = 1; i < parts.length; i++) {

					for (int x = 0; x < tcase.getChildCount(); x++) {

						if (parts[i].equals(tcase.getChildren().get(x).getData().toString())) {
							tcase = tcase.getChildren().get(x);
						}
					}
				}

				rootNodes.put(testSuite.getName(), new DefaultTreeNode("testsuite", testSuite.getName(), tcase));

			}

		} catch (Exception ex) {
			logger.error(ex.toString());
		}

	}

	// tree user a göre yetkilendirme kontrolü
	private boolean isCaseSuiteAuth(String url) {

		boolean b = true;
		BigDecimal uu = (BigDecimal) session.getAttribute("userauth");
		List<Folder> lFolder = preferencesService.getFolderList(uu.toString());

		for (Folder f : lFolder) {
			if (f.getFolder_url().equals(url)) {
				b = false;
				break;
			}
		}

		return b;
	}

	public void loadTreeExec() {

		rootExec.getChildren().clear();

		if (selectExec == null || selectExec.size() == 0) {
			rootNodes.put("Seçiniz", new DefaultTreeNode("folder", "Seçiniz", rootExec));
		}

		for (Object s : selectExec) {
			String x = s.toString();
			TreeNode node = rootExec;

			if (x.contains("TestCase")) {
				TestCase t = (TestCase) s;
				rootNodes.put(t.getName(), new DefaultTreeNode("testcase", t.getName(), rootExec));

			} else if (x.contains("TestSuite")) {
				TestSuite t = (TestSuite) s;
				rootNodes.put(t.getName(), new DefaultTreeNode("testsuite", t.getName(), rootExec));

				for (TestCase tc : t.getTestCaseList()) {
					rootNodes.put(tc.getName(), new DefaultTreeNode("testcase", tc.getName(),
							rootExec.getChildren().get(rootExec.getChildCount() - 1)));
				}
			}

		}
	}

	// functions

	public void onDragDrop1(TreeDragDropEvent event) {

	}

	public void onDragDrop(TreeDragDropEvent event) {

		TreeNode dragNode = event.getDragNode(); // taşınan
		TreeNode dropNode = event.getDropNode(); // yeni taşınan
		tempSelectNode = (DefaultTreeNode) dragNode;
		if (tempSelectNode != null && tempSelectNode.equals(dragNode)) {

			if (dragNode.getType().equals("folder")) {
				// folder taşınamaz
				FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Klasör Taşınamaz", null);
				FacesContext.getCurrentInstance().addMessage(null, msg);

			} else if (dragNode.getType().equals("testcase")) {
				TestCase tc = testCaseService.getTestCaseByUrlName(tempUrl, dragNode.getData().toString()).get(0);
				selectExec.add(tc);

			} else if (dragNode.getType().equals("testsuite")) {
				TestSuite ts = testSuiteService.getTestSuiteByUrlName(tempUrl, dragNode.getData().toString());
				selectExec.add(ts);
			}

		} else if (dragNode.getParent().getData().toString().equals("RootSelect")) {

			int a = Integer.parseInt(dragNode.getRowKey().toString());
			reloadList(a, rowKey);

		} else {
			FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Taşımak istediğiniz Case-Suite Seçiniz",
					null);
			FacesContext.getCurrentInstance().addMessage(null, msg);
		}

		loadTreeExec();
		loadTree();
		tempSelectNode = null;
		tempUrl = null;

	}

	public String getUrl(DefaultTreeNode selected) {

		TreeNode tnode = selected.getParent();
		String url = tnode.getData().toString();
		while (true) {

			if (tnode != null & tnode.getParent() != null && !tnode.getParent().getData().toString().equals("Root")) {
				tnode = tnode.getParent();
				url = tnode.getData().toString() + "/" + url;
			} else {
				break;
			}
		}

		url = "/" + url + "/";
		return url;

	}

	public void onRowSelect() {

		if (selectedNode == null){
			FacesContext.getCurrentInstance().addMessage(null,new FacesMessage("Selected Node cannot be null, please select something."));

			return;
		}

		tempSelectNode = selectedNode;
		tempUrl = getUrl(tempSelectNode);
		if (selectedNode.getType().equals("testcase")) {
			selectedTestCase = testCaseService.getTestCaseByUrlName(tempUrl, selectedNode.getData().toString()).get(0);
			addToExec();

		} else if (selectedNode.getType().equals("testsuite")) {
			addToExec();
			selectedTs = testSuiteService.getTestSuiteByUrlName(tempUrl, selectedNode.getData().toString());
		}
	}

	public void onRowSelect2() {

		if (selectedNodeExec == null){
			FacesContext.getCurrentInstance().addMessage(null,new FacesMessage("Selected NodeExec cannot be null, please select something."));
		}

		Object s = selectExec.get(Integer.parseInt(selectedNodeExec.getRowKey().toString()));
		if (s.toString().contains("TestCase")) {
			selectedTestCase = (TestCase) s;
			initializePropertyGroup();
		} else
			selectedTestSuite = (TestSuite) s;
		rowKey = Integer.parseInt(selectedNodeExec.getRowKey().toString());
	}

	public void addToExec() {

		if (selectedNode.getType().equals("testcase")) {
			selectExec.add(selectedTestCase);
			allPropertiesForOutput.add(selectedTestCase.getPropertyGroupCatalog());
		} else if (selectedNode.getType().equals("testsuite")) {
			selectExec.add(selectedTs);
		}

		loadTreeExec();
		loadTree();
		tempSelectNode = null;
		tempUrl = null;
		initializePropertyGroup();
	}

	public void removeFromExec(NodeSelectEvent event) {
		if (selectedNode.getType().equals("testcase")) {
			selectExec.remove(selectedTestCase);
			allPropertiesForOutput.remove(selectedTestCase.getPropertyGroupCatalog());
		} else if (selectedNode.getType().equals("testsuite")) {
			selectExec.remove(selectedTs);
		}
		loadTreeExec();
		loadTree();
		tempSelectNode = null;
		tempUrl = null;

	}

	public void onRowUnSelect(UnselectEvent event) {
		selectedNode = null;
	}

	public void delete() {

		TreeNode slct = selectedNodeExec;
		if (slct.getParent().getData().toString().equals("RootSelect")) {

			int y = Integer.parseInt(slct.getRowKey().toString());
			selectExec.remove(y);
		} else {
			FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Bağlı Olduğu Test Suite Silebilirsiniz",
					null);
			FacesContext.getCurrentInstance().addMessage(null, msg);
		}

		loadTreeExec();
	}

	// 2. tree liste elemanının yerini değiştirme funct.
	public void reloadList(int newIndex, int oldIndex) {

		Collections.swap(selectExec, oldIndex, newIndex);
	}

	public void testTestCase() throws IOException {
		selectExec.clear();
		selectExec.add(testCaseController.getTestCase());
		schedule("Test");
	}

	public void saveScheduler(Scheduler sch) {
		if (sch.getStatus().equals("WAITING")) {
			for (ScheduleStep ss : sch.getScheduleSteps()) {
				ss.setStatus("WAITING");
				ss.setSs_url("");
				ss.setDescription("");
			}
		}
		testCaseService.saveScheduler(sch);
		FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "İşlem Basarili", null);
		FacesContext.getCurrentInstance().addMessage(null, msg);
	}

	// getter setter

	public PreferencesService getPreferencesService() {
		return preferencesService;
	}

	public void setPreferencesService(PreferencesService preferencesService) {
		this.preferencesService = preferencesService;
	}

	public GeneralServices getGeneralServices() {
		return generalServices;
	}

	public void setGeneralServices(GeneralServices generalServices) {
		this.generalServices = generalServices;
	}

	public TestSuiteService getTestSuiteService() {
		return testSuiteService;
	}

	public void setTestSuiteService(TestSuiteService testSuiteService) {
		this.testSuiteService = testSuiteService;
	}

	public TestCaseService getTestCaseService() {
		return testCaseService;
	}

	public void setTestCaseService(TestCaseService testCaseService) {
		this.testCaseService = testCaseService;
	}

	public TreeNode getRoot() {
		return root;
	}

	public void setRoot(TreeNode root) {
		this.root = root;
	}

	public DefaultTreeNode getSelectedNode() {
		return selectedNode;
	}

	public void setSelectedNode(DefaultTreeNode selectedNode) {
		this.selectedNode = selectedNode;
	}

	public TreeNode getRootExec() {
		return rootExec;
	}

	public void setRootExec(TreeNode rootExec) {
		this.rootExec = rootExec;
	}

	public DefaultTreeNode getSelectedNodeExec() {
		return selectedNodeExec;
	}

	public void setSelectedNodeExec(DefaultTreeNode selectedNodeExec) {
		this.selectedNodeExec = selectedNodeExec;
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public List<GnlParameter> getListBrowser() {
		return listBrowser;
	}

	public void setListBrowser(List<GnlParameter> listBrowser) {
		this.listBrowser = listBrowser;
	}

	public TestCaseManageController getTestCaseController() {
		return testCaseController;
	}

	public void setTestCaseController(TestCaseManageController testCaseController) {
		this.testCaseController = testCaseController;
	}

	public String getEnvironment() {
		return environment;
	}

	public void setEnvironment(String environment) {
		this.environment = environment;
	}

	public String getBrowser() {
		return browser;
	}

	public void setBrowser(String browser) {
		this.browser = browser;
	}

	public Date getCurrentDate() {
		return currentDate;
	}

	public static Logger getLogger() {
		return logger;
	}

	public static void setLogger(Logger logger) {
		TestExecutionController.logger = logger;
	}

	public HttpSession getSession() {
		return session;
	}

	public void setSession(HttpSession session) {
		this.session = session;
	}

	public DefaultTreeNode getTempSelectNode() {
		return tempSelectNode;
	}

	public void setTempSelectNode(DefaultTreeNode tempSelectNode) {
		this.tempSelectNode = tempSelectNode;
	}

	public String getTempUrl() {
		return tempUrl;
	}

	public void setTempUrl(String tempUrl) {
		this.tempUrl = tempUrl;
	}

	public Map<String, DefaultTreeNode> getRootNodes() {
		return rootNodes;
	}

	public void setRootNodes(Map<String, DefaultTreeNode> rootNodes) {
		this.rootNodes = rootNodes;
	}

	public List<TestCase> getTestCaseList() {
		return testCaseList;
	}

	public void setTestCaseList(List<TestCase> testCaseList) {
		this.testCaseList = testCaseList;
	}

	public List<TestSuite> getTestSuiteList() {
		return testSuiteList;
	}

	public void setTestSuiteList(List<TestSuite> testSuiteList) {
		this.testSuiteList = testSuiteList;
	}

	public List<Object> getSelectExec() {
		return selectExec;
	}

	public void setSelectExec(List<Object> selectExec) {
		this.selectExec = selectExec;
	}

	public int getRowKey() {
		return rowKey;
	}

	public void setRowKey(int rowKey) {
		this.rowKey = rowKey;
	}

	public String getInterval() {
		return interval;
	}

	public void setInterval(String interval) {
		this.interval = interval;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void setCurrentDate(Date currentDate) {
		this.currentDate = currentDate;
	}

	public Date getExecuteDateTime() {
		return executeDateTime;
	}

	public void setExecuteDateTime(Date executeDateTime) {
		this.executeDateTime = executeDateTime;
	}

	public Smartest getSmartest() {
		return smartest;
	}

	public void setSmartest(Smartest smartest) {
		this.smartest = smartest;
	}

	public List<Scheduler> getSchedulerList() {
		return schedulerList;
	}

	public void setSchedulerList(List<Scheduler> schedulerList) {
		this.schedulerList = schedulerList;
	}

	public Map<Integer, Integer> getTc_pg_map() {
		return tc_pg_map;
	}

	public void setTc_pg_map(Map<Integer, Integer> tc_pg_map) {
		this.tc_pg_map = tc_pg_map;
	}

	public int getSchedulerid() {
		return schedulerid;
	}

	public void setSchedulerid(int schedulerid) {
		this.schedulerid = schedulerid;
	}

	public TestCase getSelectedTestCase() {
		return selectedTestCase;
	}

	public void setSelectedTestCase(TestCase selectedTestCase) {
		this.selectedTestCase = selectedTestCase;
	}

	public TestSuite getSelectedTestSuite() {
		return selectedTestSuite;
	}

	public void setSelectedTestSuite(TestSuite selectedTestSuite) {
		this.selectedTestSuite = selectedTestSuite;
	}

	public TestCaseProperty getSelectedProperty() {
		return selectedProperty;
	}

	public void setSelectedProperty(TestCaseProperty selectedProperty) {
		this.selectedProperty = selectedProperty;
	}

	public Set<TestCaseProperty> getTestCasePropertyList() {
		return testCasePropertyList;
	}

	public void setTestCasePropertyList(Set<TestCaseProperty> testCasePropertyList) {
		this.testCasePropertyList = testCasePropertyList;
	}

	public Set<PropertyGroup> getPropertyGroupList() {
		return propertyGroupList;
	}

	public void setPropertyGroupList(Set<PropertyGroup> propertyGroupList) {
		this.propertyGroupList = propertyGroupList;
	}

	public PropertyGroup getPropertyGroup() {
		return propertyGroup;
	}

	public void setPropertyGroup(PropertyGroup propertyGroup) {
		this.propertyGroup = propertyGroup;
	}

	public PropertyGroupCatalog getPropertyGroupCatalog() {
		return propertyGroupCatalog;
	}

	public void setPropertyGroupCatalog(PropertyGroupCatalog propertyGroupCatalog) {
		this.propertyGroupCatalog = propertyGroupCatalog;
	}

	public PropertyGroup getSelectedPropertyGroup() {
		return selectedPropertyGroup;
	}

	public void setSelectedPropertyGroup(PropertyGroup selectedPropertyGroup) {
		this.selectedPropertyGroup = selectedPropertyGroup;
	}

	public String getLinkedPropertyString() {
		return linkedPropertyString;
	}

	public void setLinkedPropertyString(String linkedPropertyString) {
		this.linkedPropertyString = linkedPropertyString;
	}

	public String getLinkedTestCaseString() {
		return linkedTestCaseString;
	}

	public void setLinkedTestCaseString(String linkedTestCaseString) {
		this.linkedTestCaseString = linkedTestCaseString;
	}

	public Set<TestCaseProperty> getPropertyListForOutput() {
		return propertyListForOutput;
	}

	public void setPropertyListForOutput(Set<TestCaseProperty> propertyListForOutput) {
		this.propertyListForOutput = propertyListForOutput;
	}

	public List<PropertyGroupCatalog> getAllPropertiesForOutput() {
		return allPropertiesForOutput;
	}

	public void setAllPropertiesForOutput(List<PropertyGroupCatalog> allPropertiesForOutput) {
		this.allPropertiesForOutput = allPropertiesForOutput;
	}

	public PropertyGroupCatalog getToPropertyGroupPG() {
		return toPropertyGroupPG;
	}

	public void setToPropertyGroupPG(PropertyGroupCatalog toPropertyGroupPG) {
		this.toPropertyGroupPG = toPropertyGroupPG;
	}

	public String getStartURL() {
		return startURL;
	}

	public void setStartURL(String startURL) {
		this.startURL = startURL;
	}

	public Scheduler getRunningScheduler() {
		return runningScheduler;
	}

	public void setRunningScheduler(Scheduler runningScheduler) {
		this.runningScheduler = runningScheduler;
	}

	public ArrayList<ScheduleStep> getScheduleStepList() {
		return scheduleStepList;
	}

	public void setScheduleStepList(ArrayList<ScheduleStep> scheduleStepList) {
		this.scheduleStepList = scheduleStepList;
	}

	public TestSuite getSelectedTs() {
		return selectedTs;
	}

	public void setSelectedTs(TestSuite selectedTs) {
		this.selectedTs = selectedTs;
	}

	public String getTestSuiteName() {
		return testSuiteName;
	}

	public void setTestSuiteName(String testSuiteName) {
		this.testSuiteName = testSuiteName;
	}

	public boolean isSaveAstestSuite() {
		return saveAstestSuite;
	}

	public void setSaveAstestSuite(boolean saveAstestSuite) {
		this.saveAstestSuite = saveAstestSuite;
	}
}
