package com.tr.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.primefaces.event.SelectEvent;

import com.tr.model.Comments;
import com.tr.model.Defect;
import com.tr.model.User;
import com.tr.service.DefectServices;
import com.tr.service.GeneralServices;
import com.tr.service.TestSuiteService;
import com.tr.service.UserService;

import lombok.Data;

@ManagedBean(name = "defectController")
@Data
@SessionScoped
public class DefectController implements  Serializable {
	
	/**
	 * Defect listesi ve kontrollerini sağlar  
	 * defect
	 */
	 private static final long serialVersionUID = 1L;
	 private static Logger logger =Logger.getLogger(DefectController.class);
	 
	 @ManagedProperty("#{userService}")
 	 private UserService userService;
	 
	 @ManagedProperty("#{generalServices}")
     private GeneralServices generalServices ;
	 
	 @ManagedProperty("#{defectServices}")
     private DefectServices defectServices ;
	 
     transient HttpSession session ;
     
     //datatable variable
     private Defect selectdfc  ;
     private List<Defect> listDefect , filterDefect ;
     private List<User> listUser=new ArrayList<User>() ;
     
     private Defect defect ;
     private String comments ;
     private List<Comments> listComments ;
     private List<Comments> listCommentsDefect ;
     private Defect tempDefect  ; // silmek istediğimiz comments in defect ini tutar.
     private boolean isDefectComments=false ;
     
     //gnl param 
     private List<String> listSeverity=new ArrayList<String>();
     private List<String> listStatu=new ArrayList<String>();
     
	public DefectController() throws IOException {
	   	 super();
		 session= (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true); 

		    if (session.getAttribute("userId") == null) {
		            ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
		            context.redirect(context.getRequestContextPath() + "/login.xhtml?faces-redirect=true");
		    }
		
	}
	
	@PostConstruct
	public void init(){
		
		listUser=userService.getUserList();
		listSeverity=userService.getParamValueList("severity");
		listStatu=userService.getParamValueList("statu");
		reset();
	}
	
	
	public void getDefectlist(){
		listDefect=defectServices.getListDefect();
	}
	
	
	public void reset(){
		listComments=new ArrayList<Comments>();
		listCommentsDefect=new ArrayList<Comments>();
		defect=new Defect();
		comments=null;
		isDefectComments=false ;
		getDefectlist();
	}
	
	
	// yeni defect in comment işlemleri
	public void addlistComment(){
		
		if(comments!=null &&  !comments.trim().equals("") )
		{
			Comments c=new Comments();
			c.setComments(comments);
			listComments.add(c);
			comments=null;
		}
		
	}
	
	public void removeComment(Comments c){
		
		listComments.remove(c);
		
	}
	
	// kayıt func
	public void save(){
		
		try{
			defect.setCreated_date(new Date());
			defect.setCreated_by((String) session.getAttribute("username"));
//			int userId=(int) session.getAttribute("userId") ;
//			defect.setAssign_user(userService.getUser(String.valueOf(userId)));

		    defectServices.createDefect(defect);
		  
		    
		    for(int i=0 ; i<listComments.size() ; i++ )
		    {
		    	listComments.get(i).setDefectId(defect.getId());
		    	listComments.get(i).setUserName(defect.getCreated_by());
		    }
		    defectServices.saveComment(listComments);
		    
		    FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO,  generalServices.getBundle().getString("msgkayitbsrl"), null);
		     FacesContext.getCurrentInstance().addMessage(null, msg);
		}catch(Exception ex){
			 FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR,  generalServices.getBundle().getString("msghata")+ex.toString(), null);
			  FacesContext.getCurrentInstance().addMessage(null, msg);
			
		}
		reset();
		
	}
	
	//datatable gelen defect in commnets işlemleri 
	
	public void getCommentsList(){
		listCommentsDefect=defectServices.getCommentList(selectdfc);
		
	}
	
	public void  removeCommmentOnDefect(Comments c){
		defectServices.removeCommentOnDefect(selectdfc, c);
	}
	
	
    public void onRowSelect(SelectEvent event) {
        isDefectComments=true ;
    	selectdfc=(Defect)event.getObject();
    	getCommentsList();
    	
    }
	
	
	
	 //getter setter 

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public GeneralServices getGeneralServices() {
		return generalServices;
	}

	public void setGeneralServices(GeneralServices generalServices) {
		this.generalServices = generalServices;
	}

	

	public Defect getSelectdfc() {
		return selectdfc;
	}

	public void setSelectdfc(Defect selectdfc) {
		this.selectdfc = selectdfc;
	}

	public List<Defect> getListDefect() {
		return listDefect;
	}

	public void setListDefect(List<Defect> listDefect) {
		this.listDefect = listDefect;
	}

	public List<Defect> getFilterDefect() {
		return filterDefect;
	}

	public void setFilterDefect(List<Defect> filterDefect) {
		this.filterDefect = filterDefect;
	}

	public Defect getDefect() {
		return defect;
	}

	public void setDefect(Defect defect) {
		this.defect = defect;
	}

	public DefectServices getDefectServices() {
		return defectServices;
	}

	public void setDefectServices(DefectServices defectServices) {
		this.defectServices = defectServices;
	}

	public List<User> getListUser() {
		return listUser;
	}

	public void setListUser(List<User> listUser) {
		this.listUser = listUser;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public List<Comments> getListComments() {
		return listComments;
	}

	public void setListComments(List<Comments> listComments) {
		this.listComments = listComments;
	}

	public List<Comments> getListCommentsDefect() {
		return listCommentsDefect;
	}

	public void setListCommentsDefect(List<Comments> listCommentsDefect) {
		this.listCommentsDefect = listCommentsDefect;
	}

	public boolean isDefectComments() {
		return isDefectComments;
	}

	public void setDefectComments(boolean isDefectComments) {
		this.isDefectComments = isDefectComments;
	}

	public List<String> getListSeverity() {
		return listSeverity;
	}

	public void setListSeverity(List<String> listSeverity) {
		this.listSeverity = listSeverity;
	}

	public List<String> getListStatu() {
		return listStatu;
	}

	public void setListStatu(List<String> listStatu) {
		this.listStatu = listStatu;
	}

	
     
    

}
