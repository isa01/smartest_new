package com.tr.controller;

import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.tr.model.GnlParameter;
import com.tr.service.GeneralServices;
import com.tr.service.UserService;

import lombok.Data;

@ManagedBean(name = "gnlparamController")
@Data
@SessionScoped
public class GnlParameterController implements Serializable {

	/**
	 * Genel parametre kontrol
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(GnlParameterController.class);

	@ManagedProperty("#{userService}")
	private UserService userService;

	@ManagedProperty("#{generalServices}")
	private GeneralServices generalServices;


	transient HttpSession session;

	private GnlParameter gnlparam, selectGnlPrm;
	private List<GnlParameter> listGnlPrm, filterGnlPrm;
	private BigDecimal tempId;
	private String executionJobStatus = "STOPPED";
	

	public GnlParameterController() throws IOException {

		super();
		session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);

		if (session.getAttribute("userId") == null) {
			ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
			context.redirect(context.getRequestContextPath() + "/login.xhtml?faces-redirect=true");
		}

	}

	@PostConstruct
	public void init() {
		listGnlPrm = userService.getParamList();
		reset();
	}

	public void reset() {
		gnlparam = new GnlParameter();
		selectGnlPrm = null;
		tempId = null;
	}

	public void save() {

		try {

			if (gnlparam.getName() != null && gnlparam.getValue() != null && !gnlparam.getName().trim().equals("")
					&& !gnlparam.getValue().trim().equals("")) {
				gnlparam.setId(null);
				gnlparam.setCreateBy((int) session.getAttribute("userId"));
				gnlparam.setCreatedDate(new java.util.Date());

				userService.saveParam(tempId, gnlparam);
				FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO,
						generalServices.getBundle().getString("msgkayitbsrl"), null);
				FacesContext.getCurrentInstance().addMessage(null, msg);
			} else {
				FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ad ve Değer Zorunlu Alan", null);
				FacesContext.getCurrentInstance().addMessage(null, msg);
			}

		} catch (Exception ex) {
			logger.error(ex.toString());
			FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR,
					generalServices.getBundle().getString("msghata") + ex.toString(), null);
			FacesContext.getCurrentInstance().addMessage(null, msg);
		}
		reset();

	}

//	public void startStopExecutionJob() {
// 
//		int executeTestJobInterval = Integer.parseInt(userService.getParamValue("executeTestJobInterval"));
//		Timer time;
//		if (executionJobStatus.equals("STOPPED")) {
//			time = new Timer(); // Instantiate Timer Object
//			time.schedule(executeJob, 0, executeTestJobInterval); // Create Repetitively task for every 1 secs
//			executionJobStatus = "RUNNING";
//		} else {
//			executeJob.cancel();
//			executionJobStatus = "STOPPED";
//		}
//	}

	public void delete() {

		try {
			userService.deleteParam(gnlparam.getId());
			FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO,
					generalServices.getBundle().getString("msgsilmebsrl"), null);
			FacesContext.getCurrentInstance().addMessage(null, msg);

		} catch (Exception ex) {
			logger.error(ex.toString());
			FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR,
					generalServices.getBundle().getString("msghata") + ex.toString(), null);
			FacesContext.getCurrentInstance().addMessage(null, msg);
		}
		reset();
	}

	public void getParamList() {
		listGnlPrm = userService.getParamList();
	}

	// getter setter

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public GnlParameter getGnlparam() {
		return gnlparam;
	}

	public void setGnlparam(GnlParameter gnlparam) {
		this.gnlparam = gnlparam;
	}

	public List<GnlParameter> getListGnlPrm() {
		return listGnlPrm;
	}

	public void setListGnlPrm(List<GnlParameter> listGnlPrm) {
		this.listGnlPrm = listGnlPrm;
	}

	public List<GnlParameter> getFilterGnlPrm() {
		return filterGnlPrm;
	}

	public void setFilterGnlPrm(List<GnlParameter> filterGnlPrm) {
		this.filterGnlPrm = filterGnlPrm;
	}

	public GnlParameter getSelectGnlPrm() {
		return selectGnlPrm;
	}

	public void setSelectGnlPrm(GnlParameter selectGnlPrm) {
		this.selectGnlPrm = selectGnlPrm;
		if (selectGnlPrm != null) {
			gnlparam = selectGnlPrm;
			tempId = selectGnlPrm.getId();

		}
	}

	public GeneralServices getGeneralServices() {
		return generalServices;
	}

	public void setGeneralServices(GeneralServices generalServices) {
		this.generalServices = generalServices;
	}
 
	public String getExecutionJobStatus() {
		return executionJobStatus;
	}

	public void setExecutionJobStatus(String executionJobStatus) {
		this.executionJobStatus = executionJobStatus;
	}

}
