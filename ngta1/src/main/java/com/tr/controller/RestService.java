package com.tr.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.URLDecoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import javax.annotation.Resource;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.persistence.Query;
import javax.servlet.http.HttpSession;
import javax.tools.JavaCompiler;
import javax.tools.JavaFileObject;
import javax.tools.StandardJavaFileManager;
import javax.tools.ToolProvider;
import javax.tools.JavaCompiler.CompilationTask;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.xml.bind.DatatypeConverter;

import org.apache.commons.io.FileUtils;
import org.hibernate.LockOptions;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Example;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.primefaces.model.DefaultTreeNode;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.base.Preconditions;
import com.tr.dao.PreferencesDao;
import com.tr.dao.TestCaseDao;
import com.tr.model.AppParamDetail;
import com.tr.model.AppParameter;
import com.tr.model.Company;
import com.tr.model.Defect;
import com.tr.model.GnlParameter;
import com.tr.model.Report;
import com.tr.model.Scheduler;
import com.tr.model.TestCase;
import com.tr.model.TestStep;
import com.tr.service.PreferencesService;
import com.tr.service.TestCaseService;
import com.tr.service.TestSuiteService;
import com.tr.util.Smartest;

@Transactional
@Path("/plugin")
public class RestService implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	Session session;
	String sonuc;
	private Smartest smartest;

	transient StringWriter output;
	// @Resource
	// @Autowired
	// private PreferencesService preferencesService;
	//
	// @Resource
	// @Autowired
	// private TestCaseService testCaseService;

	// private TestCaseManageController testCaseController;

	 
	@POST
	@Produces("application/json")
	@Path("/executeTests")
	public Response executeTests() throws UnsupportedEncodingException {
		SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
		session = sessionFactory.openSession();
		session.beginTransaction();

		// smartest = Smartest.getSmartest();
		System.out.println("Job Start Running");
		List<Scheduler> listScheduler = getSchedulersToBeExecute();
		for (Scheduler sch : listScheduler) {
			if (!sch.getStatus().equals("ALLWAYS")) {
				sch.setStatus("RUNNING");
				getSession().saveOrUpdate(sch);
			}
		}
		for (Scheduler sch : listScheduler) {
			TestCase tc = (TestCase) getSession().createQuery("from TestCase where id=:t and  status=:status")
					.setParameter("t", sch.getTestcase_id()).setParameter("status", "Aktif").uniqueResult();
			System.out.println(tc.getName());
			try {
				runTestCase(tc, sch);
			} catch (Exception exp) {
				smartest.setLogText(smartest.getLogText() + "\n Exception:" + smartest.getStackTrace(exp));
			}
			if (!sch.getStatus().equals("ALLWAYS"))
				sch.setStatus("FINISHED");
			getSession().saveOrUpdate(sch);
			//
			// Report r = new Report();
			// r.setErrlog("jenkins working");
			// session.saveOrUpdate(r);
			session.getTransaction().commit();
			session.close();

		}
		ResponseBuilder builder = Response.ok("ok");
		builder.header("Access-Control-Allow-Origin", "*");
		builder.header("Access-Control-Max-Age", "8000");
		builder.header("Access-Control-Allow-Methods", "*");
		builder.header("Access-Control-Allow-Headers",
				"X-Requested-With,Host,User-Agent,Accept,Accept-Language,Accept-Encoding,Accept-Charset,Keep-Alive,Connection,Referer,Origin");
		return builder.build();
	}

	@POST
	@Path("/addRecord")
	@Produces("application/json")
	@Consumes("text/plain;charset=UTF-8")
	public void addRecord(String base64String) throws IOException {
		SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		System.out.println(base64String);
		System.out.println(base64String.length());
		String[] strings = base64String.split(",");
		String extension;
		 
		String str = strings[0];
		 String parts[] = str.split("$");
		String page = parts[0];
		String desc = parts[2];
		String action = parts[1];
		// convert base64 string to binary data
//		String path =   session.createQuery("from GnlParameter where status=:status and name=:name",GnlParameter.class)
//				.setParameter("status", "Aktif").setParameter("name", "schreenShot").getSingleResult().getDescription();
		String filePath = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
		byte[] data = DatatypeConverter.parseBase64Binary(strings[1]);
		String path ="C:\\Users\\hurka\\git\\smartest_isa\\ngta1\\src\\main\\webapp\\resources\\images"+"\\"+filePath + ".png";
		File file = new File(path);
		
		try (OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(file))) {
			outputStream.write(data);
			FileUtils.copyFile(file, outputStream);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		session.getTransaction().commit();
		session.close();
	}

	private List<Scheduler> getSchedulersToBeExecute() {
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Date date = new Date();
		List<Scheduler> ls = (List<Scheduler>) getSession()
				.createQuery("from Scheduler where status=:status and run_date <=:runDate")
				.setParameter("status", "WAITING").setParameter("runDate", dateFormat.format(date)).getResultList();

		List<Scheduler> ls2 = (List<Scheduler>) getSession().createQuery("from Scheduler where status=:status")
				.setParameter("status", "ALLWAYS").getResultList();
		ls.addAll(ls2);
		return ls;
	}

	private void runTestCase(TestCase tc, Scheduler sch) {
		try {
			createTestCaseFile(tc, sch);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		// session = (HttpSession)
		// FacesContext.getCurrentInstance().getExternalContext().getSession(true);
		// Smartest smarTest = Smartest.getSmartest();
		System.out.println(tc.getName() + " TestCase Preparing");
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		JUnitCore junit = new JUnitCore();
		// Class<?> clazz = null;
		Class clazz = null;
		Result result = null;
		Long xxx = getNextKey();
		int runId = Integer.parseInt(String.valueOf(xxx));
		Report rep = new Report();
		rep.setRunId(runId);
		rep.setEndDate(dateFormat.format(date));
		rep.setTestCase(tc);
		// rep.setCreate_by((String) session.getAttribute("username"));
		smartest.setLogText("");
		try {
			// File f = new
			// File(userService.getParamValueListByKey("classes.folder").get(0).getValue());
			// File f = new
			// File(this.getClass().getClassLoader().getResource("").getPath());
			// URL[] cp = { f.toURI().toURL() };
			// URLClassLoader urlcl = new URLClassLoader(cp);
			// clazz = urlcl.loadClass("com.tr.testCaseFiles." + tc.getName());
			clazz = Class.forName("com.tr.testCaseFiles." + tc.getName());
			// urlcl.close();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			System.out.println("\n" + tc.getName() + " TestCase Starting");
			result = junit.run(clazz);
		} catch (Exception exp) {
			rep.setState("failed");
			{
				rep.setSs_url(smartest.getFilePath());
				rep.setErrlog(smartest.getLogText());
				Defect defect = new Defect();
				defect.setFailed_test_case(tc.getId());
				// defect.setCreated_by((String) session.getAttribute("username"));
				defect.setCreated_date(new Date());
				defect.setDescription("Jenkins: " + smartest.getLogText());
				defect.setFailed_test_suite(0);
				defect.setRunId(runId);
				getSession().save(defect);
			}
		}
		if (result.wasSuccessful()) {
			rep.setState("passed");
			rep.setErrlog(smartest.getLogText());
			rep.setSs_url(smartest.getFilePath());
		} else {
			rep.setState("failed");
			rep.setSs_url(smartest.getFilePath());
			rep.setErrlog(smartest.getLogText());
			Defect defect = new Defect();
			defect.setFailed_test_case(tc.getId());
			// defect.setCreated_by((String) session.getAttribute("username"));
			defect.setCreated_date(new Date());
			defect.setDescription("Jenkins: " + smartest.getLogText());
			defect.setFailed_test_suite(0);
			defect.setRunId(runId);
			getSession().save(defect);
		}
		getSession().save(rep);
	}

	@POST
	@Produces("application/json")
	@Path("/getElement") // @PathParam("param")
							// encodeURIComponent('text='+allAttr+'_Smartest_'+desc+'_Smartest_'+actnn+'_Smartest_'+href+'_Smartest_'+testcasename)
	public Response getElement(String param) throws UnsupportedEncodingException {

		System.out.println("Gelen Parametre :" + param);
		SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		// session=(HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(true);
		// session=null ;
		// if(session==null)
		// {
		// sonuc="Kullanıcı oturumu kapalı" ;
		// }else {
		// sonuc = saveElement(param);
		try {
			AppParamDetail appdet = null;
			String tagName = null;
			System.out.println("");
			// selectedNode.setType("folder");
			// selectedNode.setData("hb");
			TestCase tc = null;
			// Transaction tx1 = session.beginTransaction();
			List<TestStep> testSteps = new ArrayList<TestStep>();

			String parts[] = param.split("_Smartest_");
			AppParameter ap = new AppParameter();
			ap.setDescription(parts[1]);
			String action = parts[2];
			String parts_url[] = parts[3].split("/");
			ap.setPage(parts_url[parts_url.length - 1]);

			if (!parts[4].equals("T")) {
				String testCaseName = parts[4];
				testCaseName = testCaseName.substring(1, testCaseName.length());
				TestStep ts = new TestStep();
				ts.setAppParameter(ap);
				ts.setStatus("Aktif");
				ts.setDescription(ap.getDescription());
				ts.setType("TestStep");
				ts.setPage(ap.getPage());
				ts.setAction_type(action);
				tc = (TestCase) session.createQuery("from TestCase where name=:t and  status=:status")
						.setParameter("t", testCaseName).setParameter("status", "Aktif").uniqueResult();
				if (tc == null) {
					tc = new TestCase();
					tc.setCompany_id(2);
					tc.setTestSteps(testSteps);
					tc.setName(testCaseName);
					tc.setStatus("Aktif");
					tc.setFolderUrl("/hb/");
					// tc.setCompany(getCompany());
					tc.setOpen_page(parts[3]);
					tc.setDescription(ts.getDescription());
				} else {
					tc = (TestCase) session.createQuery("from TestCase where name=:t and  status=:status")
							.setParameter("t", testCaseName).setParameter("status", "Aktif").uniqueResult();
					// testSteps = testCaseService.getTestStepById(tc.getId());
					testSteps = session.createQuery("from TestStep where status=:status and testcase_id=:tCaseId")
							.setParameter("status", "Aktif").setParameter("tCaseId", tc.getId()).getResultList();
					tc.setTestSteps(testSteps);
				}
				// ts.setTestCase(tc);
				testSteps.add(ts);
				for (TestStep t : testSteps)
					session.saveOrUpdate(t);
			}

			// testCaseController.setAutoTestCaseCreateMode(true);
			// testCaseController.setTestCase(tc);
			// testCaseController.setSelectedNode(selectedNode);
			// testCaseController.saveOrUpdateTestCaseForPlugin();
			// testCaseController.setAutoTestCaseCreateMode(false);

			if (parts[0].contains("_ngta_")) {
				String attrs[] = parts[0].split("_ngta_");
				String textContent[] = attrs[0].split("=");
				if (textContent.length == 4 && !action.equals("select")) {
					appdet = new AppParamDetail();
					ap.getAppParamDetailList().add(appdet);
					appdet.setAction_type(action);
					appdet.setAttr_name(textContent[2]);
					appdet.setAttr_value(textContent[3].trim());
					appdet.setTag_name(textContent[1]);
					appdet.setStatus("Aktif");
				} else {
					for (int i = 0; i < attrs.length; i++) {
						String[] key_value = attrs[i].split("=");
						if (key_value.length == 3 && !key_value[1].equalsIgnoreCase("type")
								&& !key_value[1].equalsIgnoreCase("maxlength")
								&& !key_value[1].equalsIgnoreCase("autocomplete")
								&& !key_value[1].equalsIgnoreCase("for")) {
							tagName = key_value[0];
							appdet = new AppParamDetail();
							ap.getAppParamDetailList().add(appdet);
							appdet.setAction_type(action);
							appdet.setAttr_name(key_value[1]);
							appdet.setAttr_value(key_value[2].trim());
							appdet.setTag_name(tagName);
							appdet.setStatus("Aktif");
						}
					}
				}
				if (parts.length > 5) {
					String prevSibling = parts[5];
					appdet = new AppParamDetail();
					ap.getAppParamDetailList().add(appdet);
					appdet.setAction_type(action);
					appdet.setAttr_name("prev_text");
					appdet.setAttr_value(prevSibling);
					appdet.setTag_name(tagName);
					appdet.setStatus("Aktif");
				}
				// preferencesService.saveAppParamDetailList(appParamDetailList);
				ap.setCompany_id(2);
				session.saveOrUpdate(ap);
				session.getTransaction().commit();
				session.close();
			}
			sonuc = "OK";
		} catch (Exception exp) {
			exp.printStackTrace();
			sonuc = "NOK";
		}

		// }


		ResponseBuilder builder = Response.ok(sonuc);
		builder.header("Access-Control-Allow-Origin", "*");
		builder.header("Access-Control-Max-Age", "8000");
		builder.header("Access-Control-Allow-Methods", "*");
		builder.header("Access-Control-Allow-Headers",
				"X-Requested-With,Host,User-Agent,Accept,Accept-Language,Accept-Encoding,Accept-Charset,Keep-Alive,Connection,Referer,Origin");
		return builder.build();

	}

	private String getParamValue(String key) {
		String value = null;
		List<GnlParameter> gnlList = getSession().createQuery("from GnlParameter where status=:status")
				.setParameter("status", "Aktif").list();
		for (GnlParameter g : gnlList) {
			if (g.getName().equals(key)) {
				value = g.getValue();
			}
		}

		return value;
	}

	public int createTestCaseFile(TestCase tc, Scheduler sch) throws IOException {
		String environment = "Server";
		String browser = "Chrome";
		String schedulerString = "";
		if (sch != null) {
			environment = sch.getEnvironment();
			browser = sch.getBrowser();
			schedulerString = "\n smartest.setSchId(" + sch.getId() + ");";
		}
		int i = 0;
		OutputStreamWriter out = null;
		String folder = tc.getFolderUrl().replace("/", "\\");

		File f = new File(getParamValue("testCaseFolder") + tc.getFolderUrl() + tc.getName() + ".java");
		if (!f.exists()) {
			f.mkdirs();
		}
		f = new File(getParamValue("testCaseFolder") + tc.getFolderUrl() + tc.getName() + ".java");
		String packageName = "com.tr.testCaseFiles" + tc.getFolderUrl();
		packageName = packageName.replace('/', '.');
		packageName = "package " + packageName.substring(0, packageName.length() - 1);
		f.createNewFile();
		FileOutputStream fop = null;
		try {
			fop = new FileOutputStream(f);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			out = new OutputStreamWriter(fop, "utf8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (tc.getPlatform().equals("ANDROID")) {

			String android_import = getParamValue("android_imports");
			String str_class_android = "public class " + tc.getName()
					+ " { \n private Smartest smartest = null; \n private String env=\"" + environment + "\";\n"
					+ getParamValue("android_configure") + "\n";

			out.write(android_import + "\n");
			out.write(str_class_android + "\n");
			out.write("@Test\n public void " + tc.getName() + "Test(){\n try{" + "\n");
			out.write(" if(env==\"Local\"){\r\n"
					+ "			final DesiredCapabilities caps = DesiredCapabilities.chrome();\r\n"
					+ "			final ChromeOptions chromeOptions = new ChromeOptions();\r\n"
					+ "            chromeOptions.addArguments(\"--disable-gpu\");\r\n"
					+ "            chromeOptions.addArguments(\"--no-sandbox\");\r\n"
					+ "			caps.setCapability(ChromeOptions.CAPABILITY, chromeOptions); \r\n"
					+ "			System.setProperty(\"webdriver.chrome.driver\", \"/usr/bin/chromedriver\");\r\n"
					+ "			driver = new ChromeDriver(caps);\r\n" + " }\r\n"
					+ " else{	 System.setProperty(\"webdriver.chrome.driver\", " + getParamValue("chrome_yol")
					+ ");\r\n" + "driver = new ChromeDriver();\r\n" + " }");
			out.write("smartest = Smartest.getSmartest();\n");
			out.write(schedulerString + "\n");

			convertTestStepToScript(out, tc, tc.getPlatform());

			out.write(
					"\n}catch (Exception e) {\r\n smartest.closePage(driver); fail(smartest.getLogText());\r\n }  catch (AssertionError exp) {\r\n smartest.closePage(driver);\r\n fail(smartest.getLogText());\r\n	} smartest.closePage(driver); ");
			out.write("}\n}\n\n");

			out.close();
			int isSuccess = addPath(getParamValue("testCaseFolder") + tc.getFolderUrl() + tc.getName() + ".java");
			if (isSuccess == -1) {
				i = -1;
				// f.delete();
				FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "HATA", "Test Case Yaratılamadı");
				FacesContext.getCurrentInstance().addMessage(null, msg);
			} else {
				i = 1;
				// saveTestCase(tc);
				FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "TestCase Yaratıldı", tc.getName());
				FacesContext.getCurrentInstance().addMessage(null, msg);
			}
			return i;

		} else if (tc.getPlatform().equals("WEB")) {

			String testCaseType = tc.getTypeName();
			out.write(packageName + ";\n");
			out.write(getParamValue("str_imports") + "\n");
			out.write("public class " + tc.getName() + "{\n");
			out.write("private WebDriver driver = null;  \n private Smartest smartest;  \n private String env=\""
					+ environment + "\";\n WebElement webElement;\n " + "@" + testCaseType + "\n public void "
					+ tc.getName() + "Test(){\n " + "try{"
					+ "\n System.out.println(\"....Started....\"); \n smartest = Smartest.getSmartest();"
					+ "System.setProperty(\"webdriver.chrome.driver\", \"" + getParamValue("chrome_yol") + "\");");
			out.write(" if(env==\"Server\"){\r\n" + "final DesiredCapabilities caps = DesiredCapabilities.chrome();\r\n"
					+ "final ChromeOptions chromeOptions = new ChromeOptions();\r\n"
					+ "chromeOptions.addArguments(\"--disable-gpu\");\r\n"
					+ "chromeOptions.addArguments(\"--no-sandbox\");\r\n"
					+ "caps.setCapability(ChromeOptions.CAPABILITY, chromeOptions); \r\n"
					+ "driver = new ChromeDriver(caps);\r\n" + " }\n" + "else{ \n driver = new ChromeDriver();\n}\n"
					+ "driver.manage().timeouts().implicitlyWait(8, TimeUnit.SECONDS);\n driver.manage().window().maximize(); \n"
					+ "driver.get(\"" + tc.getOpen_page() + "\");\n" + " smartest.setLogText(smartest.getLogText()+\""
					+ tc.getOpen_page() + " Sayfası Açıldı....\");\n");
			out.write(schedulerString + "\n");

			convertTestStepToScript(out, tc, tc.getPlatform());

			out.write("\n}catch (Exception e) {\r\n " + "e.printStackTrace(); smartest.closePage(driver);\n "
					+ "smartest.setLogText(smartest.getLogText() + smartest.getStackTrace(e));\n " + "fail();\r\n "
					+ "}  " + "catch (AssertionError exp) {\r\n" + "smartest.closePage(driver);\r\n "
					+ "smartest.setLogText(smartest.getLogText() + \"\\n Assertion Hatası :\" + exp.getMessage());"
					+ "fail();\r\n	} \n smartest.closePage(driver); \n " + "}\n");
			out.write("	public Smartest getSmartest() {\r\n" + "		return smartest;\r\n" + "	}\r\n" + "\r\n"
					+ "	public void setSmartest(Smartest smartest) {\r\n" + "		this.smartest = smartest;\r\n"
					+ "	}}");

			out.close();
			int isSuccess;
			if (getParamValue("testCaseFolder").contains("/")) {
				System.out.println(
						"linux: " + getParamValue("testCaseFolder") + tc.getFolderUrl() + tc.getName() + ".java");
				isSuccess = addPath(getParamValue("testCaseFolder") + tc.getFolderUrl() + tc.getName() + ".java");

			} else {
				System.out.println("windows: " + getParamValue("testCaseFolder") + folder + tc.getName() + ".java");
				isSuccess = addPath(getParamValue("testCaseFolder") + folder + tc.getName() + ".java");
			}
			if (isSuccess == -1) {
				i = -1;
				// f.delete();
				FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "HATA",
						"Failed to compile: " + tc.getName() + ":" + output.toString());
				FacesContext.getCurrentInstance().addMessage(null, msg);
			} else if (sch == null) {
				i = 1;
				// saveTestCase(tc);
				FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "TestCase Yaratıldı", tc.getName());
				FacesContext.getCurrentInstance().addMessage(null, msg);
			} else
				i = 1;
			return i;
		}
		return -1;
	}

	public void convertTestStepToScript(OutputStreamWriter out, TestCase tc, String type) throws IOException {
		TestCase testcase;
		tc.getTestSteps().sort(Comparator.comparing(TestStep::getOrderNum));
		if (tc.getTestSteps() != null) {
			if (tc.getTestSteps().size() > 0) {
				for (TestStep ts : tc.getTestSteps()) {
					if (ts.getType().equals("TestCase")) {
						testcase = getTestCaseById(ts.getTemplateId());
						testcase.setTestSteps(getTestStepById(testcase.getId()));
						convertTestStepToScript(out, testcase, type);
					} else if (ts.getType().equals("TestStep")) {
						String value = "smartest.executeTestStep(driver," + ts.getId() + ",\"" + type + "\");";
						out.write(value + "//" + (getSession()
								.createQuery("from AppParameter where id=:id and status=:status", AppParameter.class)
								.setParameter("id", ts.getAppParameter().getId()).setParameter("status", "Aktif")
								.getResultList().get(0)).getDescription());
					} else if (ts.getType().equals("Java")) {
						out.write(getSession()
								.createQuery("from AppParamDetail where appParameter_id=:id and  status=:status",
										AppParamDetail.class)
								.setParameter("id", ts.getAppParameter().getId()).setParameter("status", "Aktif")
								.getResultList().get(0).getAttr_value() + "\n");
					}
				}
			}
		}
	}

	private List<TestStep> getTestStepById(int tCaseId) {
		List<TestStep> ts = getSession().createQuery("from TestStep where status=:status and testcase_id=:tCaseId")
				.setParameter("status", "Aktif").setParameter("tCaseId", tCaseId).getResultList();
		if (ts != null)
			return ts;
		return new ArrayList<TestStep>();
	}

	public TestCase getTestCaseById(int id) {

		TestCase tCase = (TestCase) getSession().createQuery("from TestCase where id=:t and  status=:status")
				.setParameter("t", id).setParameter("status", "Aktif").uniqueResult();
		if (tCase != null)
			return tCase;
		return tCase;
	}

	public int addPath(String s) {

		int isSuccess = 0;
		try {
			String javaHome = getParamValue("java.home");
			System.setProperty("java.home", javaHome);
			output = new StringWriter();
			boolean result;

			JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
			StandardJavaFileManager fileManager = compiler.getStandardFileManager(null, null, null);
			List<File> sourceFileList = new ArrayList<File>();
			sourceFileList.add(new File(s));
			Iterable<? extends JavaFileObject> compilationUnits = fileManager
					.getJavaFileObjectsFromFiles(sourceFileList);
			List<String> optionList = new ArrayList<String>();
			String path = this.getClass().getClassLoader().getResource("").getPath();
			System.out.println("classesFolder:" + path);
			optionList.addAll(Arrays.asList("-classpath", path));
			optionList.addAll(Arrays.asList("-d", path));
			CompilationTask task = compiler.getTask(output, fileManager, null, optionList, null, compilationUnits);
			result = task.call();
			System.out.println("Compilation result : " + output.toString());
			if (result) {
				isSuccess = 1;
				System.out.println("Success");
			} else {
				System.out.println("Fail");
				isSuccess = -1;
			}
			try {
				fileManager.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} catch (Exception ex) {
			System.out.println("Hata :::" + ex.toString());
		}
		return isSuccess;

	}

	public Long getNextKey() {
		Long key = null;
		try {
			Query query = getSession().createSQLQuery("select distinct next_val from hibernate_sequence ");
			key = ((BigInteger) ((org.hibernate.Query) query).uniqueResult()).longValue();
		} catch (Exception ex) {
			System.out.println(ex.toString());
		}

		return key;
	}

	public Session getSession() {
		return session;
	}

	public void setSession(Session session) {
		this.session = session;
	}

}
