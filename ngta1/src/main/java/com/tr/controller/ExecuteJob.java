package com.tr.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

import org.junit.runner.JUnitCore;
import com.tr.model.Scheduler;
import com.tr.model.TestCase;
import com.tr.service.TestCaseService;
import com.tr.service.TestSuiteService;
import com.tr.service.UserService;
import lombok.Data;

@ManagedBean(name = "ExecuteJob")
@Data
@SessionScoped
public class ExecuteJob implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// transient HttpSession session;

	// private SessionFactory sessionFactory = new
	// Configuration().configure().buildSessionFactory();

	@ManagedProperty("#{userService}")
	private UserService userService;

	@ManagedProperty("#{testSuiteService}")
	private TestSuiteService testSuiteService;

	@ManagedProperty("#{testCaseService}")
	private TestCaseService testCaseService;

	@ManagedProperty("#{testExecutionController}")
	private TestExecutionController testExecution;

	private Scheduler runningScheduler;
	private List<Scheduler> runningSchedulers;

	private String logs; 

	private Class<?> clazz;

 

	public void runAllAvailableTestCase() {
		runningSchedulers = new ArrayList<Scheduler>();
		System.out.println("Job Start Running");
		List<Scheduler> listScheduler = testCaseService.getSchedulersToBeExecute("Regular"); 
		for (Scheduler sch : listScheduler) {
			sch.setStatus("RUNNING");
			testCaseService.saveScheduler(sch);
		}
		for (Scheduler sch : listScheduler) {
			runningScheduler = sch;
			runningSchedulers.add(runningScheduler);
			TestCase tc = testCaseService.getTestCaseByIdExcludePGC(runningScheduler.getTestcase_id());
			try {
				runTestCase(tc, runningScheduler);
			} catch (Exception exp) {
			} 
		} 
	}

	public void pollRunningstatus() {
		System.out.println("refreshhinnggggg"); 
	}

	private void runTestCase(TestCase tc, Scheduler sch) {
//		try {
//			tc.setOpen_page(sch.getStartURL());
//			testCaseService.createTestCaseFile(tc, sch,username);
//		} catch (IOException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
		System.out.println(tc.getName() + " TestCase Preparing");

		JUnitCore junit = new JUnitCore();

		try {
			String packageName = "com.tr.testCaseFiles" + tc.getFolderUrl();
			packageName = packageName.replace('/', '.');
			clazz = Class.forName(packageName + tc.getName());
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			return;
		}
		try {
			System.out.println("\n" + tc.getName() + " TestCase Starting");
			new Thread(() -> {
				junit.run(clazz);
			}).start();

			// testCaseService.deleteClassFile(tc.getFolderUrl() + tc.getName());
		} catch (Exception exp) {
			exp.printStackTrace();
		}
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public TestSuiteService getTestSuiteService() {
		return testSuiteService;
	}

	public void setTestSuiteService(TestSuiteService testSuiteService) {
		this.testSuiteService = testSuiteService;
	}

	public TestCaseService getTestCaseService() {
		return testCaseService;
	}

	public void setTestCaseService(TestCaseService testCaseService) {
		this.testCaseService = testCaseService;
	}

	public void setLogs(String logs) {
		this.logs = logs;
	}

	public TestExecutionController getTestExecution() {
		return testExecution;
	}

	public void setTestExecution(TestExecutionController testExecution) {
		this.testExecution = testExecution;
	}

	public List<Scheduler> getRunningSchedulers() {
		return runningSchedulers;
	}

	public void setRunningSchedulers(List<Scheduler> runningSchedulers) {
		this.runningSchedulers = runningSchedulers;
	}

}
