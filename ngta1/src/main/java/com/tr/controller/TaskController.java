package com.tr.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import org.primefaces.event.SelectEvent;

import com.tr.model.Comments;
import com.tr.model.Task;
import com.tr.model.User;
import com.tr.service.GeneralServices;
import com.tr.service.TaskService;
import com.tr.service.UserService;

import lombok.Data;

@ManagedBean(name = "taskController")
@Data
@SessionScoped
public class TaskController implements  Serializable {
	
	/**
	 * Task listesi ve kontrollerini sağlar  
	 * 
	 */
	 private static final long serialVersionUID = 1L;
	 private static Logger logger =Logger.getLogger(TaskController.class);
	 
	 @ManagedProperty("#{userService}")
 	 private UserService userService;
	 
	 @ManagedProperty("#{generalServices}")
     private GeneralServices generalServices ;
	 
	 @ManagedProperty("#{taskService}")
     private TaskService taskService ;
	 
	 transient HttpSession session ;
     
     //datatable variables
     private List<Task> listTask , filterTask ;
     private Task selectTask ;
     
     // genel variables
     private Task task ;
     private String comments ;
     private List<Comments> listComments ;
     private List<Comments> listCommentsTask ;
     
     private boolean isTaskComments=false;
     
     //gnl param 
     private List<String> listSeverity=new ArrayList<String>();
     private List<String> listStatu=new ArrayList<String>();
     private List<User> listUser=new ArrayList<User>() ;

	public TaskController() throws IOException {
		super();
		 session= (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true); 

		    if (session.getAttribute("userId") == null) {
		            ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
		            context.redirect(context.getRequestContextPath() + "/login.xhtml?faces-redirect=true");
		    }
	}
    
	
	@PostConstruct
	public void init(){
		listUser=userService.getUserList();
		listSeverity=userService.getParamValueList("severity");
		listStatu=userService.getParamValueList("statu");
		reset();
	}
	
	public void reset(){
		getListTasks();
		task=new Task();
		comments=null ;
		selectTask=null;
		listComments=new ArrayList<Comments>();
		listCommentsTask=new ArrayList<Comments>();
		isTaskComments=false;		
	}
	
	public void getListTasks(){
		   listTask=taskService.getListTask();
    }
	
	public void addlistComment(){
		if(comments!=null &&  !comments.trim().equals("") )
		{
			Comments c=new Comments();
			c.setComments(comments);
			listComments.add(c);
			comments=null;
		}
	}
	
    public void removeComment(Comments c){
		
		listComments.remove(c);
		
	 }
    
    
     public void save(){
    	 try{
 			task.setCreated_date(new Date());
 			task.setCreated_by((String) session.getAttribute("username"));
// 			int assignUserId=(int) task.getAssign_user().getId() ;
//			task.setAssign_user(userService.getUser(String.valueOf(assignUserId)));
 		    taskService.saveTask(task);
 		    
 		    for(int i=0 ; i<listComments.size() ; i++ )
 		    {
 		    	listComments.get(i).setTaskId(task.getId());
 		    	listComments.get(i).setUserName(task.getCreated_by());
 		    }
 		   taskService.saveComment(listComments);
 		    
 		    FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO,  generalServices.getBundle().getString("msgkayitbsrl"), null);
 		     FacesContext.getCurrentInstance().addMessage(null, msg);
 		}catch(Exception ex){
 			 FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR,  generalServices.getBundle().getString("msghata")+ex.toString(), null);
 			  FacesContext.getCurrentInstance().addMessage(null, msg);
 			
 		}
 		reset();
     }
	
	//datatable gelen defect in commnets işlemleri 
	
		public void getCommentsList(){
			listCommentsTask=taskService.getCommentList(selectTask);
			
		}
		
		public void  removeCommmentOnTask(Comments c){
			taskService.removeCommentOnTask(selectTask, c);
		}
		
		
	    public void onRowSelect(SelectEvent event) {
	        isTaskComments=true ;
	    	selectTask=(Task)event.getObject();
	    	getCommentsList();
	    	
	    }

    
    //getter setter 
    
	
	


	public List<Task> getFilterTask() {
		return filterTask;
	}


	public void setFilterTask(List<Task> filterTask) {
		this.filterTask = filterTask;
	}


	public Task getSelectTask() {
		return selectTask;
	}


	public void setSelectTask(Task selectTask) {
		this.selectTask = selectTask;
	}


	public Task getTask() {
		return task;
	}


	public void setTask(Task task) {
		this.task = task;
	}


	public String getComments() {
		return comments;
	}


	public void setComments(String comments) {
		this.comments = comments;
	}


	public List<Comments> getListComments() {
		return listComments;
	}


	public void setListComments(List<Comments> listComments) {
		this.listComments = listComments;
	}


	


	public List<Comments> getListCommentsTask() {
		return listCommentsTask;
	}


	public void setListCommentsTask(List<Comments> listCommentsTask) {
		this.listCommentsTask = listCommentsTask;
	}


	public boolean isTaskComments() {
		return isTaskComments;
	}


	public void setTaskComments(boolean isTaskComments) {
		this.isTaskComments = isTaskComments;
	}


	public List<String> getListSeverity() {
		return listSeverity;
	}


	public void setListSeverity(List<String> listSeverity) {
		this.listSeverity = listSeverity;
	}


	public List<String> getListStatu() {
		return listStatu;
	}


	public void setListStatu(List<String> listStatu) {
		this.listStatu = listStatu;
	}


	public List<User> getListUser() {
		return listUser;
	}


	public void setListUser(List<User> listUser) {
		this.listUser = listUser;
	}


	public void setListTask(List<Task> listTask) {
		this.listTask = listTask;
	}


	public UserService getUserService() {
		return userService;
	}


	public void setUserService(UserService userService) {
		this.userService = userService;
	}


	public GeneralServices getGeneralServices() {
		return generalServices;
	}


	public void setGeneralServices(GeneralServices generalServices) {
		this.generalServices = generalServices;
	}


	public TaskService gettaskService() {
		return taskService;
	}


	public void settaskService(TaskService taskService) {
		this.taskService = taskService;
	}


	public static Logger getLogger() {
		return logger;
	}


	public static void setLogger(Logger logger) {
		TaskController.logger = logger;
	}


	public HttpSession getSession() {
		return session;
	}


	public void setSession(HttpSession session) {
		this.session = session;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	public TaskService getTaskService() {
		return taskService;
	}


	public void setTaskService(TaskService taskService) {
		this.taskService = taskService;
	}


	public List<Task> getListTask() {
		return listTask;
	}
     
	
	
     
}
