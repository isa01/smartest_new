package com.tr.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.Locale;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

import com.tr.dao.UserDao;
import com.tr.model.User;
import com.tr.service.GeneralServices;
import com.tr.service.UserService;

import lombok.Data;

@ManagedBean(name = "passchangeController")
@Data
@SessionScoped
public class PasswordChangeController implements Serializable {

	/**
	 * parola değiştirme sayfası 
	 * 
	 */
	 private static final long serialVersionUID = 1L;
	 private static Logger logger =Logger.getLogger(PasswordChangeController.class);
     private String  password, newpassword , newpassword2 ;	
     private User user ;
     
     @ManagedProperty("#{userService}")
 	 private UserService userService;
     transient HttpSession session ;
     
     @ManagedProperty("#{generalServices}")
 	private GeneralServices generalServices ;
     
   

	public PasswordChangeController() throws IOException {
		
		     super();
		    session= (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true); 

		    if (session.getAttribute("userId") == null) {
		            ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
		            context.redirect(context.getRequestContextPath() + "/login.xhtml?faces-redirect=true");
		            FacesContext.getCurrentInstance().getViewRoot().setLocale(new Locale("tr"));
		    }
		    
		    FacesContext.getCurrentInstance().getViewRoot().setLocale(new Locale("tr"));
		
	}

	
	public void updatePassword(){
		  
		  try{
			  user=userService.getUser(String.valueOf(session.getAttribute("userId")));
			  
			  if(user.getPassword().equals(password)){
				  
				  if(newpassword.equals(newpassword2)){
					   int result=userService.updatePassword(user.getId(), newpassword2);
					   FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO,  generalServices.getBundle().getString("msgguncelleme"), null);
				       FacesContext.getCurrentInstance().addMessage(null, msg);	
				  }else{
					  FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR,  generalServices.getBundle().getString("ayniparolahata"), null);
					  FacesContext.getCurrentInstance().addMessage(null, msg);		
						
				  }
			  }else{
				    FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR,  generalServices.getBundle().getString("parolahata"),null);
					FacesContext.getCurrentInstance().addMessage(null, msg);		
					
			  }
			 
			  
		  }catch(Exception ex){
			  logger.error(ex.toString());
		  }
		
		
	}
	
	
	
	
	//getter setter 
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getNewpassword() {
		return newpassword;
	}

	public void setNewpassword(String newpassword) {
		this.newpassword = newpassword;
	}

	public String getNewpassword2() {
		return newpassword2;
	}

	public void setNewpassword2(String newpassword2) {
		this.newpassword2 = newpassword2;
	}


	public UserService getUserService() {
		return userService;
	}


	public void setUserService(UserService userService) {
		this.userService = userService;
	}


	public GeneralServices getGeneralServices() {
		return generalServices;
	}


	public void setGeneralServices(GeneralServices generalServices) {
		this.generalServices = generalServices;
	}



	
	
}
