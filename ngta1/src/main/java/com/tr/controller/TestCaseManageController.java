package com.tr.controller;

import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger; 
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.event.ReorderEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.TransferEvent;
import org.primefaces.event.TreeDragDropEvent;
import org.primefaces.event.UnselectEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.DualListModel;
import org.primefaces.model.TreeNode;

import com.tr.model.AppParamDetail;
import com.tr.model.AppParameter;
import com.tr.model.Folder;
import com.tr.model.PropertyGroup;
import com.tr.model.PropertyGroupCatalog;
import com.tr.model.Request;
import com.tr.model.ScheduleStep;
import com.tr.model.StepValidation;
import com.tr.model.TestCase;
import com.tr.model.TestCaseProperty;
import com.tr.model.TestCaseVariable;
import com.tr.model.TestStep;
import com.tr.model.TestSuite;
import com.tr.model.UserRoles;
import com.tr.service.GeneralServices;
import com.tr.service.PreferencesService;
import com.tr.service.TestCaseService;
import com.tr.service.TestSuiteService;

import lombok.Data;

/*
 * Test Case durumları yönetim sayfası 
 * 
 * 
 */

@ManagedBean(name = "testCaseController")
@Data
@SessionScoped
public class TestCaseManageController implements Serializable {

	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(TestCaseManageController.class);

	transient HttpSession session;

	@ManagedProperty("#{preferencesService}")
	private PreferencesService preferencesService;

	@ManagedProperty("#{generalServices}")
	private GeneralServices generalServices;

	@ManagedProperty("#{testCaseService}")
	private TestCaseService testCaseService;

	@ManagedProperty("#{testSuiteService}")
	private TestSuiteService testSuiteService;

	@ManagedProperty("#{testStepPageController}")
	private TestStepPageController testStepPageController;

	// other variables

	private TreeNode root;
	private DefaultTreeNode selectedNode;
	private String folderName, newFolderName;
	private int userRoles;
	Map<String, DefaultTreeNode> rootNodes = null;
	// Map<String,String> serviceVariableMap = null;
	List<TestCaseVariable> testCaseVariables = new ArrayList<TestCaseVariable>();
	private List<TestCase> testCaseList1;

	private boolean testCaseSelect = false;
	private Folder selectFolder;
	private TestCase testCase;
	private TestCase selectTestCase;
	private TestCase tempTC;
	private List<String> tcTypeList;
	private List<TestStep> testStepList, filterTestStep;
	private TestStep tStep;
	private AppParameter selectedListWebElement;
	private AppParamDetail selectedAppDet;
	private AppParameter selectedAppParam;
	private boolean tempTcEditable;
	// private String java_code = "";
	String tempurl; // old url for drag folder

	// temp test case variables
	private Set<StepValidation> selectedValidationList;
	private List<TestCase> tempTestCaseList;
	private TestCase tempSelectTestCase;
	private List<TestCase> tempTCFilter;

	private List<UserRoles> listRoles; // folder role atamak için

	// test suite variables
	private TestSuite testSuite;
	private boolean testSuiteSelect = false;
	private List<TestSuite> testSuiteList;
	private DualListModel<TestCase> dualCaseList;
	List<TestCase> TargetCase;
	List<TestCase> SourceCase;
	private List<TestStep> testStepsOfTestCase;
	private TestCase testCaseView;
	private boolean autoTestCaseCreateMode = false;
	private TestCaseProperty testCaseProperty;
	private TestCaseProperty selectedProperty;
	private Set<TestCaseProperty> testCasePropertyList;
	private PropertyGroup propertyGroup;
	private PropertyGroupCatalog propertyGroupCatalog;
	private List<PropertyGroup> propertyGroupList;
	private PropertyGroup selectedPropertyGroup;
	private Set<TestCaseProperty> propertyListForOutput;
	private Object linkedTestCaseString;
	private String linkedPropertyString;
	private Request selectedRequest;
	private List<Request> requestList;
	private String stepPage;
	private String username;

	public TestCaseManageController() throws IOException {
		super();

		session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
		username = (String) session.getAttribute("username");
		// if (session.getAttribute("userId") == null) {
		// ExternalContext context =
		// FacesContext.getCurrentInstance().getExternalContext();
		// context.redirect(context.getRequestContextPath() +
		// "/login.xhtml?faces-redirect=true");
		// FacesContext.getCurrentInstance().getViewRoot().setLocale(new Locale("tr"));
		// }
	}

	public void init() {
		logger.info("Post Const...");
		setTcTypeList(testCaseService.getTcTypeList());
		root = new DefaultTreeNode("Root", null);
		rootNodes = new HashMap<String, DefaultTreeNode>();
		root.setExpanded(true);
		System.out.println("TestCaseControllerrrrrrrrrr");
		testStepList = new ArrayList<TestStep>();
		tStep = new TestStep();
		testStepList.add(tStep);
		tempTestCaseList = testCaseService.getAllTemplateCase();
		listRoles = preferencesService.getUserRoles();

		testSuite = new TestSuite();
		SourceCase = testCaseService.getAllTestCase();
		TargetCase = new ArrayList<TestCase>();
		dualCaseList = new DualListModel<TestCase>(SourceCase, TargetCase);

		testSuite = null;
		testCaseSelect = false;
		testSuiteSelect = false;
		testCase = new TestCase();
		testCaseView = new TestCase();
		testStepPageController.onload();
		selectedAppDet = new AppParamDetail();
		selectedAppParam = new AppParameter();
		propertyGroupCatalog = new PropertyGroupCatalog();
		propertyListForOutput = new HashSet<TestCaseProperty>();
		requestList = testStepPageController.getAllRequests();

		FacesContext fc = FacesContext.getCurrentInstance();
		Map<String, String> params = fc.getExternalContext().getRequestParameterMap();
		if (params.get("testcase_id") != null) {
			testCaseSelect = true;
			testCase = testCaseService.getTestCaseById(Integer.parseInt(params.get("testcase_id")));
			testCase.setTestSteps(testCaseService.getTestStepById(testCase.getId()));
			// params.put("testcase_id", null);
		}
		// propertyGroupList = testCase.getPropertyGroupList();
		// if (propertyGroupList == null)
		// propertyGroupList = new ArrayList<PropertyGroup>();
		// testCase.setPropertyGroupList(propertyGroupList);
		// testCase.getPropertyGroupList().add(propertyGroup);

		// addTestCasePropertyToSelectedGroup();
		// testCasePropertyList = new ArrayList<TestCaseProperty>();
		// testCaseProperty = new TestCaseProperty();
		// testCasePropertyList.add(testCaseProperty);
		getTestCaseListe();

	}

	public void reorder(ReorderEvent event) {
		for (int i = 0; i < testCase.getTestSteps().size(); i++)
			testCase.getTestSteps().get(i).setOrderNum(i);
	}

	public void addTestCasePropertyToSelectedGroup() {
		testCaseProperty = new TestCaseProperty();
		propertyGroupCatalog.getTestCasePropertyList().add(testCaseProperty);
	}

	// public void bringPropertiesOfGroup(SelectEvent event) {
	// selectedPropertyGroup = (PropertyGroup) event.getObject();
	// testCasePropertyList = selectedPropertyGroup.getTestCasePropertyList();
	// }

	public void onRowUnselectPG() {
		testCasePropertyList = new HashSet<TestCaseProperty>();
	}

	public void initializePropertyGroup() {
		propertyGroupCatalog = testCase.getPropertyGroupCatalog();
		if (propertyGroupCatalog == null) {
			propertyGroupCatalog = new PropertyGroupCatalog();
		}
		if (propertyGroupCatalog.getTestCasePropertyList() == null) {
			propertyGroupCatalog.setTestCasePropertyList(new HashSet<TestCaseProperty>());
			propertyGroupCatalog.getTestCasePropertyList().add(new TestCaseProperty());
		}
		testCase.setPropertyGroupCatalog(propertyGroupCatalog);

	}

	// public void useOutputPropertyForInput(int i) {
	// testCaseService.get
	// }
	// tree yeniden listeler ve ekler
	public void addFoldersToTree() {
		//BigDecimal uu = (BigDecimal) session.getAttribute("userauth");
		List<Folder> lFolder = preferencesService.getFolderList("userauth");
		String parent = null;

		for (Folder f : lFolder) {

			String[] parts = f.getFolder_url().split("/");
			if (parts.length > 2) {
				parent = parts[parts.length - 2];
				rootNodes.put(f.getName(), new DefaultTreeNode("folder", f.getName(), rootNodes.get(parent)));
			} else
				rootNodes.put(f.getName(), new DefaultTreeNode("folder", f.getName(), root));
		}

	}

	private boolean isCaseSuiteAuth(String url) {

		boolean b = true;
		BigDecimal uu = (BigDecimal) session.getAttribute("userauth");
		List<Folder> lFolder = preferencesService.getFolderList(uu.toString());

		for (Folder f : lFolder) {
			if (f.getFolder_url().equals(url)) {
				b = false;
				break;
			}
		}

		return b;
	}

	// ----------------test case işlemleri----------------------

	public void saveOrUpdateTestCase() {

		if ((validateTestCaseName(testCase.getName()) && testCase.getName() != null
				&& !testCase.getName().trim().equals("")) || autoTestCaseCreateMode == true) {

			if (selectedNode != null) {
				if (selectedNode.getType().equals("folder"))
					testCase.setFolderUrl(getUrl(selectedNode) + "/");
				// selectedPropertyGroup = propertyGroup;
				// selectedPropertyGroup.setTestCasePropertyList(testCasePropertyList);
				// selectedPropertyGroup.setTestCase(testCase);
				propertyGroupCatalog = testCase.getPropertyGroupCatalog();
				if (propertyGroupCatalog == null)
					propertyGroupCatalog = new PropertyGroupCatalog();
				propertyGroupCatalog.getTestCasePropertyList().clear();
				testCase.setPropertyGroupCatalog(propertyGroupCatalog);
				propertyGroupCatalog.setTestCase(testCase);
				propertyGroupCatalog.setDescription("Default Properties" + testCase.getDescription());
				int j = 0;
				TestCaseProperty tcp = null;
				if (testCase.getTestSteps() != null && testCase.getTestSteps().size() > 0) {
					for (int i = 0; i < testCase.getTestSteps().size(); i++) {
						testCase.getTestSteps().get(i).setOrderNum(j);
						if (testCase.getTestSteps().get(i).getId() != 0)
							tcp = testCaseService.getPropertyOfTestStep(testCase.getTestSteps().get(i).getId());
						else
							tcp = null;
						j++;
						if ((testCase.getTestSteps().get(i).getAction_type().equals("type")
								|| testCase.getTestSteps().get(i).getAction_type().equals("select"))
								&& null != testCase.getTestSteps().get(i).getParam1()
								&& !testCase.getTestSteps().get(i).getParam1().equals("")) {
							if (tcp == null)
								tcp = new TestCaseProperty();
							tcp.setDescription(testCase.getTestSteps().get(i).getDescription());
							tcp.setKey(testCase.getTestSteps().get(i).getParam1());
							tcp.setTestStep(testCase.getTestSteps().get(i));
							tcp.setValue(testCase.getTestSteps().get(i).getDefaultValue());
							propertyGroupCatalog.getTestCasePropertyList().add(tcp);
						}
						if (testCase.getTestSteps().get(i).getType().equals("Java")) {
							AppParamDetail lapd = testCaseService.getAppParamDetByAppParamId(
									testCase.getTestSteps().get(i).getAppParameter().getId()).get(0);
							if (lapd.getAttr_value().contains("smartest.setParameter")
									|| lapd.getAttr_value().contains("smartest.getParameter")) {
								int start = lapd.getAttr_value().indexOf("$");
								int stop = lapd.getAttr_value().indexOf("\"", start);
								// int startVal
								if (tcp == null)
									tcp = new TestCaseProperty();
								tcp.setDescription(testCase.getTestSteps().get(i).getDescription());
								tcp.setKey(lapd.getAttr_value().substring(start, stop));
								// tcp.setValue(str[3]);
								tcp.setInputoutput("output");
								tcp.setTestStep(testCase.getTestSteps().get(i));
								propertyGroupCatalog.getTestCasePropertyList().add(tcp);
							}
						} else if (testCase.getTestSteps().get(i).getType().equals("TestCase")) {
							TestCase tc = testCaseService
									.getTestCaseById(testCase.getTestSteps().get(i).getTemplateId());
							if (tc.getPropertyGroupCatalog().getTestCasePropertyList() != null) {
								for (TestCaseProperty p : tc.getPropertyGroupCatalog().getTestCasePropertyList()) {
									TestCaseProperty ntcp = new TestCaseProperty();
									ntcp.setDescription(p.getDescription());
									ntcp.setKey(p.getKey());
									ntcp.setValue(p.getValue());
									ntcp.setSourceStr(p.getSourceStr());
									ntcp.setTestStep(p.getTestStep());
									propertyGroupCatalog.getTestCasePropertyList().add(ntcp);
								}

							}
						} else if (testCase.getTestSteps().get(i).getType().equals("Rest")) {
							Request req = preferencesService
									.getRequestById(testCase.getTestSteps().get(i).getTemplateId());
							if (req.getPropertyGroupCatalog().getTestCasePropertyList() != null) {
								for (TestCaseProperty p : req.getPropertyGroupCatalog().getTestCasePropertyList()) {
									TestCaseProperty ntcp = new TestCaseProperty();
									ntcp.setDescription(p.getDescription());
									ntcp.setKey(p.getKey());
									ntcp.setValue(p.getValue());
									ntcp.setSourceStr(p.getSourceStr());
									ntcp.setTestStep(p.getTestStep());
									propertyGroupCatalog.getTestCasePropertyList().add(ntcp);
								}

							}
						}
					}
				}
				testCaseService.saveOrUpdateTestCase(testCase, username);

				// test case file oluşturma
				if (!testCase.getTypeName().equals("Function")) {
					try {
						testCaseService.createTestCaseFile(testCase, null, username);
						testCaseSelect = false;
						testCase = new TestCase();
					} catch (IOException e) {
						// FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "İşlem
						// Basarisiz:", e.toString());
						// FacesContext.getCurrentInstance().addMessage(null, msg);
						e.printStackTrace();
					}
				}

				FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "İşlem Basarili", null);
				FacesContext.getCurrentInstance().addMessage(null, msg);
			} else {

				FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Klasör Seçiniz", null);
				FacesContext.getCurrentInstance().addMessage(null, msg);
			}
			tempSelectTestCase = null;
			getTestCaseListe();

		} else {
			FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Bu Test Case var", null);
			FacesContext.getCurrentInstance().addMessage(null, msg);
		}
	}

	public void saveOrUpdateTestCaseForPlugin() {

		if ((validateTestCaseName(testCase.getName()) && testCase.getName() != null
				&& !testCase.getName().trim().equals("")) || autoTestCaseCreateMode == true) {

			if (selectedNode != null) {
				if (selectedNode.getType().equals("folder"))
					testCase.setFolderUrl(getUrl(selectedNode) + "/");

				testCaseService.saveOrUpdateTestCase(testCase, username);
				int j = 1;

				if (testCase.getTestSteps() != null) {
					if (testCase.getTestSteps().size() > 0) {
						for (int i = 0; i < testCase.getTestSteps().size(); i++) {
							// testCase.getTestSteps().get(i).setTestCase(testCase);
							testCase.getTestSteps().get(i).setOrderNum(j);
							j++;
						}
						testCaseService.saveOrUpdateTestStep(testCase.getTestSteps());
					}
				}

				// test case file oluşturma
				if (!testCase.getTypeName().equals("Function")) {
					try {
						testCaseService.createTestCaseFile(testCase, null, username);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

				FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "İşlem Basarili", null);
				FacesContext.getCurrentInstance().addMessage(null, msg);
			} else {

				FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Klasör Seçiniz", null);
				FacesContext.getCurrentInstance().addMessage(null, msg);
			}
			tempSelectTestCase = null;

		} else {
			FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Bu Test Case var", null);
			FacesContext.getCurrentInstance().addMessage(null, msg);
		}
		testCase = new TestCase();

	}

	private boolean validateTestCaseName(String testCaseName) { 
		String url = getUrl(selectedNode) + "/";
		List<TestCase> tempTC = testCaseService.getTestCaseByUrlName(url, testCase.getName());

		if (!tempTC.isEmpty())
			return false;
		else
			return true;
	}

	// custom validation method
	public void validateSalePrice(FacesContext context, UIComponent componentToValidate, Object value)
			throws ValidatorException {
		boolean is = true;
		String url = getUrl(selectedNode) + "/";
		List<TestCase> tempTC = testCaseService.getTestCaseByUrlName(url, testCase.getName());
		if (!tempTC.isEmpty()) {
			FacesMessage message = new FacesMessage("Bu Test Case Adı Kullanılmaktadır");
			throw new ValidatorException(message);
		}
	}

	public void getTestCaseDetailForUpdate() {
		testCase = testCaseService.getTestCaseById(selectTestCase.getId());
		testCase.setTestSteps(testCaseService.getTestStepById(testCase.getId()));
	}

	public void removeTestStep(TestStep ts) {
		testStepList.remove(ts);
	}

	public void removeTestStep1(TestStep ts) {
		testCase.getTestSteps().remove(ts);

	}

	public void addTestStep(AppParameter appParameter) {
		if (appParameter == null){
			FacesContext.getCurrentInstance().addMessage(null,new FacesMessage("Please select appParam"));
		  return;
		}

		AppParamDetail appdet = (AppParamDetail) testCaseService.getAppParamDetByAppParamId(appParameter.getId()).get(0);
		tStep = new TestStep();
		tStep.setType(appParameter.getType());
		tStep.setAction_type(appdet.getAction_type());
		tStep.setDescription(appParameter.getDescription());
		tStep.setAppParameter(appParameter);
		if (testCase.getTestSteps() == null) {
			tStep.setOrderNum(0);
			testCase.setTestSteps(new ArrayList<TestStep>());
			testCase.getTestSteps().add(tStep);
		} else {
			tStep.setOrderNum(testCase.getTestSteps().size());
			testCase.getTestSteps().add(tStep);
		}
	}

	private int getNextOrderNum(List<TestStep> testSteps) {
		int max_num = 0;
		for (TestStep ts : testSteps) {
			if (ts.getOrderNum() > max_num)
				max_num = ts.getOrderNum();
		}
		return max_num + 1;
	}

	public void addTestStepForRest(SelectEvent event) {

		Request req = (Request) event.getObject();
		tStep = new TestStep();
		tStep.setType(req.getType());
		tStep.setDescription(req.getDescription());
		tStep.setTemplateId(req.getId());
		tStep.setOrderNum(testCase.getTestSteps().size());
		if (testCase.getTestSteps() == null) {
			testCase.setTestSteps(new ArrayList<TestStep>());
			testCase.getTestSteps().add(tStep);
		} else {
			testCase.getTestSteps().add(tStep);
		}
		configServiceOfTestCase(tStep);
	}

	public void addAssertEquals() {
		selectedAppDet.setAttr_value(
				selectedAppDet.getAttr_value() + "\n" + "Assert.assertEquals(\"expected\" , \"actual\");");
	}

	public void addAssertExist() {
		selectedAppDet.setAttr_value(selectedAppDet.getAttr_value() + "\n" + "Assert.assertExist(webElement);");
	}

	public void addAssertTrue() {
		selectedAppDet.setAttr_value(selectedAppDet.getAttr_value() + "\n" + "Assert.assertTrue(webElement != null);");
	}

	public void addAssertThat() {
		selectedAppDet.setAttr_value(selectedAppDet.getAttr_value() + "\n" + "Assert.assertThat(\"\",\"\",is());");
	}

	public void sendEnterKey() {
		selectedAppDet.setAttr_value(selectedAppDet.getAttr_value() + "\n" + "webElement.sendKeys(Keys.ENTER);");
	}

	public void threadSleep() {
		selectedAppDet.setAttr_value(selectedAppDet.getAttr_value() + "\n" + "Thread.sleep(3000);");
	}

	public void executeSQL() {
		selectedAppDet.setAttr_value(selectedAppDet.getAttr_value() + "\n"
				+ "ResultSet rs = smartest.executeSql(\"\");\n   while (rs.next()) {\n\n}");
	}

	public void setParameter() {
		selectedAppDet.setAttr_value(
				selectedAppDet.getAttr_value() + "\n" + "smartest.setParameter(\"parameter\",\"value\");");
	}

	public void getParameterFromTestCase() {
		selectedAppDet.setAttr_value(
				selectedAppDet.getAttr_value() + "\n" + "smartest.setParameter(\"parameter\",\"value\");");
	}

	public void getSelectBoxText() {
		selectedAppDet.setAttr_value(
				selectedAppDet.getAttr_value() + "\n" + "String text = smartest.getSelectBoxText(webElement);");
	}

	public void onRowSelectAppParameter(SelectEvent event) {
		selectedAppDet.setAttr_value(selectedAppDet.getAttr_value()
				+ "\n webElement = smartest.getElementByAppId(driver,\"\"," + selectedListWebElement.getId() + ",\""
				+ selectedListWebElement.getPlatform() + "\");//" + selectedListWebElement.getDescription() + "");
	}

	public void viewUpdateJavaCode(TestStep testStep) {
		selectedAppDet = testCaseService.getAppParamDetByAppParamId(testStep.getAppParameter().getId()).get(0);
		selectedAppParam = testCaseService.getAppParamaterById(testStep.getAppParameter().getId());
	}

	public void viewTestStepOfTestCase(TestStep tstep) {
		testCaseView = testCaseService.getTestCaseById(tstep.getTemplateId());
		testStepsOfTestCase = testCaseService.getTestStepById(testCaseView.getId());
		testCaseView.setTestSteps(testStepsOfTestCase);
	}

	public void onRowSelectPropertyGroup(SelectEvent event) {
		propertyListForOutput.clear();
		propertyListForOutput.addAll(selectedPropertyGroup.getTestCasePropertyList());
		linkedTestCaseString = "" + selectedPropertyGroup.getTestCase().getId() + "_"
				+ selectedPropertyGroup.getTestCase().getName();
	}

	public void onRowSelectProperty(SelectEvent event) {
		TestCaseProperty tcp = (TestCaseProperty) event.getObject();
		linkedPropertyString = linkedTestCaseString + "_" + tcp.getKey();
		selectedProperty.setSourceStr(linkedPropertyString);
		selectedAppDet.setAttr_value(selectedAppDet.getAttr_value() + "\n" + "String text = smartest.getParameter(\""
				+ linkedPropertyString + "\");");

	}

	public void configServiceOfTestCase(TestStep tStep) {
		int lastIndex = 0;
		Request req = preferencesService.getRequestById(tStep.getTemplateId());
		String str = req.getRequest_text();
		while (lastIndex != -1) {

			lastIndex = str.indexOf("$", lastIndex);

			if (lastIndex != -1) {
				String var = str.substring(lastIndex + 1, str.indexOf("$", lastIndex + 1));
				// testCaseVariables.add(var, "");
				lastIndex += 1;
				TestCaseVariable tcv = new TestCaseVariable();
				tcv.setKey(var);
				testCaseVariables.add(tcv);
			}
		}
		// testCase.setTestCaseVariables(testCaseVariables);
	}

	public void initializeJavaCode() {
		selectedAppDet = new AppParamDetail();
		selectedAppParam = new AppParameter();
		selectedAppParam.setType("Java");
		selectedAppDet.setAction_type("Java");
		List<String> autoCompleteTest = new ArrayList<String>();
		autoCompleteTest.add("WebDriver webdriver");
		autoCompleteTest.add("Smartest");
		autoCompleteTest.add("driver.getCurrentUrl()");
		autoCompleteTest.add("webElement.getText();");
	}

	public void saveJavaCode() {
		if (selectedAppParam.getId() == 0) {
			tStep = new TestStep();
			tStep.setType(selectedAppParam.getType());
			tStep.setAction_type(selectedAppDet.getAction_type());
			tStep.setDescription(selectedAppParam.getDescription());
			tStep.setAppParameter(selectedAppParam);
			if (testCase.getTestSteps() == null) {
				tStep.setOrderNum(0);
				testCase.setTestSteps(new ArrayList<TestStep>());
				testCase.getTestSteps().add(tStep);
			} else {
				tStep.setOrderNum(testCase.getTestSteps().size());
				testCase.getTestSteps().add(tStep);
			}
		}
		selectedAppParam.getAppParamDetailList().add(selectedAppDet);
		preferencesService.saveParameter(selectedAppParam);
		// selectedAppDet.setApp_param_id(selectedAppParam.getId());
		preferencesService.saveAppParamDetail(selectedAppDet);
		selectedAppDet = new AppParamDetail();
		selectedAppParam = new AppParameter();

	}

	private void deleteAppParameter(int old_java_code_app_det2) {
		testStepPageController.deleteParam(testStepPageController.getAppParameterById(old_java_code_app_det2));
	}

	public void deleteTestCase() {

		testCaseService.deleteTestCase(testCase);
		getTestCaseListe();

		FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO,
				generalServices.getBundle().getString("msgsilmebsrl"), null);
		FacesContext.getCurrentInstance().addMessage(null, msg);

	}

	// temp test case funct.

	public void onRowSelectCase(SelectEvent event) {
		if (tempTcEditable == true) {
			tempSelectTestCase.setTestSteps(testCaseService.getTestStepById(tempSelectTestCase.getId()));
			if (testCase.getTestSteps() == null) {
				testCase.setTestSteps(new ArrayList<TestStep>());
				testCase.getTestSteps().addAll(tempSelectTestCase.getTestSteps());
			} else {
				testCase.getTestSteps().addAll(tempSelectTestCase.getTestSteps());
			}
		} else {
			TestStep ts = new TestStep();
			ts.setType("TestCase");
			ts.setStatus("Aktif");
			ts.setAction_type("TestCase");
			ts.setTemplateId(tempSelectTestCase.getId());
			ts.setDescription(tempSelectTestCase.getDescription());
			if (testCase.getTestSteps() == null) {
				testCase.setTestSteps(new ArrayList<TestStep>());
				ts.setOrderNum(0);
				testCase.getTestSteps().add(ts);

			} else {
				ts.setOrderNum(testCase.getTestSteps().size());
				testCase.getTestSteps().add(ts);
			}
		}
	}

	public void onRowSelectRest() {
		TestStep ts = new TestStep();
		ts.setType("Rest");
		ts.setStatus("Aktif");
		ts.setAction_type("Rest");
		ts.setTemplateId(selectedRequest.getId());
		ts.setDescription(selectedRequest.getDescription());
		if (testCase.getTestSteps() == null) {
			testCase.setTestSteps(new ArrayList<TestStep>());
			testCase.getTestSteps().add(ts);
		} else {
			testCase.getTestSteps().add(ts);
		}
	}

	public void onRowUnselectCase(UnselectEvent event) {
		tempSelectTestCase = null;
	}

	public void prepareForNewTestCase() {
		setTestCaseSelect(true);
		testCase = new TestCase();
		tempTestCaseList = testCaseService.getAllTemplateCase();

		System.out.println("after prepare new testcase");
	}
	
	public void getStepValidationList(TestStep ts) {
		selectedValidationList = ts.getStepValidationList();
	}
	
	public void addValidation() {
		selectedValidationList.add(new StepValidation());
	}

	public void deleteValidation(StepValidation sv) {
		selectedValidationList.remove(sv);
	}
	
//	public void saveValidationList() {
//		testCaseService.saveOrUpdateValidation(selectedValidationList);
//	}
	// Test Suite funct ---------------------------
	public void onTransfer(TransferEvent event) {
		StringBuilder builder = new StringBuilder();
		for (Object item : event.getItems()) {
			if (event.isAdd()) {
				TargetCase.add((TestCase) item);
				SourceCase.remove((TestCase) item);
			} else {
				TargetCase.remove((TestCase) item);
				SourceCase.add((TestCase) item);
			}
		}

	}

	public void saveUpdateSuite() {

		try {

			if (testSuite.getId() == 0) {

				if (validateTestSuiteName(testSuite.getName()) && testSuite != null) {
					if (selectedNode != null) {
						testSuite.setCreated_by((String) session.getAttribute("username"));
						testSuite.setFolderUrl(getUrl(selectedNode) + "/");
						testSuite.setTestCaseList(TargetCase);
						testSuiteService.saveUpdate(testSuite);
						FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "İşlem Basarili", null);
						FacesContext.getCurrentInstance().addMessage(null, msg);
					} else {

						FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Klasör Seçiniz", null);
						FacesContext.getCurrentInstance().addMessage(null, msg);
					}

				} else {
					FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Bu Test Suite var", null);
					FacesContext.getCurrentInstance().addMessage(null, msg);
				}

			} else {
				testSuite.setTestCaseList(TargetCase);
				testSuiteService.saveUpdate(testSuite);
				FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Güncelleme Basarili", null);
				FacesContext.getCurrentInstance().addMessage(null, msg);
			}

			getTestCaseListe();
			testSuite = null;
		} catch (Exception ex) {
			logger.error(ex.toString());
		}

	}

	private boolean validateTestSuiteName(String testSuiteName) {
		boolean is = true;
		String url = getUrl(selectedNode) + "/";
		TestSuite tempTS = testSuiteService.getTestSuiteByUrlName(url, testSuiteName);

		if (tempTS != null)
			is = false;
		else
			is = true;
		return is;
	}

	// test case listeler
	public void getTestCaseListe() {

		root = new DefaultTreeNode("Root", null);
		testCaseList1 = testCaseService.getAllTestCase();
		rootNodes = new HashMap<String, DefaultTreeNode>();

		addFoldersToTree();

		for (TestCase testCase : testCaseList1) {
			TreeNode tcase = root;
			String url = testCase.getFolderUrl();
			if (isCaseSuiteAuth(url))
				continue;
			String[] parts = url.split("/");

			for (int i = 1; i < parts.length; i++) {

				for (int x = 0; x < tcase.getChildCount(); x++) {

					if (parts[i].equals(tcase.getChildren().get(x).getData().toString())) {
						tcase = tcase.getChildren().get(x);
					}
				}
			}

			rootNodes.put(testCase.getName(), new DefaultTreeNode("testcase", testCase.getName(), tcase));

		}
		// case sonu

		getTestSuiteListe();
	}

	public void getTestSuiteListe() {

		testSuiteList = testSuiteService.getListTestSuite();

		for (TestSuite testSuite : testSuiteList) {
			TreeNode tcase = root;
			String url = testSuite.getFolderUrl();
			if (isCaseSuiteAuth(url))
				continue;
			String[] parts = url.split("/");

			for (int i = 1; i < parts.length; i++) {

				for (int x = 0; x < tcase.getChildCount(); x++) {

					if (parts[i].equals(tcase.getChildren().get(x).getData().toString())) {
						tcase = tcase.getChildren().get(x);
					}
				}
			}

			rootNodes.put(testSuite.getName(), new DefaultTreeNode("testsuite", testSuite.getName(), tcase));

		}

	}

	// yeni klasör oluşturma
	public void createNewFolder() {

		try {
			if (validateFolderName(folderName) && folderName != null && !folderName.trim().equals("")) {
				Folder f = new Folder();
				f.setName(folderName);
				f.setUserRoles(userRoles);
				if (selectedNode != null)
					f.setFolder_url(getUrl(selectedNode) + "/" + folderName + "/");
				else
					f.setFolder_url("/" + folderName + "/");
				preferencesService.createFolder(f);
				getTestCaseListe();
				FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "İşlem Basarili", null);
				FacesContext.getCurrentInstance().addMessage(null, msg);
			} else {
				FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Bu klasörden var", null);
				FacesContext.getCurrentInstance().addMessage(null, msg);
			}
			folderName = null;
			selectedNode = null;
		} catch (Exception ex) {
			logger.error(ex.toString());
			;
		}
	}

	public void renameFolder() {

		try {

			folderName = selectedNode.getData().toString();
			selectedNode = (DefaultTreeNode) selectedNode.getParent();

			if (validateFolderName(newFolderName) && newFolderName != null && !newFolderName.trim().equals("")) {
				folderName = getUrl(selectedNode) + "/" + folderName + "/";
				newFolderName = getUrl(selectedNode) + "/" + newFolderName + "/";
				preferencesService.updateFolderName(folderName, newFolderName);
				// selectedNode.setData(newFolderName);
				getTestCaseListe();
				FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "İşlem Basarili", null);
				FacesContext.getCurrentInstance().addMessage(null, msg);
			} else {
				FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Bu klasörden var", null);
				FacesContext.getCurrentInstance().addMessage(null, msg);

			}

			newFolderName = null;
			folderName = null;
			logger.info("başarılı");
		} catch (Exception ex) {
			logger.error(ex.toString());
		}
	}

	public void deleteFolder() {

		try {

			selectFolder = preferencesService.getFolderByUrl(getUrl(selectedNode) + "/");
			preferencesService.deleteFolder(selectFolder);
			getTestCaseListe();
			FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "İşlem Basarili", null);
			FacesContext.getCurrentInstance().addMessage(null, msg);
		} catch (Exception ex) {
			logger.error(ex.toString());
		}

	}


	public void onCaseSelect() {

		if (selectedNode == null){
			FacesContext.getCurrentInstance().addMessage(null,new FacesMessage("please select node"));
		}
		testCase = new TestCase();
		testSuite = null;
		testCaseSelect = false;
		testSuiteSelect = false;

		if (selectedNode.getType().equals("folder")) {
			selectedNode.setExpanded(!selectedNode.isExpanded());
			folderName = selectedNode.getData().toString();
			tempurl = getUrl(selectedNode);

		} else if (selectedNode.getType().equals("testcase")) {
			testCase = testCaseService.getTestCaseByUrlName(
					getUrl((DefaultTreeNode) selectedNode.getParent()) + "/",
					(String) selectedNode.getData()).get(0);
			testCase.setTestSteps(testCaseService.getTestStepById(testCase.getId()));
			setTestCaseSelect(true);
			setTestSuiteSelect(false);
		} else if (selectedNode.getType().equals("testsuite")) {
			setTestCaseSelect(false);
			setTestSuiteSelect(true);
			testSuite = testSuiteService.getTestSuiteByUrlName(
					getUrl((DefaultTreeNode) selectedNode.getParent()) + "/",
					(String) selectedNode.getData());
			resetSuite();

		}
	}

	// klasör & case seçme
	public void onRowSelect(NodeSelectEvent event) {
		testCase = new TestCase();
		testSuite = null;
		testCaseSelect = false;
		testSuiteSelect = false;

		if (selectedNode.getType().equals("folder")) {
			selectedNode.setExpanded(!selectedNode.isExpanded());
			folderName = selectedNode.getData().toString();
			tempurl = getUrl(selectedNode);

		} else if (selectedNode.getType().equals("testcase")) {
			testCase = testCaseService.getTestCaseByUrlName(
					getUrl((DefaultTreeNode) event.getTreeNode().getParent()) + "/",
					(String) event.getTreeNode().getData()).get(0);
			testCase.setTestSteps(testCaseService.getTestStepById(testCase.getId()));
			setTestCaseSelect(true);
			setTestSuiteSelect(false);
		} else if (selectedNode.getType().equals("testsuite")) {
			setTestCaseSelect(false);
			setTestSuiteSelect(true);
			testSuite = testSuiteService.getTestSuiteByUrlName(
					getUrl((DefaultTreeNode) event.getTreeNode().getParent()) + "/",
					(String) event.getTreeNode().getData()); 
			resetSuite();
			
		}
	}

	public void onRowUnSelect(UnselectEvent event) {
		selectedNode = null;
	}

	public String getUrl(DefaultTreeNode selected) {

		String url = selected.getData().toString();
		TreeNode tnode = selected;
		while (true) {

			if (tnode != null & tnode.getParent() != null && !tnode.getParent().getData().toString().equals("Root")) {
				tnode = tnode.getParent();
				url = tnode.getData().toString() + "/" + url;
			} else {
				break;
			}
		}

		url = "/" + url;
		return url;

	}

	private boolean validateFolderName(String folderName) {
		boolean is = true;
		String url = getUrl(selectedNode) + "/" + folderName + "/";
		selectFolder = preferencesService.getFolderByUrl(url);

		if (selectFolder != null)
			is = false;
		else
			is = true;
		return is;
	}

	public void onTestStepReorder(ReorderEvent event) {

		int dragts = event.getFromIndex(); // taşınan
		int dropts = event.getToIndex(); // yeni taşınan

		if (dragts < dropts) {
			for (TestStep ts : testCase.getTestSteps()) {
				if (ts.getOrderNum() < dragts || ts.getOrderNum() > dropts) {

				}
				if (dropts == ts.getOrderNum()) {
					ts.setOrderNum(dropts);
				} else if (ts.getOrderNum() > dragts)
					ts.setOrderNum(ts.getOrderNum() - 1);
			}
		} else {
			for (TestStep ts : testCase.getTestSteps()) {
				if (ts.getOrderNum() > dragts || ts.getOrderNum() < dropts) {

				}
				if (dropts == ts.getOrderNum()) {
					ts.setOrderNum(dropts);
				} else if (ts.getOrderNum() < dragts)
					ts.setOrderNum(ts.getOrderNum() - 1);
			}
		}
	}

	public void onDragDrop(TreeDragDropEvent event) {

		TreeNode dragNode = event.getDragNode(); // taşınan
		TreeNode dropNode = event.getDropNode(); // yeni taşınan

		if (dragNode.getType().equals("folder")) {

			if (selectedNode == null) {
				FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Taşıncak Klasörü seçiniz", null);
				FacesContext.getCurrentInstance().addMessage(null, msg);
				return;
			}

			selectFolder = preferencesService.getFolderByUrl(tempurl + "/");

			String url = getUrl((DefaultTreeNode) dragNode) + "/";
			selectFolder = preferencesService.getFolderByUrl(url);

			if (selectFolder == null) {

				preferencesService.updateFolderName(tempurl + "/", url);

			} else {
				FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Bu Klasör Taşınamaz", null);
				FacesContext.getCurrentInstance().addMessage(null, msg);
			}

			FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO,
					generalServices.getBundle().getString("klstsn"), null);
			FacesContext.getCurrentInstance().addMessage(null, msg);

		} else if (dragNode.getType().equals("testcase")) {

			if (selectTestCase == null) {
				FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Taşıncak Case seçiniz", null);
				FacesContext.getCurrentInstance().addMessage(null, msg);
				return;
			}

			if (dropNode.getType().equals("folder")) {
				String url = getUrl((DefaultTreeNode) dropNode) + "/";
				tempTC = testCaseService.getTestCaseByUrlName(getUrl((DefaultTreeNode) dropNode),
						dragNode.getData().toString()).get(0);

				if (tempTC == null) {

					selectTestCase.setFolderUrl(url);
					testCaseService.saveOrUpdateTestCase(selectTestCase, username);

				} else {
					FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Bu Klasöre Taşınamaz", null);
					FacesContext.getCurrentInstance().addMessage(null, msg);
				}

				FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO,
						generalServices.getBundle().getString("casetsn"), null);
				FacesContext.getCurrentInstance().addMessage(null, msg);
			} else if (dropNode.getType().equals("testsuite")) {
				// işlemler
				String url = getUrl((DefaultTreeNode) dropNode.getParent()) + "/";
				TestSuite temp = testSuiteService.getTestSuiteByUrlName(url, dropNode.getData().toString());
				TestCase tempcase = selectTestCase;
				boolean isadd = true;
				for (TestCase t : temp.getTestCaseList()) {
					if (t.getId() == tempcase.getId()) {
						isadd = false;
						break;
					}

				}
				if (isadd) {
					temp.getTestCaseList().add(tempcase);
					testSuiteService.saveUpdate(temp);
					FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "İşlem Başarılı", null);
					FacesContext.getCurrentInstance().addMessage(null, msg);

				} else {
					FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Bu Suit içinde Bu Case var",
							null);
					FacesContext.getCurrentInstance().addMessage(null, msg);
				}

			}

		} else if (dragNode.getType().equals("testsuite")) {

			if (testSuite == null) {
				FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Taşıncak Suite seçiniz", null);
				FacesContext.getCurrentInstance().addMessage(null, msg);
				return;
			}

			String url = getUrl((DefaultTreeNode) dropNode) + "/";
			TestSuite tempTS = testSuiteService.getTestSuiteByUrlName(getUrl((DefaultTreeNode) dropNode),
					dragNode.getData().toString());

			if (tempTS == null) {

				testSuite.setFolderUrl(url);
				testSuiteService.saveUpdate(testSuite);

			} else {
				FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Bu Klasöre Taşınamaz", null);
				FacesContext.getCurrentInstance().addMessage(null, msg);
			}

			FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Test Suite Taşındı", null);
			FacesContext.getCurrentInstance().addMessage(null, msg);

		}

		getTestCaseListe(); // reload tree

	}

	public void resetSuite() { 
		SourceCase = testCaseService.getAllTestCase();
		TargetCase = new ArrayList<TestCase>();
		dualCaseList = new DualListModel<TestCase>(SourceCase, TargetCase);
		TargetCase.addAll(testSuite.getTestCaseList());
		for (TestCase tc : testSuite.getTestCaseList()) {
			for (TestCase t : SourceCase) {
				if (t.getId() == tc.getId()) {
					SourceCase.remove(t);
					break;
				}
			}

		}
	}

	public void deleteSuite() {
		testSuiteService.deleteSuite(testSuite);
		FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Silme Basarili", null);
		FacesContext.getCurrentInstance().addMessage(null, msg);
		getTestCaseListe();
	}

	// getter setter

	public PreferencesService getPreferencesService() {
		return preferencesService;
	}

	public void setPreferencesService(PreferencesService preferencesService) {
		this.preferencesService = preferencesService;
	}

	public GeneralServices getGeneralServices() {
		return generalServices;
	}

	public void setGeneralServices(GeneralServices generalServices) {
		this.generalServices = generalServices;
	}

	public TestCaseService getTestCaseService() {
		return testCaseService;
	}

	public void setTestCaseService(TestCaseService testCaseService) {
		this.testCaseService = testCaseService;
	}

	public TestCase getSelectTestCase() {
		return selectTestCase;
	}

	public void setSelectTestCase(TestCase selectTestCase) {
		this.selectTestCase = selectTestCase;
	}

	public List<String> getTcTypeList() {
		return tcTypeList;
	}

	public void setTcTypeList(List<String> tcTypeList) {
		this.tcTypeList = tcTypeList;
	}

	public TreeNode getRoot() {
		return root;
	}

	public void setRoot(TreeNode root) {
		this.root = root;
	}

	public DefaultTreeNode getSelectedNode() {
		return selectedNode;
	}

	public void setSelectedNode(DefaultTreeNode selectedNode) {
		this.selectedNode = selectedNode;
	}

	public String getFolderName() {
		return folderName;
	}

	public void setFolderName(String folderName) {
		this.folderName = folderName;
	}

	public Map<String, DefaultTreeNode> getRootNodes() {
		return rootNodes;
	}

	public void setRootNodes(Map<String, DefaultTreeNode> rootNodes) {
		this.rootNodes = rootNodes;
	}

	public boolean isTestCaseSelect() {
		return testCaseSelect;
	}

	public void setTestCaseSelect(boolean testCaseSelect) {
		this.testCaseSelect = testCaseSelect;
	}

	public List<TestCase> getTestCaseList1() {
		return testCaseList1;
	}

	public void setTestCaseList1(List<TestCase> testCaseList1) {
		this.testCaseList1 = testCaseList1;
	}

	public String getNewFolderName() {
		return newFolderName;
	}

	public void setNewFolderName(String newFolderName) {
		this.newFolderName = newFolderName;
	}

	public List<TestStep> getTestStepList() {
		return testStepList;
	}

	public void setTestStepList(List<TestStep> testStepList) {
		this.testStepList = testStepList;
	}

	public List<TestStep> getFilterTestStep() {
		return filterTestStep;
	}

	public void setFilterTestStep(List<TestStep> filterTestStep) {
		this.filterTestStep = filterTestStep;
	}

	public List<TestCase> getTempTestCaseList() {
		return tempTestCaseList;
	}

	public void setTempTestCaseList(List<TestCase> tempTestCaseList) {
		this.tempTestCaseList = tempTestCaseList;
	}

	public TestCase getTempSelectTestCase() {
		return tempSelectTestCase;
	}

	public void setTempSelectTestCase(TestCase tempSelectTestCase) {
		this.tempSelectTestCase = tempSelectTestCase;
	}

	public List<TestCase> getTempTCFilter() {
		return tempTCFilter;
	}

	public void setTempTCFilter(List<TestCase> tempTCFilter) {
		this.tempTCFilter = tempTCFilter;
	}

	public TestCase getTestCase() {
		return testCase;
	}

	public void setTestCase(TestCase testCase) {
		this.testCase = testCase;
	}

	public List<UserRoles> getListRoles() {
		return listRoles;
	}

	public void setListRoles(List<UserRoles> listRoles) {
		this.listRoles = listRoles;
	}

	public int getUserRoles() {
		return userRoles;
	}

	public void setUserRoles(int userRoles) {
		this.userRoles = userRoles;
	}

	public TestSuite getTestSuite() {
		return testSuite;
	}

	public void setTestSuite(TestSuite testSuite) {
		this.testSuite = testSuite;
	}

	public boolean isTestSuiteSelect() {
		return testSuiteSelect;
	}

	public void setTestSuiteSelect(boolean testSuiteSelect) {
		this.testSuiteSelect = testSuiteSelect;
	}

	public TestSuiteService getTestSuiteService() {
		return testSuiteService;
	}

	public void setTestSuiteService(TestSuiteService testSuiteService) {
		this.testSuiteService = testSuiteService;
	}

	public DualListModel<TestCase> getDualCaseList() {
		return dualCaseList;
	}

	public void setDualCaseList(DualListModel<TestCase> dualCaseList) {
		this.dualCaseList = dualCaseList;
	}

	public TestStep gettStep() {
		return tStep;
	}

	public void settStep(TestStep tStep) {
		this.tStep = tStep;
	}

	public AppParameter getSelectedListWebElement() {
		return selectedListWebElement;
	}

	public void setSelectedListWebElement(AppParameter selectedListWebElement) {
		this.selectedListWebElement = selectedListWebElement;
	}

	public TestStepPageController getTestStepPageController() {
		return testStepPageController;
	}

	public void setTestStepPageController(TestStepPageController testStepPageController) {
		this.testStepPageController = testStepPageController;
	}

	public boolean isTempTcEditable() {
		return tempTcEditable;
	}

	public void setTempTcEditable(boolean tempTcEditable) {
		this.tempTcEditable = tempTcEditable;
	}

	// public int getOld_java_code_app_det() {
	// return old_java_code_app_det;
	// }
	//
	// public void setOld_java_code_app_det(int old_java_code_app_det) {
	// this.old_java_code_app_det = old_java_code_app_det;
	// }
	//
	// public TestStep getOld_java_code_test_step() {
	// return old_java_code_test_step;
	// }
	//
	// public void setOld_java_code_test_step(TestStep old_java_code_test_step) {
	// this.old_java_code_test_step = old_java_code_test_step;
	// }

	public List<TestStep> getTestStepsOfTestCase() {
		return testStepsOfTestCase;
	}

	public void setTestStepsOfTestCase(List<TestStep> testStepsOfTestCase) {
		this.testStepsOfTestCase = testStepsOfTestCase;
	}

	public TestCase getTestCaseView() {
		return testCaseView;
	}

	public void setTestCaseView(TestCase testCaseView) {
		this.testCaseView = testCaseView;
	}

	public boolean isAutoTestCaseCreateMode() {
		return autoTestCaseCreateMode;
	}

	public void setAutoTestCaseCreateMode(boolean autoTestCaseCreateMode) {
		this.autoTestCaseCreateMode = autoTestCaseCreateMode;
	}

	public List<TestCaseVariable> getTestCaseVariables() {
		return testCaseVariables;
	}

	public void setTestCaseVariables(List<TestCaseVariable> testCaseVariables) {
		this.testCaseVariables = testCaseVariables;
	}

	public AppParamDetail getSelectedAppDet() {
		return selectedAppDet;
	}

	public void setSelectedAppDet(AppParamDetail selectedAppDet) {
		this.selectedAppDet = selectedAppDet;
	}

	public AppParameter getSelectedAppParam() {
		return selectedAppParam;
	}

	public void setSelectedAppParam(AppParameter selectedAppParam) {
		this.selectedAppParam = selectedAppParam;
	}

	public TestCaseProperty getSelectedProperty() {
		return selectedProperty;
	}

	public void setSelectedProperty(TestCaseProperty selectedProperty) {
		this.selectedProperty = selectedProperty;
	}

	public PropertyGroup getPropertyGroup() {
		return propertyGroup;
	}

	public void setPropertyGroup(PropertyGroup propertyGroup) {
		this.propertyGroup = propertyGroup;
	}

	public List<PropertyGroup> getPropertyGroupList() {
		return propertyGroupList;
	}

	public void setPropertyGroupList(List<PropertyGroup> propertyGroupList) {
		this.propertyGroupList = propertyGroupList;
	}

	public PropertyGroup getSelectedPropertyGroup() {
		return selectedPropertyGroup;
	}

	public void setSelectedPropertyGroup(PropertyGroup selectedPropertyGroup) {
		this.selectedPropertyGroup = selectedPropertyGroup;
	}

	public TestCaseProperty getTestCaseProperty() {
		return testCaseProperty;
	}

	public void setTestCaseProperty(TestCaseProperty testCaseProperty) {
		this.testCaseProperty = testCaseProperty;
	}

	public Set<TestCaseProperty> getTestCasePropertyList() {
		return testCasePropertyList;
	}

	public void setTestCasePropertyList(Set<TestCaseProperty> testCasePropertyList) {
		this.testCasePropertyList = testCasePropertyList;
	}

	public PropertyGroupCatalog getPropertyGroupCatalog() {
		return propertyGroupCatalog;
	}

	public void setPropertyGroupCatalog(PropertyGroupCatalog propertyGroupCatalog) {
		this.propertyGroupCatalog = propertyGroupCatalog;
	}

	public Request getSelectedRequest() {
		return selectedRequest;
	}

	public void setSelectedRequest(Request selectedRequest) {
		this.selectedRequest = selectedRequest;
	}

	public List<Request> getRequestList() {
		return requestList;
	}

	public void setRequestList(List<Request> requestList) {
		this.requestList = requestList;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Set<StepValidation> getSelectedValidationList() {
		return selectedValidationList;
	}

	public void setSelectedValidationList(Set<StepValidation> selectedValidationList) {
		this.selectedValidationList = selectedValidationList;
	}

}
