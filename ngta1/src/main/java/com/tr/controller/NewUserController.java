package com.tr.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import com.tr.model.User;
import com.tr.model.UserAuthorization;
import com.tr.service.GeneralServices;
import com.tr.service.UserService;
import lombok.Data;

@ManagedBean(name = "newUserController")
@Data
@SessionScoped
public class NewUserController implements Serializable {

	/**
	 * yeni kullanıcı ekleme sayfası  
	 * 
	 */
	 private static final long serialVersionUID = 1L;
	 private static Logger logger =Logger.getLogger(NewUserController.class);	
     private User user  , selectUser;
     private List<User> listUser, filterUser;  
     @ManagedProperty("#{userService}")
 	 private UserService userService;
     transient HttpSession session ;
     private List<UserAuthorization> userAuth ;
     
     @ManagedProperty("#{generalServices}")
     private GeneralServices generalServices ;

	public NewUserController() throws IOException {
		super();
		 session= (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true); 

		    if (session.getAttribute("userId") == null) {
		            ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
		            context.redirect(context.getRequestContextPath() + "/login.xhtml?faces-redirect=true");
		    }
		   
	        reset();
	        
	}
	
	@PostConstruct
	public void init()
	{
		 userAuth= userService.getUserAuth();
	}
	

	
	//new save && update funct.
	public void save(){
		
		try{		
		
	     if(!user.getUser_name().trim().equals("") && !user.getMailAddress().trim().equals(""))
	     {
	    	 
	    	 if(user.getId()==0){
	    		user.setPassword(user.getUser_name());
				user.setRecordMacadress(generalServices.getMacAddress());
		        user.setRecordIpadress(generalServices.getIpAddress());
		        user.setRecordDate(new java.util.Date() );
		        
			}else{
				user.setUpdateMacadress(generalServices.getMacAddress());
		        user.setUpdateIpadress(generalServices.getIpAddress());
		        user.setUpdateDate(new java.util.Date() );
			}
				
				userService.createUser(user);
				FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO,  generalServices.getBundle().getString("msgkayitbsrl"), null);
				FacesContext.getCurrentInstance().addMessage(null, msg);
		 }else{
			 FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR,  "Ad ve Mail Zorunlu Alan", null);
				FacesContext.getCurrentInstance().addMessage(null, msg);
		 }
		    
		}catch(Exception ex){
			FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR,  generalServices.getBundle().getString("msghata")+ex.toString(), null);
			FacesContext.getCurrentInstance().addMessage(null, msg);
		}finally{
			reset();
		}
		
		
	}
	
	
	// delete func.
	public void delete(){
		 
		  try{
			  userService.deleteUser(user);
			  FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO,  generalServices.getBundle().getString("msgsilmebsrl"), null);
			  FacesContext.getCurrentInstance().addMessage(null, msg);
			
		  }catch(Exception ex){
			  FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR,  generalServices.getBundle().getString("msghata")+ex.toString(), null);
			  FacesContext.getCurrentInstance().addMessage(null, msg);
		  }finally{
			  reset();
		  }
		
	}
	
	
	// reset function  
	
	public void reset(){
		
		user=new User();
		
	}
	// list function 
	public void getUserList(){
		
		listUser=userService.getUserList();
		
	}
	
	 //getter setter 
	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public GeneralServices getGeneralServices() {
		return generalServices;
	}

	public void setGeneralServices(GeneralServices generalServices) {
		this.generalServices = generalServices;
	}


	public User getSelectUser() {
		return selectUser;
	}


	public void setSelectUser(User selectUser) {
		this.selectUser = selectUser;
		if(selectUser!=null)
			user=selectUser ;
	}


	public List<User> getListUser() {
		return listUser;
	}


	public void setListUser(List<User> listUser) {
		this.listUser = listUser;
	}


	public List<User> getFilterUser() {
		return filterUser;
	}


	public void setFilterUser(List<User> filterUser) {
		this.filterUser = filterUser;
	}


	public List<UserAuthorization> getUserAuth() {
		return userAuth;
	}


	public void setUserAuth(List<UserAuthorization> userAuth) {
		this.userAuth = userAuth;
	}
    
	
     
}
