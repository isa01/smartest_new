package com.tr.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger; 
import org.primefaces.event.SelectEvent;

import com.tr.model.Action;
import com.tr.model.AppParamDetail;
import com.tr.model.AppParameter;
import com.tr.model.FindElementMethod;
import com.tr.model.PropertyGroupCatalog;
import com.tr.model.Request_Header;
import com.tr.model.TestCase;
import com.tr.model.TestCaseProperty;
import com.tr.model.TestStep;
import com.tr.model.Request;
import com.tr.model.RequestProperty;
import com.tr.service.GeneralServices;
import com.tr.service.PreferencesService;
import com.tr.service.TestCaseService;

import lombok.Data;

@ManagedBean(name = "testStepPageController", eager = true)
@Data
@SessionScoped
public class TestStepPageController implements Serializable {

	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(TestStepPageController.class);
	transient HttpSession session;

	@ManagedProperty("#{preferencesService}")
	private PreferencesService preferencesService;

	@ManagedProperty("#{generalServices}")
	private GeneralServices generalServices;

	@ManagedProperty("#{testCaseService}")
	private TestCaseService testCaseService;

	private List<AppParameter> appParameters = new ArrayList<AppParameter>();
	private List<AppParameter> filteredAppParameters = new ArrayList<AppParameter>();
	private AppParameter newAppParameter;
	private AppParamDetail newAppParamDetail;
	private List<AppParameter> filterAppParameter;
	private List<Action> actionList = new ArrayList<Action>();
	private Action selectedAction;
	private String newParamDesc;
	private AppParameter selectedAppParam;
	private String selectedPage;
	// private String jsonData;
	private List<Request_Header> headers;
	private Request request;
	private Request selectedRequest;
	private RequestProperty requestProperty;
	private List<Request> requestList;
	private Map<String, String> jsonParameterMap = new LinkedHashMap<String, String>();
	private Map<String, String> requestPropertyMap = new HashMap<String, String>();;
	private String rp_username = "hbintegration";
	private String rp_password = "Admin123";
	private String stepPage = "";
	private List<String> pagesFilter;
	private List<TestCase> usedTestCaseList;
	private String output = "";

	public TestStepPageController() throws IOException {
		super();

		session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);

		if (session.getAttribute("userId") == null) {
			ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
			context.redirect(context.getRequestContextPath() + "/login.xhtml?faces-redirect=true");
			FacesContext.getCurrentInstance().getViewRoot().setLocale(new Locale("tr"));
		}

	}
	public void init() {
		appParameters.clear();
		actionList.clear();
		appParameters.addAll(preferencesService.getAppParamaters());
		actionList.addAll(preferencesService.getActionList());
		pagesFilter = preferencesService.getPageFilter();
		apiload();
		setSelectedAppParam(null);
	}

	public void onload() {
		setSelectedAppParam(null);
		appParameters.clear();
		actionList.clear();
		appParameters.addAll(preferencesService.getAppParamaters());
		actionList.addAll(preferencesService.getActionList());
		pagesFilter = preferencesService.getPageFilter();
		apiload();
		FacesContext fc = FacesContext.getCurrentInstance();
		Map<String, String> params = fc.getExternalContext().getRequestParameterMap();

		if (params.get("ss") != null) {
			AppParameter app = testCaseService.getTestStepByTsId(Integer.parseInt(params.get("ss"))).getAppParameter();
			setSelectedAppParam(app); 
//			params.put("ss", null);
		} 
	}

	public List<String> getPagesFilter() {
		return preferencesService.getPageFilter();
	}

	public void deleteParam(AppParameter app) {
		usedTestCaseList = testCaseService.getTestCaseListByAppParam(app);
		if (app != null && usedTestCaseList.isEmpty()) {
			preferencesService.removeParam(app);
			appParameters.remove(app);
			FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO,
					generalServices.getBundle().getString("msgsilmebsrl"), null);
			FacesContext.getCurrentInstance().addMessage(null, msg);
		} else {
			FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR,
					generalServices.getBundle().getString("msgsilmebsrsz"), null);
			FacesContext.getCurrentInstance().addMessage(null, msg);
		}


	}

	public void deleteParamDetail(AppParamDetail appDet) {

		if (appDet != null) {
			preferencesService.removeParamDetail(appDet);
			selectedAppParam.getAppParamDetailList().remove(appDet);
		} else {
			selectedAppParam.getAppParamDetailList().remove(appDet);
		}

		FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO,
				generalServices.getBundle().getString("msgsilmebsrl"), null);
		FacesContext.getCurrentInstance().addMessage(null, msg);
	}

	public void addNewParameter() {
		newAppParameter = new AppParameter();
		newAppParameter.setPage("-");
		newAppParameter.setDescription("-");
		// appParameters.add(newAppParameter);

		appParameters.add(0, newAppParameter);
	}

	public void addNewParamDetail() {
		newAppParamDetail = new AppParamDetail();
		// newAppParamDetail.setApp_param_id(activeAppParam);
		selectedAppParam.getAppParamDetailList().add(newAppParamDetail);
		newAppParamDetail.setTag_name("-");
		newAppParamDetail.setAttr_name("-");
		newAppParamDetail.setAttr_value("-");
		newAppParamDetail.setAction_type("-");
		newAppParamDetail.setStatus("Aktif");
	}
	
	public void prepareAppParameter(AppParameter app) {
		selectedAppParam = app;
		usedTestCaseList = testCaseService.getTestCaseListByAppParam(app);
		
	}

	public synchronized void saveParameters() {

		try {

			for (AppParameter p : appParameters) {
				if (p.getPage().equals("-"))
					appParameters.remove(p);
			}
			preferencesService.saveParameters(appParameters);
			appParameters.clear();
			appParameters.addAll(preferencesService.getAppParamaters());
			FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO,
					generalServices.getBundle().getString("msgkayitbsrl"), null);
			FacesContext.getCurrentInstance().addMessage(null, msg);

		} catch (Exception ex) {
			logger.error(ex.toString());
		}
	}

	public AppParameter getAppParameterById(int id) {
		// TODO Auto-generated method stub
		return preferencesService.getAppParameterById(id);
	}

	public void saveParamDetails() {

		try {

			for (AppParamDetail p : selectedAppParam.getAppParamDetailList()) {
				if (p.getAction_type().equals(""))
					selectedAppParam.getAppParamDetailList().remove(p);
			}
			preferencesService.saveParameter(selectedAppParam);

			FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO,
					generalServices.getBundle().getString("msgkayitbsrl"), null);
			FacesContext.getCurrentInstance().addMessage(null, msg);

		} catch (Exception ex) {
			logger.error(ex.toString());
		}
	}

	// assertion process

	public List<FindElementMethod> getFindElementMethodList() {
		List<FindElementMethod> list = preferencesService.getFindElementMethodList();

		logger.info("ok..");
		return list;
	}

	public void saveNewAction() {
		newAppParameter = new AppParameter();
		newAppParameter.setPage("-");
		newAppParameter.setDescription(newParamDesc);

		List<AppParamDetail> appDetList = new ArrayList<AppParamDetail>();
		for (AppParamDetail kaynak_appdet : selectedAppParam.getAppParamDetailList()) {
			AppParamDetail hedef_appdet = new AppParamDetail();
			hedef_appdet.setAttr_name(kaynak_appdet.getAttr_name());
			hedef_appdet.setAttr_value(kaynak_appdet.getAttr_value());
			hedef_appdet.setTag_name(kaynak_appdet.getTag_name());
			hedef_appdet.setStatus("Aktif");
			hedef_appdet.setAction_type(selectedAction.getValue());
			// hedef_appdet.setApp_param_id(newAppParameter.getId());
			newAppParameter.getAppParamDetailList().add(hedef_appdet);
			appDetList.add(hedef_appdet);
		}
		appParameters.add(0, newAppParameter);
		preferencesService.saveParameter(newAppParameter);

	}

	public void apiload() {
		request = new Request();
		// rp_username = new RequestProperty();
		// rp_username.setKey("Username");
		// rp_password = new RequestProperty();
		// rp_password.setKey("Password");
		// rp_username.setRequest(request);
		// rp_password.setRequest(request);
		// request.getRequestPropertyList().add(rp_username);
		// request.getRequestPropertyList().add(rp_password);
		Request_Header header = new Request_Header();
		header.setRequest(request);
		header.setKey("X-Auth-Token");
		header.setValue("");
		headers = new ArrayList<Request_Header>();
		headers.add(header);
		request.setHeaders(headers);
		requestList = getAllRequests();
	}

	public void saveRequest() {
		List<String> keyList = new ArrayList<String>();
		String str = request.getRequest_text();
		Pattern p = Pattern.compile("\\[(.*?)\\]");
		Matcher m = p.matcher(str);
		while (m.find()) {
			TestCaseProperty tcp = new TestCaseProperty();
			tcp.setInputoutput("input");
			tcp.setKey(m.group(1));
			if (request.getPropertyGroupCatalog() == null) {
				request.setPropertyGroupCatalog(new PropertyGroupCatalog());
				request.getPropertyGroupCatalog().setTestCasePropertyList(new HashSet<TestCaseProperty>());
				request.getPropertyGroupCatalog().getTestCasePropertyList().add(tcp);
			} else {
				for (TestCaseProperty t : request.getPropertyGroupCatalog().getTestCasePropertyList())
					keyList.add(t.getKey());
				if (!keyList.contains(tcp.getKey()))
					request.getPropertyGroupCatalog().getTestCasePropertyList().add(tcp);
			}

		}
		// output = preferencesService.runRequest(request,requestPropertyMap);

		preferencesService.saveRequest(request);
	}

	public void saveTcps() {
		preferencesService.saveRequest(request);
	}

	public void resetRequest() {
		request = new Request();
		headers = new ArrayList<Request_Header>();
	}

	public void runRequest() {
		requestPropertyMap.remove("username");
		requestPropertyMap.remove("password");
		requestPropertyMap.put("username", rp_username);
		requestPropertyMap.put("password", rp_password);
//		output = preferencesService.runRequest(request, requestPropertyMap);
	}

	public void deleteRequest(Request_Header req) {
		preferencesService.deleteRequest(req);
	}

	public List<Request> getAllRequests() {
		return preferencesService.getAllRequests();
	}

	public void getRequestById(SelectEvent event) {
		request = (Request) event.getObject();
	}

	public List<Request_Header> getHeadersByRequest(int request_id) {
		return preferencesService.getHeadersByRequest(request_id);
	}

	// public void addHeader() {
	// Request_Header header = new Request_Header();
	// header.setRequest(request);
	// headers.add(header);
	// }

	public void addParameter() {
		TestCaseProperty tcp = new TestCaseProperty();
		if (request.getPropertyGroupCatalog() == null) {
			request.setPropertyGroupCatalog(new PropertyGroupCatalog());
			request.getPropertyGroupCatalog().setTestCasePropertyList(new HashSet<TestCaseProperty>());
		}
		request.getPropertyGroupCatalog().getTestCasePropertyList().add(tcp);

	}

	public void removeHeader(Request_Header header) {
		headers.remove(header);
	}

	public void generateParams() {
		String parent = "";

		String[] params = request.getRequest_text().split("\"");
		for (int i = 0; i < params.length; i++) {
			String param = params[i];
			if (param.contains("}")) {
				parent = parent.substring(0, parent.lastIndexOf("_"));

			}
			if (!param.contains("}") && !param.contains("{") && !param.contains(":") && !param.contains(",")
					&& !params[i - 1].equals(":")) {
				if (params[i - 1].contains(":{")) {
					if (parent.equals(""))
						parent = "_" + params[i - 2];
					else
						parent = parent + "_" + params[i - 2];
				}
				if (i < params.length - 2 && !params[i + 1].contains(":{")) {
					String finalParam = parent + "_" + param;
					if (params[i + 1].equals(":"))
						jsonParameterMap.put(finalParam.substring(1, finalParam.length()), params[i + 2]);
					else {
						String str = params[i + 1];
						str = str.substring(1, str.length());
						if (str.contains(","))
							str = str.substring(0, str.indexOf(","));
						if (str.contains("}"))
							str = str.substring(0, str.indexOf("}"));
						jsonParameterMap.put(finalParam.substring(1, finalParam.length()), str);
					}

				}
			}
		}
	}

	public void pageFilterChange() {
		appParameters = preferencesService.getPageFilter(stepPage);
	}

	public void selectedPageChanged() {
		appParameters = preferencesService.getAppParamsByPage(selectedPage);
	}

	public PreferencesService getPreferencesService() {
		return preferencesService;
	}

	public void setPreferencesService(PreferencesService preferencesService) {
		this.preferencesService = preferencesService;
	}

	public List<AppParameter> getAppParameters() {
		return appParameters;
	}

	public void setAppParameters(List<AppParameter> appParameters) {
		this.appParameters = appParameters;
	}

	public List<AppParameter> getFilterAppParameter() {
		return filterAppParameter;
	}

	public void setFilterAppParameter(List<AppParameter> filterAppParameter) {
		this.filterAppParameter = filterAppParameter;
	}

	public GeneralServices getGeneralServices() {
		return generalServices;
	}

	public void setGeneralServices(GeneralServices generalServices) {
		this.generalServices = generalServices;
	}

	public void setPagesFilter(List<String> pagesFilter) {
		this.pagesFilter = pagesFilter;
	}

	public Action getSelectedAction() {
		return selectedAction;
	}

	public void setSelectedAction(Action selectedAction) {
		this.selectedAction = selectedAction;
	}

	public List<Action> getActionList() {
		return actionList;
	}

	public void setActionList(List<Action> actionList) {
		this.actionList = actionList;
	}

	public AppParameter getSelectedAppParam() {
		return selectedAppParam;
	}

	public void setSelectedAppParam(AppParameter selectedAppParam) {
		if (selectedAppParam != null)
			this.selectedAppParam = selectedAppParam;
	}

	public String getNewParamDesc() {
		return newParamDesc;
	}

	public void setNewParamDesc(String newParamDesc) {
		this.newParamDesc = newParamDesc;
	}

	public String getSelectedPage() {
		return selectedPage;
	}

	public void setSelectedPage(String selectedPage) {
		this.selectedPage = selectedPage;
	}

	public Map<String, String> getJsonParameterMap() {
		return jsonParameterMap;
	}

	public void setJsonParameterMap(Map<String, String> jsonParameterMap) {
		this.jsonParameterMap = jsonParameterMap;
	}

	public List<Request_Header> getHeaders() {
		return headers;
	}

	public void setHeaders(List<Request_Header> headers) {
		this.headers = headers;
	}

	public Request getRequest() {
		return request;
	}

	public void setRequest(Request request) {
		this.request = request;
	}

	public List<Request> getRequestList() {
		return requestList;
	}

	public void setRequestList(List<Request> requestList) {
		this.requestList = requestList;
	}

	public Request getSelectedRequest() {
		return selectedRequest;
	}

	public void setSelectedRequest(Request selectedRequest) {
		this.selectedRequest = selectedRequest;
	}

	public void setRp_username(String rp_username) {
		this.rp_username = rp_username;
	}

	public void setRp_password(String rp_password) {
		this.rp_password = rp_password;
	}

	public String getRp_username() {
		return rp_username;
	}

	public String getRp_password() {
		return rp_password;
	}

	public String getOutput() {
		return output;
	}

	public void setOutput(String output) {
		this.output = output;
	}

	public List<AppParameter> getFilteredAppParameters() {
		return filteredAppParameters;
	}

	public void setFilteredAppParameters(List<AppParameter> filteredAppParameters) {
		this.filteredAppParameters = filteredAppParameters;
	}

	public String getStepPage() {
		return stepPage;
	}

	public void setStepPage(String stepPage) {
		this.stepPage = stepPage;
	}

	public TestCaseService getTestCaseService() {
		return testCaseService;
	}

	public void setTestCaseService(TestCaseService testCaseService) {
		this.testCaseService = testCaseService;
	}

	public List<TestCase> getUsedTestCaseList() {
		return usedTestCaseList;
	}

	public void setUsedTestCaseList(List<TestCase> usedTestCaseList) {
		this.usedTestCaseList = usedTestCaseList;
	}

}
