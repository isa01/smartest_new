package com.tr.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.primefaces.model.chart.LineChartModel;
import org.primefaces.model.chart.PieChartModel;

import com.tr.model.AppParameter;
import com.tr.model.Comments;
import com.tr.model.Defect;
import com.tr.model.Report;
import com.tr.model.ScheduleStep;
import com.tr.model.TestCase;
import com.tr.service.DefectServices;
import com.tr.service.GeneralServices;
import com.tr.service.TestCaseService;
import com.tr.service.TestSuiteService;
import com.tr.service.UserService;
 
import lombok.Data;

@ManagedBean(name = "reportController")
@Data
@SessionScoped
public class ReportController implements Serializable {

	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(ReportController.class);

	@ManagedProperty("#{userService}")
	private UserService userService;

	@ManagedProperty("#{generalServices}")
	private GeneralServices generalServices;
	
	@ManagedProperty("#{defectServices}")
	private DefectServices defectServices;

	@ManagedProperty("#{testSuiteService}")
	private TestSuiteService testSuiteService;

	@ManagedProperty("#{testCaseService}")
	private TestCaseService testCaseService;

	transient HttpSession session;
	private Report selectrpt;
	private List<Report> listReport, filterReport;

	private int totalTestCaseCount;
	private int totalRunCount;
	private int successCount = 0;
	private int failedCount = 0;
	private long totalSuccessRate;
	private List<TestCase> lastRunFailedTestCaseList;
	private List<AppParameter> appParamListNotUsed;
	private List<TestCase> lastWeekNotRunTestCaseList;
	private List<Defect> totalDefectListOpenByAutomation;
	private List<Comments> lastComments;
	private PieChartModel testCaseState;
	private List<ScheduleStep> lss;
	private List<String> images;
	
	public ReportController() throws IOException {
		super();
		session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);

		if (session.getAttribute("userId") == null) {
			ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
			context.redirect(context.getRequestContextPath() + "/login.xhtml?faces-redirect=true");
		}

	}

	@PostConstruct
 	public void init(){
 		
 		listReport=testSuiteService.getListReport();
 		for(Report r : listReport)
 		{
 			if(r.getState() != null && r.getState().equals("passed"))
 				successCount++;
 			else if(r.getState() != null && r.getState().equals("failed"))
 				failedCount++;	
 		}
 		List<TestCase> testCaseList = testCaseService.getAllTestCase();
 		images = new ArrayList<String>();
 		totalTestCaseCount = testCaseList.size();
 		totalRunCount = listReport.size();
// 		totalSuccessRate = 
 		lastRunFailedTestCaseList = testCaseService.lastRunFailedTestCaseList();
 		appParamListNotUsed = testCaseService.appParamListNotUsed();
// 		lastWeekNotRunTestCaseList = testCaseService.lastWeekNotRunTestCaseList();
 		totalDefectListOpenByAutomation = defectServices.getListDefect();
 		lastComments = defectServices.getCommentList();
 		createSuccessRate();
 		
 	}

	private void createSuccessRate() {
		testCaseState = new PieChartModel();
		testCaseState.set("Success", successCount);
		testCaseState.set("Failed", failedCount);
		testCaseState.setTitle("Test Case Run Success Rate");
		testCaseState.setLegendPosition("w");
	}
	
	public void onRowSelect(Report r) { 
		lss = new ArrayList<ScheduleStep>(r.getScheduler().getScheduleSteps());
		Collections.sort(lss);
	}
	
	public void initializeGalleria(Report r) {
		lss = new ArrayList<ScheduleStep>(r.getScheduler().getScheduleSteps());
		Collections.sort(lss);
		for(ScheduleStep ss : lss) {
			images.add(ss.getSs_url());
		} 
	}

	// getter setter

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public GeneralServices getGeneralServices() {
		return generalServices;
	}

	public void setGeneralServices(GeneralServices generalServices) {
		this.generalServices = generalServices;
	}

	public TestSuiteService getTestSuiteService() {
		return testSuiteService;
	}

	public void setTestSuiteService(TestSuiteService testSuiteService) {
		this.testSuiteService = testSuiteService;
	}

	public Report getSelectrpt() {
		return selectrpt;
	}

	public void setSelectrpt(Report selectrpt) {
		this.selectrpt = selectrpt;
	}

	public List<Report> getListReport() {
		return listReport;
	}

	public void setListReport(List<Report> listReport) {
		this.listReport = listReport;
	}

	public List<Report> getFilterReport() {
		return filterReport;
	}

	public void setFilterReport(List<Report> filterReport) {
		this.filterReport = filterReport;
	}

	public TestCaseService getTestCaseService() {
		return testCaseService;
	}

	public void setTestCaseService(TestCaseService testCaseService) {
		this.testCaseService = testCaseService;
	}

	public int getTotalTestCaseCount() {
		return totalTestCaseCount;
	}

	public void setTotalTestCaseCount(int totalTestCaseCount) {
		this.totalTestCaseCount = totalTestCaseCount;
	}

	public int getTotalRunCount() {
		return totalRunCount;
	}

	public void setTotalRunCount(int totalRunCount) {
		this.totalRunCount = totalRunCount;
	}

	public long getTotalSuccessRate() {
		return totalSuccessRate;
	}

	public void setTotalSuccessRate(long totalSuccessRate) {
		this.totalSuccessRate = totalSuccessRate;
	}

	public List<TestCase> getLastRunFailedTestCaseList() {
		return lastRunFailedTestCaseList;
	}

	public void setLastRunFailedTestCaseList(List<TestCase> lastRunFailedTestCaseList) {
		this.lastRunFailedTestCaseList = lastRunFailedTestCaseList;
	}

	public List<AppParameter> getAppParamListNotUsed() {
		return appParamListNotUsed;
	}

	public void setAppParamListNotUsed(List<AppParameter> appParamListNotUsed) {
		this.appParamListNotUsed = appParamListNotUsed;
	}

	public List<TestCase> getLastWeekNotRunTestCaseList() {
		return lastWeekNotRunTestCaseList;
	}

	public void setLastWeekNotRunTestCaseList(List<TestCase> lastWeekNotRunTestCaseList) {
		this.lastWeekNotRunTestCaseList = lastWeekNotRunTestCaseList;
	}

	public List<Defect> getTotalDefectListOpenByAutomation() {
		return totalDefectListOpenByAutomation;
	}

	public void setTotalDefectListOpenByAutomation(List<Defect> totalDefectListOpenByAutomation) {
		this.totalDefectListOpenByAutomation = totalDefectListOpenByAutomation;
	}

	public DefectServices getDefectServices() {
		return defectServices;
	}

	public void setDefectServices(DefectServices defectServices) {
		this.defectServices = defectServices;
	}

	public int getSuccessCount() {
		return successCount;
	}

	public void setSuccessCount(int successCount) {
		this.successCount = successCount;
	}

	public int getFailedCount() {
		return failedCount;
	}

	public void setFailedCount(int failedCount) {
		this.failedCount = failedCount;
	}

	public PieChartModel getTestCaseState() {
		return testCaseState;
	}

	public void setTestCaseState(PieChartModel testCaseState) {
		this.testCaseState = testCaseState;
	}

	public List<Comments> getLastComments() {
		return lastComments;
	}

	public void setLastComments(List<Comments> lastComments) {
		this.lastComments = lastComments;
	}

	public List<ScheduleStep> getLss() {
		return lss;
	}

	public void setLss(List<ScheduleStep> lss) {
		this.lss = lss;
	}

	public List<String> getImages() {
		return images;
	}

	public void setImages(List<String> images) {
		this.images = images;
	}

}
